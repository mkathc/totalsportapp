package com.totalsportapp.extra;

import android.Manifest;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.totalsportapp.R;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.model.NotificationFirebaseType;
import com.totalsportapp.data.db.model.LoggedMode;
import com.totalsportapp.data.prefs.PreferencesHelper;
import com.totalsportapp.ui.encuestas.EncuestaActivity;
import com.totalsportapp.ui.invitationchallenge.TeamInvitationChallengeActivity;
import com.totalsportapp.ui.main.MainActivity;
import com.totalsportapp.ui.myfriends.MyFriendsActivity;
import com.totalsportapp.ui.team.TeamActivity;
import com.totalsportapp.ui.teammemberslocation.TeamMembersMapsActivity;
import com.totalsportapp.ui.teamrequests.TeamRequestsActivity;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //super.onMessageReceived(remoteMessage);

        String title = "";
        String body = "";

        int idReto = 0;
        int idEquipo = 0;

        if(remoteMessage.getData().containsKey("NotificationTitle")){
            title = remoteMessage.getData().get("NotificationTitle");
            body = remoteMessage.getData().get("NotificationBody");
        }else{
            RemoteMessage.Notification notification = remoteMessage.getNotification();
            title = notification.getTitle();
            body = notification.getBody();
        }


        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(title)
                .setContentText(body)
                .setAutoCancel(true)
                .setSound(defaultSoundUri);

        if(remoteMessage.getData().containsKey("NotificationType")){
            int notificationType = Integer.parseInt(remoteMessage.getData().get("NotificationType"));

            if(notificationType == NotificationFirebaseType.FRIEND_ADD){
                Intent intent = new Intent(this, MyFriendsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);
                notificationBuilder.setContentIntent(pendingIntent);
            } else if(notificationType == NotificationFirebaseType.JOIN_TEAM){
                Intent intent = new Intent(this, TeamRequestsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);
                notificationBuilder.setContentIntent(pendingIntent);
            } else if(notificationType == NotificationFirebaseType.CHALLENGE_TEAM){
                Intent intent = new Intent(this, TeamRequestsActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);
                notificationBuilder.setContentIntent(pendingIntent);
            } else if(notificationType == NotificationFirebaseType.START_SERVICE_LOCATION){
                Log.e("AppFirebaseMessaging", "START_SERVICE_LOCATION");
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ContextCompat.checkSelfPermission(this, Manifest.permission.FOREGROUND_SERVICE) != PackageManager.PERMISSION_GRANTED) {

                }else{
                    Log.e("IntentService", "START_SERVICE_LOCATION");
                    PreferencesHelper.getInstance().setUbicationKey(true);
                    Intent i = new Intent(this, AppService.class);
                    i.putExtra("data", remoteMessage.getData().get("NotificationData"));
                    ContextCompat.startForegroundService(this, i);

                    Intent intent = new Intent(this, TeamMembersMapsActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    intent.putExtra(MainActivity.KEY_TAB_LOAD, 1);
                    intent.putExtra("data", remoteMessage.getData().get("NotificationData"));
                    intent.putExtra(TeamMembersMapsActivity.KEY_TEAM_ID, -1);
                    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent, PendingIntent.FLAG_ONE_SHOT);
                    notificationBuilder.setContentIntent(pendingIntent);
                }
            }else if(notificationType == NotificationFirebaseType.END_SERVICE_LOCATION){
                Intent stopIntent = new Intent(this, AppService.class);
                stopService(stopIntent);
                PreferencesHelper.getInstance().setUbicationKey(false);
                Intent i = new Intent(this, MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra(MainActivity.KEY_TAB_LOAD, 1);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, i, PendingIntent.FLAG_ONE_SHOT);
                notificationBuilder.setContentIntent(pendingIntent);
            } else if(notificationType == NotificationFirebaseType.CHALLENGE_INVITE_MEMBER){
                Intent i = new Intent(this, TeamInvitationChallengeActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, i, PendingIntent.FLAG_ONE_SHOT);
                notificationBuilder.setContentIntent(pendingIntent);
            } else if(notificationType == NotificationFirebaseType.CHALLENGE_INVITE_RESPONSE){
                Intent i = new Intent(this,  MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, i, PendingIntent.FLAG_ONE_SHOT);
                notificationBuilder.setContentIntent(pendingIntent);
            } else if(notificationType == NotificationFirebaseType.CHALLENGE_RESPONSE){
                Intent i = new Intent(this, MainActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra(MainActivity.KEY_TAB_LOAD, 1);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, i, PendingIntent.FLAG_ONE_SHOT);
                notificationBuilder.setContentIntent(pendingIntent);
            } else if(notificationType == NotificationFirebaseType.INTEGRATION_RESPONSE){
                Intent i = new Intent(this, TeamActivity.class);
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, i, PendingIntent.FLAG_ONE_SHOT);
                notificationBuilder.setContentIntent(pendingIntent);
            }else if(notificationType == NotificationFirebaseType.CHALLENGE_QUALIFY){
                PreferencesHelper.getInstance().setUbicationKey(false);
                Intent i = new Intent(this, EncuestaActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                i.putExtra("data", remoteMessage.getData().get("NotificationData"));
                PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, i, PendingIntent.FLAG_ONE_SHOT);
                notificationBuilder.setContentIntent(pendingIntent);
            }
        }

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId, "Channel human readable title", NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        PreferencesHelper pref = PreferencesHelper.getInstance();
        int notificacionId = pref.getNotificationId() + 1;
        pref.setNotificationId(notificacionId);
        notificationManager.notify(notificacionId, notificationBuilder.build());
    }

    @Override
    public void onNewToken(final String newToken) {

        final PreferencesHelper pref = PreferencesHelper.getInstance();
        if(pref.getCurrentUserLoggedInMode() == LoggedMode.LOGGED_IN){
            String oldToken = pref.getTokenFCM();

            AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
            Call<ResponseBody> call = authServicesAPI.refreshTokenFCM(oldToken, newToken);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if(response.isSuccessful()){
                        pref.setTokenFCM(newToken);
                    }else{
                        pref.setTokenFCM(null);
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    pref.setTokenFCM(null);
                }
            });
        }
    }
}
