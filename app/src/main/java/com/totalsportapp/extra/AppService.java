package com.totalsportapp.extra;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.totalsportapp.R;
import com.totalsportapp.data.api.UtilAPI;
import com.totalsportapp.data.api.model.EquipoSolicitudRetarDataFCM;
import com.totalsportapp.data.db.model.LoggedMode;
import com.totalsportapp.data.db.model.Usuario;
import com.totalsportapp.data.prefs.PreferencesHelper;
import com.totalsportapp.ui.main.MainActivity;
import com.totalsportapp.ui.start.SliderPageAdapter;
import com.totalsportapp.ui.teamrequests.TeamRequestsActivity;

import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executor;

import hirondelle.date4j.DateTime;

public class AppService extends Service {

    private static final int LOCATION_INTERVAL = 300 * 1000;
    private static final int LOCATION_FAST_INTERVAL = 60 * 1000;
    private static final int MINIMUM_DISTANCE = 10;

    private FusedLocationProviderClient mFusedLocationClient;
    private LocationCallback locationCallback;
    private LocationManager locationManager;
    private LocationListener locationListener;

    @Override
    public void onCreate() {
        super.onCreate();

        Log.e("SERVICES", "onCreate");

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationListener = new LocationListener();

        //checkLocation();
    }

    /*private boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
       /* final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Activar ubicación")
                .setMessage("Su ubicación esta desactivada.\npor favor active su ubicación ")
                .setPositiveButton("Configuración de ubicación", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    }
                });
        dialog.show();*/
      /*  Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(myIntent);
    }

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }*/

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.e("SERVICES", "onStartCommand");

        String channelId = getString(R.string.default_notification_channel_id);

        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 1, notificationIntent, 0);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, channelId)
                .setSmallIcon(R.drawable.ic_notification)
                .setContentTitle(getApplicationContext().getText(R.string.app_name))
                .setContentText("Compartiendo ubicación en tiempo real")
                .setContentIntent(pendingIntent)
                .setAutoCancel(true);

        startForeground(123, notificationBuilder.build());

        String jsonData = intent.getExtras().getString("data");
        Usuario usuario = PreferencesHelper.getInstance().getUser();

        if(jsonData != null){
            Gson gson = UtilAPI.getGsonSync();
            EquipoSolicitudRetarDataFCM equipoSolicitudRetarDataFCM = gson.fromJson(jsonData, EquipoSolicitudRetarDataFCM.class);


            boolean retador = false;
            boolean retado = false;

            for(String item : equipoSolicitudRetarDataFCM.getEmailsRetadores()){
                if(usuario.getEmail().equalsIgnoreCase(item)){
                    retador = true;
                    Log.e("SERVICES", "retador = true");

                }
            }

            if(retador == false){
                for(String item : equipoSolicitudRetarDataFCM.getEmailsRetados()){
                    if(usuario.getEmail().equalsIgnoreCase(item)){
                        retado = true;
                        Log.e("SERVICES", "retado = true");
                    }
                }
            }

            if(retado == true){

                locationListener.connect(equipoSolicitudRetarDataFCM.getIdEquipoSolicitudRetar(), equipoSolicitudRetarDataFCM.getEquipoRetado(), usuario.getEmail());
            }else if(retador == true){

                locationListener.connect(equipoSolicitudRetarDataFCM.getIdEquipoSolicitudRetar(), equipoSolicitudRetarDataFCM.getEquipoRetador(), usuario.getEmail());
            }
        }else{

            int idReto = intent.getExtras().getInt("idReto");
            int idEquipo = intent.getExtras().getInt("idEquipo");

            locationListener.connect(idReto, idEquipo , usuario.getEmail());

        }


        return START_NOT_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e("SERVICES", "onDestroy");

        if (mFusedLocationClient != null) {
            mFusedLocationClient.removeLocationUpdates(locationCallback);
        }
    }

    class LocationListener {
        private LocationRequest mLocationRequest;
        private Context ctx;

        public LocationListener() {
            Log.e("SERVICES", "contruct");
            ctx = getApplicationContext();
            mFusedLocationClient = LocationServices.getFusedLocationProviderClient(ctx);
        }

        @SuppressLint("MissingPermission")
        public void connect(final int idReto, final int idEquipo, final String email){
            Log.e("SERVICES", "connect");
            PreferencesHelper.getInstance().setEquipoNumber(idEquipo);
            PreferencesHelper.getInstance().setRetoNumber(idReto);
            mLocationRequest = LocationRequest.create();
            mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            mLocationRequest.setInterval(LOCATION_INTERVAL);
            mLocationRequest.setSmallestDisplacement(MINIMUM_DISTANCE);
            mLocationRequest.setFastestInterval(LOCATION_FAST_INTERVAL);

            locationCallback = new LocationCallback() {
                @Override
                public void onLocationResult(LocationResult locationResult) {
                    Log.e("SERVICES", "locationCallback");
                    if (locationResult == null) {
                        Log.e("SERVICES", "locationResult == null");
                        return;
                    }
                    Location location = locationResult.getLastLocation();

                    if (location != null) {
                        Log.e("SERVICES" , "LAT:" + location.getLatitude() + " LNG:" + location.getLongitude());
                        double wayLatitude = location.getLatitude();
                        double wayLongitude = location.getLongitude();

                        FirebaseDatabase database = FirebaseDatabase.getInstance();
                        DatabaseReference myRef = database.getReference("reto-" + idReto + "/equipo-" + idEquipo + "/" + email.replace('.', ','));

                        Map<String, Object> data = new HashMap<String, Object>();
                        data.put("lat", wayLatitude);
                        data.put("lng", wayLongitude);
                        data.put("datetime", new Date().getTime());

                        myRef.setValue(data);
                    } else {
                        Log.e("SERVICES", "location == null");
                    }
                }
            };

            mFusedLocationClient.requestLocationUpdates(mLocationRequest, locationCallback, null);

            Log.e("SERVICES", "connected");
        }
    }
}
