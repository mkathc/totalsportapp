package com.totalsportapp.ui.teamprofile;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.totalsportapp.R;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.UtilAPI;
import com.totalsportapp.data.api.model.OperationResult;
import com.totalsportapp.data.api.model.ScheduleAPI;
import com.totalsportapp.data.api.model.SolicitudEquipoAPI;
import com.totalsportapp.data.db.model.Equipo;
import com.totalsportapp.data.db.model.EquipoPreferenciaHoraria;
import com.totalsportapp.data.db.model.NivelEquipo;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.ui.challengeteam.ChallengeFormActivity;
import com.totalsportapp.ui.jointeam.JoinTeamFormActivity;
import com.totalsportapp.ui.schedule.ScheduleActivity;
import com.totalsportapp.ui.teammembers.TeamMembersActivity;
import com.totalsportapp.util.GlideApp;
import com.totalsportapp.util.NetworkUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeamProfileInvitationActivity extends BaseAppCompatActivity implements View.OnClickListener {

    public final static String KEY_TEAM_ID = "KEY_TEAM_ID";
    public final static String KEY_TYPE_TEAM_PROFILE = "KEY_TYPE_TEAM_PROFILE";
    public static final String KEY_TEAM_NAME = "KEY_TEAM_NAME";
    public static final String KEY_TEAM_RETO_NAME = "KEY_TEAM_RETO_NAME";
    public static final String KEY_TEAM_RETO_ID = "KEY_TEAM_RETO_ID";
    public static final String KEY_REQUEST_DATA = "KEY_REQUEST_DATA";

    public final static int TYPE_CHALLENGE_TEAM = 1;
    public final static int TYPE_JOIN_TEAM = 2;
    public final static int TYPE_PROFILE = 3;
    public final static int TYPE_PROFILE_CHALLENGE_TEAM = 4;

    //region controls
    private Toolbar toolbar;
    private TextView txtvTeamName;
    private TextView txtv_team_level;
    private TextView txtv_capitain;
    private ImageView imgv_team_photo;
    private RelativeLayout rl_team_members;
    private RelativeLayout rl_team_schedule;
    private RelativeLayout rl_team_locations;
    private RelativeLayout rl_team_join;
    private View rl_team_join_line;
    private RelativeLayout rl_team_challenge;
    private View rl_team_challenge_line;
    private LinearLayout ll_container_accept_request;
    private TextView txtv_msg;
    private Button btn_accept_request;
    private Button btn_deny_request;
    private TextView txtv_info_reto;
    private View separator_info_reto;
    //endregion

    private int typeProfile;

    private Equipo equipoLoaded;
    private ArrayList<ScheduleAPI> scheduleAPI;
    private SolicitudEquipoAPI solicitudEquipoAPI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_profile);

        onCreateBase();

        typeProfile = getIntent().getExtras().getInt(KEY_TYPE_TEAM_PROFILE);

        if(typeProfile == TYPE_PROFILE_CHALLENGE_TEAM){
            Gson gson = UtilAPI.getGsonSync();
            String requestData = getIntent().getExtras().getString(KEY_REQUEST_DATA);

            solicitudEquipoAPI = gson.fromJson(requestData, SolicitudEquipoAPI.class);
        }

        scheduleAPI = new ArrayList<>();

        findControls();

        configureControls();

        if(NetworkUtils.isNetworkConnected(ctx)){
            loadTeamProfile();
        }else{
            showDisconnectedNetwork();
            onBackPressed();
        }
    }

    private void findControls(){
        toolbar = findViewById(R.id.toolbar);
        txtvTeamName = findViewById(R.id.txtv_team_name);
        txtv_team_level = findViewById(R.id.txtv_team_level);
        txtv_capitain = findViewById(R.id.txtv_capitain);
        imgv_team_photo = findViewById(R.id.imgv_team_photo);
        rl_team_schedule = findViewById(R.id.rl_team_schedule);
        rl_team_members = findViewById(R.id.rl_team_members);
        rl_team_join = findViewById(R.id.rl_team_join);
        rl_team_join_line = findViewById(R.id.rl_team_join_line);
        rl_team_challenge = findViewById(R.id.rl_team_challenge);
        rl_team_challenge_line = findViewById(R.id.rl_team_challenge_line);
        rl_team_locations = findViewById(R.id.rl_team_locations);
        ll_container_accept_request = findViewById(R.id.ll_container_accept_request);
        txtv_msg = findViewById(R.id.txtv_msg);
        btn_accept_request = findViewById(R.id.btn_accept_request);
        btn_deny_request = findViewById(R.id.btn_deny_request);
        txtv_info_reto = findViewById(R.id.txtv_info_reto);
        separator_info_reto = findViewById(R.id.separator_info_reto);
    }

    private void configureControls(){
        rl_team_schedule.setOnClickListener(this);
        rl_team_members.setOnClickListener(this);
        rl_team_locations.setOnClickListener(this);
        imgv_team_photo.setClipToOutline(true);
        btn_accept_request.setOnClickListener(this);
        btn_deny_request.setOnClickListener(this);

        setUpToolbar();

        if(typeProfile == TYPE_CHALLENGE_TEAM){
            rl_team_challenge.setOnClickListener(this);
            rl_team_challenge.setVisibility(View.VISIBLE);
            rl_team_challenge_line.setVisibility(View.VISIBLE);
        } else if(typeProfile == TYPE_JOIN_TEAM){
            rl_team_join.setOnClickListener(this);
            rl_team_join.setVisibility(View.VISIBLE);
            rl_team_join_line.setVisibility(View.VISIBLE);
        } else if(typeProfile == TYPE_PROFILE){
            rl_team_challenge.setVisibility(View.GONE);
            rl_team_challenge_line.setVisibility(View.GONE);
            rl_team_join.setVisibility(View.GONE);
            rl_team_join_line.setVisibility(View.GONE);
        }
    }

    private void setUpToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("Perfil de Equipo");
    }

    private void rl_team_schedule_click(){
        Gson gson = new Gson();
        Intent i = new Intent(ctx, ScheduleActivity.class);
        i.putExtra(ScheduleActivity.KEY_SCHEDULE_CAN_SAVE, false);
        i.putExtra(ScheduleActivity.KEY_SCHEDULE_TEAM_DATA, gson.toJson(scheduleAPI));
        i.putExtra(ScheduleActivity.KEY_SCHEDULE_TYPE, ScheduleActivity.SCHEDULE_TYPE_TEAM);
        startActivity(i);
    }

    private void rl_team_members_click(){
        Intent i = new Intent(ctx, TeamMembersActivity.class);
        i.putExtra(TeamMembersActivity.KEY_TEAM_ID, equipoLoaded.getIdEquipo());
        startActivity(i);
    }

    private void rl_team_join_click(){
        Intent i = new Intent(ctx, JoinTeamFormActivity.class);
        i.putExtra(JoinTeamFormActivity.KEY_TEAM_ID, equipoLoaded.getIdEquipo());
        i.putExtra(JoinTeamFormActivity.KEY_TEAM_NAME, equipoLoaded.getNombre());
        startActivity(i);
    }

    private void rl_team_challenge_click(){
        Gson gson = UtilAPI.getGsonSync();

        Intent i = new Intent(ctx, ChallengeFormActivity.class);
        i.putExtra(ChallengeFormActivity.KEY_TEAM_ID, equipoLoaded.getIdEquipo());
        i.putExtra(ChallengeFormActivity.KEY_TEAM_NAME, equipoLoaded.getNombre());
        i.putExtra(ChallengeFormActivity.KEY_PLACES_TEAM, gson.toJson(equipoLoaded.getLugares()));
        startActivity(i);
    }

    private void rl_team_locations_click(){
        simpleAlert("Lugares", equipoLoaded.getLugaresText());
    }

    private void loadTeamProfile(){
        int idEquipo = getIntent().getExtras().getInt(KEY_TEAM_ID);

        progressDialog.show();

        AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<Equipo> call = authServicesAPI.infoTeam(idEquipo);
        call.enqueue(new Callback<Equipo>() {
            @Override
            public void onResponse(Call<Equipo> call, Response<Equipo> response) {
                if(response.isSuccessful()){
                    if(progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }

                    Equipo equipo = response.body();

                   if(typeProfile == TYPE_PROFILE_CHALLENGE_TEAM){
                        String mgsText = "<b>" + getIntent().getExtras().getString(KEY_TEAM_NAME) +"</b> te quiere invitar a participar en un reto con <b>\"" + getIntent().getExtras().getString(KEY_TEAM_RETO_NAME) + "\".</b>";

                        Locale spanish = new Locale("es", "ES");
                        String nivelEquipo = NivelEquipo.getDescripcion(solicitudEquipoAPI.getNivel());
                       // String fechaHora = solicitudEquipoAPI.getFechaReto().format(DATETIME_FORMAT_CLIENT, spanish);
                      //  String infoReto = "<b>Nivel:</b> " + nivelEquipo + "<br/>" + "<b>Fecha:</b> " + fechaHora;
                       String infoReto = "<b>Nivel:</b> " + nivelEquipo + "<br/>" + "<b>Fecha:</b> "    ;

                        if(solicitudEquipoAPI.getNombreSede() != null && solicitudEquipoAPI.getNombreSede().isEmpty() == false){
                            infoReto = infoReto + "<br/>" + "<b>Sede:</b> " + solicitudEquipoAPI.getNombreSede();
                        }else{
                            infoReto = infoReto + "<br/>" + "<b>Sede:</b> -";
                        }

                        if(solicitudEquipoAPI.getComentarios() != null && solicitudEquipoAPI.getComentarios().isEmpty() == false){
                            infoReto = infoReto + "<br/>" + "<b>Comentarios:</b><br/>" + solicitudEquipoAPI.getComentarios() + ".";
                        }else{
                            infoReto = infoReto + "<br/>" + "<b>Comentarios:</b><br/> Sin comentarios.";
                        }

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                            txtv_info_reto.setText(Html.fromHtml(infoReto, Html.FROM_HTML_MODE_COMPACT));
                            txtv_msg.setText(Html.fromHtml(mgsText, Html.FROM_HTML_MODE_COMPACT));
                        } else {
                            txtv_info_reto.setText(Html.fromHtml(infoReto));
                            txtv_msg.setText(Html.fromHtml(mgsText));
                        }

                       txtv_info_reto.setVisibility(View.VISIBLE);
                       separator_info_reto.setVisibility(View.VISIBLE);

                        ll_container_accept_request.setVisibility(View.VISIBLE);
                    }

                    equipoLoaded = equipo;

                    txtvTeamName.setText(equipo.getNombre());;
                    txtv_team_level.setText(NivelEquipo.getDescripcion(equipo.getNivel()));
                    txtv_capitain.setText(equipo.getCapitan());

                    for(EquipoPreferenciaHoraria item : equipo.getPreferenciaHorarias()){
                        scheduleAPI.add(new ScheduleAPI(item.getDia(), item.getHora()));
                    }

                    if(equipo.getFoto() != null){
                        imgv_team_photo.setPadding(0, 0 , 0, 0);
                        String photoUrl = equipo.getFoto();
                        GlideApp.with(ctx)
                                .load(photoUrl)
                                .dontAnimate()
                                .centerCrop()
                                .into(imgv_team_photo);
                    }
                }else{
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<Equipo> call, Throwable t) {
                showInternalError();
            }
        });
    }

    public void showInternalError() {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("ERROR", R.string.internal_error);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void btn_accept_request_click(){
        if(typeProfile == TYPE_PROFILE_CHALLENGE_TEAM){
            confirmAlert("¿Aprobar?", new ExecuteParam() {
                @Override
                public void onExecute() {
                    requestTeamResponse(solicitudEquipoAPI.getIdSolicitud(),1);
                }
            });
        }
    }

    private void btn_deny_request_click(){
        if(typeProfile == TYPE_PROFILE_CHALLENGE_TEAM){
            confirmAlert("¿Rechazar?", new ExecuteParam() {
                @Override
                public void onExecute() {
                    requestTeamResponse(solicitudEquipoAPI.getIdSolicitud(),0);
                }
            });
        }
    }

    private void requestTeamResponse(int idSolicitud, final int aprueba){
        progressDialog.show();

        AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<OperationResult> call = authServicesAPI.responseRequests(idSolicitud, SolicitudEquipoAPI.TIPO_SOLICITUD_RETAR, aprueba);
        call.enqueue(new Callback<OperationResult>() {
            @Override
            public void onResponse(Call<OperationResult> call, Response<OperationResult> response) {
                if (response.isSuccessful()){
                    OperationResult op = response.body();
                    if(op.isStatus()){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }

                        if(aprueba == 1){
                            Toast.makeText(ctx, "Solicitud Aprobada", Toast.LENGTH_SHORT).show();
                        } else if (aprueba == 0){
                            Toast.makeText(ctx, "Solicitud Rechazada", Toast.LENGTH_SHORT).show();
                        }

                        setResult(RESULT_OK);
                        finish();
                    }else{
                        showResultSaveResponseFail(op.getErrors());
                    }
                }else{
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<OperationResult> call, Throwable t) {
                showInternalError();
            }
        });
    }

    public void showResultSaveResponseFail(List<String> errors) {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert(errors.get(0));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.rl_team_schedule:
                rl_team_schedule_click();
                break;
            case R.id.rl_team_members:
                rl_team_members_click();
                break;
            case R.id.rl_team_join:
                rl_team_join_click();
                break;
            case R.id.rl_team_challenge:
                rl_team_challenge_click();
                break;
            case R.id.rl_team_locations:
                rl_team_locations_click();
                break;
            case R.id.btn_accept_request:
                btn_accept_request_click();
                break;
            case R.id.btn_deny_request:
                btn_deny_request_click();
                break;
        }
    }
}
