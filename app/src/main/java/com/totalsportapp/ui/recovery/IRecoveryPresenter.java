package com.totalsportapp.ui.recovery;

import com.totalsportapp.data.api.model.RegisterRequest;
import com.totalsportapp.data.db.model.Recovery;

public interface IRecoveryPresenter {

    void sendEmailRecovery(String email);

    void sendDataRecovery(Recovery recovery);
}
