package com.totalsportapp.ui.profile;

import com.totalsportapp.data.db.model.Usuario;

import java.util.List;

public interface ProfileView {

    void setPrefUser(Usuario usuario);
    void onResultSaveProfileSuccess();
    void showResultSaveProfileFail(List<String> errors);

    void showFormValidateError(String messague);
    void showProcessingSaveProfile();

    void showInternalError();
}
