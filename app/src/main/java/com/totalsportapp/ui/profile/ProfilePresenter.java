package com.totalsportapp.ui.profile;

import android.util.Log;

import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.ServicesAPI;
import com.totalsportapp.data.api.model.OperationResult;
import com.totalsportapp.data.db.model.Gender;
import com.totalsportapp.data.db.model.Usuario;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfilePresenter implements IProfilePresenter {

    private ProfileView view;

    public ProfilePresenter(ProfileView profileView){
        view = profileView;
    }

    private boolean validateSaveProfile(Usuario usuario){
        if(usuario.getNombres().isEmpty()){
            view.showFormValidateError("Nombres son requeridos");
            return false;
        }

        if(usuario.getApellidos().isEmpty()){
            view.showFormValidateError("Apellidos son requeridos");
            return false;
        }

        if(usuario.getNombres().length() > 70){
            view.showFormValidateError("Nombres muy extenso, máximo 70 dígitos");
            return false;
        }

        if(usuario.getApellidos().length() > 70){
            view.showFormValidateError("Apellidos muy extenso, máximo 70 dígitos");
            return false;
        }

        if(usuario.getSexo() == null){
            view.showFormValidateError("Seleccione su sexo");
            return false;
        }

        return true;
    }

    @Override
    public void saveProfile(final Usuario usuario) {

        if(validateSaveProfile(usuario) == false){
            return;
        }

        view.showProcessingSaveProfile();

        AuthServicesAPI services = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<OperationResult> call = services.saveProfile(usuario);
        call.enqueue(new Callback<OperationResult>() {
            @Override
            public void onResponse(Call<OperationResult> call, Response<OperationResult> response) {
                if (response.isSuccessful()) {
                    OperationResult opResult = response.body();
                    if(opResult.isStatus()){

                        view.setPrefUser(usuario);
                        view.onResultSaveProfileSuccess();
                    }else{
                        view.showResultSaveProfileFail(opResult.getErrors());
                    }
                }else{
                    view.showInternalError();
                }
            }

            @Override
            public void onFailure(Call<OperationResult> call, Throwable t) {
                view.showInternalError();
            }
        });
    }
}
