package com.totalsportapp.ui.teammemberslocation;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.totalsportapp.R;
import com.totalsportapp.data.api.model.EquipoMiembroAPI;
import com.totalsportapp.util.AppConstants;
import com.totalsportapp.util.GlideApp;

import java.util.ArrayList;
import java.util.Locale;
import java.util.TimeZone;

import hirondelle.date4j.DateTime;

import static com.totalsportapp.util.AppConstants.BASE_URL_PHOTO;

public class TeamMemberMapAdapter extends ArrayAdapter<TeamMemberMapModel> {

    public TeamMemberMapAdapter(@NonNull Context context, ArrayList<TeamMemberMapModel> items) {
        super(context, 0, items);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_item_teamlocation, parent, false);
        }

        TeamMemberMapModel teamMemberMapModel = getItem(position);
        EquipoMiembroAPI amigoAPI = teamMemberMapModel.getEquipoMiembroAPI();
        ImageView imgv_photo = convertView.findViewById(R.id.imgv_photo);
        TextView txtv_fullname = convertView.findViewById(R.id.txtv_fullname);
        TextView txtv_nickname = convertView.findViewById(R.id.txtv_nickname);
        TextView txtv_update_time = convertView.findViewById(R.id.txtv_update_time);

        if(amigoAPI.getFoto() != null){
            imgv_photo.setClipToOutline(true);
            imgv_photo.setPadding(0,0,0,0);
            String photoUrl = AppConstants.BASE_URL_PHOTO_TEAM + amigoAPI.getFoto();
            //have to build project if GlideApp is not found
            GlideApp.with(getContext())
                    .load(photoUrl)
                    .dontAnimate()
                    .centerCrop()
                    .into(imgv_photo);
        }

        txtv_fullname.setText(amigoAPI.getNombreCompleto());
        if(amigoAPI.getAlias() != null){
            txtv_nickname.setText(amigoAPI.getAlias());
        }

        if(teamMemberMapModel.getDate() != null){
            DateTime dtNow = DateTime.now(TimeZone.getDefault());

            long secs = dtNow.numSecondsFrom(teamMemberMapModel.getDate());

            int minutes = ((int) secs / 60) * -1;

            if(minutes == 1){
                txtv_update_time.setText("Actualizado hace un min.");
            }else{
                txtv_update_time.setText("Actualizado hace " + minutes + " min.");
            }
        }

        return convertView;
    }
}
