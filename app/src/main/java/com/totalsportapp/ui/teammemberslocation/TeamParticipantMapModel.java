package com.totalsportapp.ui.teammemberslocation;

import com.totalsportapp.data.api.model.EquipoMiembroAPI;
import com.totalsportapp.data.api.model.MiembroParticipante;

import hirondelle.date4j.DateTime;

public class TeamParticipantMapModel {

    private MiembroParticipante miembroParticipante;
    private DateTime date;

    public MiembroParticipante getMiembroParticipante() {
        return miembroParticipante;
    }

    public void setMiembroParticipante(MiembroParticipante miembroParticipante) {
        this.miembroParticipante = miembroParticipante;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }
}
