package com.totalsportapp.ui.teammemberslocation;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.totalsportapp.R;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.UtilAPI;
import com.totalsportapp.data.api.model.CalendarioEquipoAPI;
import com.totalsportapp.data.api.model.EquipoMiembroAPI;
import com.totalsportapp.data.api.model.EquipoSolicitudRetarDataFCM;
import com.totalsportapp.data.api.model.MiembroParticipante;
import com.totalsportapp.data.db.model.EquipoMiembro;
import com.totalsportapp.data.db.model.Usuario;
import com.totalsportapp.data.prefs.PreferencesHelper;
import com.totalsportapp.ui.requestchallenge.TeamMembersRequestActivity;
import com.totalsportapp.util.NetworkUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;

import hirondelle.date4j.DateTime;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeamMembersMapsActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mMap;

    private Toolbar toolbar;
    private ListView lv_team_members;
    private int requestChallengeId;
    private int teamId;
    private int tipeList;
    public final static String TIPE_LIST = "TIPE_LIST";
    public final static String KEY_CHALLENGE_ID = "KEY_CHALLENGE_ID";
    public final static String KEY_TEAM_ID = "KEY_TEAM_ID";
    private LinearLayout btn_invite_members;
    private List<Marker> markers;

    private Context ctx;
    private ProgressDialog progressDialog;

    private ArrayList<EquipoMiembroAPI> teamMembers;
    private ArrayList<MiembroParticipante> teamParticipant;
    private ArrayList<MiembroParticipante> firstListAllMembers;
    private SupportMapFragment mapFragment;

    private boolean firstLoad;

    private TeamMemberMapAdapter adapter;
    private TeamParticipantMapAdapter participantAdapter;
    private int idEquipo;

    private LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_members_maps);

        ctx = this;

        teamId = getIntent().getExtras().getInt(KEY_TEAM_ID);
        tipeList = getIntent().getExtras().getInt(TIPE_LIST);

        markers = new ArrayList<>();
        teamParticipant = new ArrayList<>();
        findControls();

        configureControls();

        if(getIntent().getExtras().getInt(KEY_TEAM_ID) == -1 ){
            String jsonData = getIntent().getExtras().getString("data");
            Log.e("DATA MAPS", jsonData);
            Gson gson = UtilAPI.getGsonSync();
            EquipoSolicitudRetarDataFCM equipoSolicitudRetarDataFCM = gson.fromJson(jsonData, EquipoSolicitudRetarDataFCM.class);
            idEquipo = equipoSolicitudRetarDataFCM.getMiEquipo();
            teamId = equipoSolicitudRetarDataFCM.getMiEquipo();
            requestChallengeId = equipoSolicitudRetarDataFCM.getIdEquipoSolicitudRetar();
        }else {
            Log.e("KEY_TEAM_ID", String.valueOf(getIntent().getExtras().getInt(KEY_TEAM_ID)));
            idEquipo = getIntent().getExtras().getInt(KEY_TEAM_ID);
            requestChallengeId = getIntent().getExtras().getInt(KEY_CHALLENGE_ID);
        }

        if (NetworkUtils.isNetworkConnected(ctx)) {
            displayParticipant();
            // displayMembers();
        } else {
            Toast.makeText(ctx, R.string.disconnected_network, Toast.LENGTH_LONG).show();
            onBackPressed();
        }


        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        if(PreferencesHelper.getInstance().getUbicationKey()){
            checkLocation();
            PreferencesHelper.getInstance().setUbicationKey(false);
        }


    }

    private boolean checkLocation() {

        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Activar ubicación")
                .setMessage("Su ubicación esta desactivada.\npor favor active su ubicación ")
                .setCancelable(false)
                .setPositiveButton("Configuración de ubicación", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        Toast.makeText(ctx, "Si no enciende su ubicación, no podrá ver la unibicación de sus compañeros", Toast.LENGTH_SHORT).show();
                    }
                });
        dialog.show();

    }

    private boolean isLocationEnabled() {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void findControls() {
        toolbar = findViewById(R.id.toolbar);
        mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        lv_team_members = findViewById(R.id.lv_team_members);
        btn_invite_members = findViewById(R.id.btn_invite_members);
        if(tipeList == CalendarioEquipoAPI.CALENDARIO_TIPO_RETO){
            btn_invite_members.setVisibility(View.VISIBLE);
        }else{
            btn_invite_members.setVisibility(View.GONE);
        }
    }

    private void configureControls() {
        setUpToolbar();
        btn_invite_members.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), TeamMembersRequestActivity.class);
                intent.putExtra(KEY_TEAM_ID, teamId);
                intent.putExtra(KEY_CHALLENGE_ID, requestChallengeId);
                startActivity(intent);
            }
        });
    }

    private void setUpToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("Ubicación del Equipo");
    }

    private void displayParticipant() {
        progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("Espere por favor...");
        progressDialog.setCancelable(false);
        progressDialog.show();


        AuthServicesAPI services = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<ArrayList<MiembroParticipante>> call = services.teamMembersByReto(idEquipo, requestChallengeId);
        call.enqueue(new Callback<ArrayList<MiembroParticipante>>() {
            @Override
            public void onResponse(Call<ArrayList<MiembroParticipante>> call, Response<ArrayList<MiembroParticipante>> response) {
                if (response.isSuccessful()) {
                    firstListAllMembers = response.body();
                    for (int i = 0; i <firstListAllMembers.size() ; i++) {
                        if(firstListAllMembers.get(i).getEstado() == 1){
                            teamParticipant.add(firstListAllMembers.get(i));
                        }else{
                            if(firstListAllMembers.get(i).getEsCapitan() == 1){
                                teamParticipant.add(firstListAllMembers.get(i));
                            }
                        }
                    }

                    lv_team_members.setVisibility(View.VISIBLE);
                    if (teamParticipant.size() != 0) {
                        ArrayList<TeamParticipantMapModel> data = new ArrayList<>();

                        for (MiembroParticipante miembroParticipante : teamParticipant) {
                            TeamParticipantMapModel teamParticipantMapModel = new TeamParticipantMapModel();
                            teamParticipantMapModel.setMiembroParticipante(miembroParticipante);
                            teamParticipantMapModel.setDate(null);
                            data.add(teamParticipantMapModel);
                        }

                        participantAdapter = new TeamParticipantMapAdapter(ctx, data);
                        lv_team_members.setAdapter(participantAdapter);
                        mapFragment.getMapAsync(TeamMembersMapsActivity.this);
                    } else {
                        if (progressDialog.isShowing()) {
                            progressDialog.dismiss();
                        }
                    }

                } else {
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<MiembroParticipante>> call, Throwable t) {
                showInternalError();
            }
        });
    }

    private void displayMembers() {
        progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("Espere por favor...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        int idEquipo = getIntent().getExtras().getInt(KEY_TEAM_ID);

        AuthServicesAPI services = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<ArrayList<EquipoMiembroAPI>> call = services.teamMembers(idEquipo);
        call.enqueue(new Callback<ArrayList<EquipoMiembroAPI>>() {
            @Override
            public void onResponse(Call<ArrayList<EquipoMiembroAPI>> call, Response<ArrayList<EquipoMiembroAPI>> response) {
                if (response.isSuccessful()) {
                    teamMembers = response.body();

                    ArrayList<TeamMemberMapModel> data = new ArrayList<>();

                    for (EquipoMiembroAPI equipoMiembroAPI : teamMembers) {
                        TeamMemberMapModel teamMemberMapModel = new TeamMemberMapModel();
                        teamMemberMapModel.setEquipoMiembroAPI(equipoMiembroAPI);
                        teamMemberMapModel.setDate(null);
                        data.add(teamMemberMapModel);
                    }

                    adapter = new TeamMemberMapAdapter(ctx, data);
                    lv_team_members.setAdapter(adapter);

                    mapFragment.getMapAsync(TeamMembersMapsActivity.this);
                } else {
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<EquipoMiembroAPI>> call, Throwable t) {
                showInternalError();
            }
        });
    }

    private void setMapToCenter() {
        double latitude = -12.0469294;
        double longitude = -77.0437909;
        LatLng latLng = new LatLng(latitude, longitude);
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 10));
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        setMapToCenter();
        String path = "reto-" + requestChallengeId + "/equipo-" + teamId;

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        DatabaseReference myRef = database.getReference(path);

        final String emailLogged = PreferencesHelper.getInstance().getUser().getEmail();
        firstLoad = true;

        myRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (Marker item : markers) {
                    item.remove();
                }

                for (DataSnapshot item : dataSnapshot.getChildren()) {
                    Map<String, Object> data = (Map<String, Object>) item.getValue();
                    LatLng latLng = new LatLng(Double.parseDouble(data.get("lat").toString()), Double.parseDouble(data.get("lng").toString()));
                    long millis = Long.parseLong(data.get("datetime").toString());
                    String name = "";

                    boolean markerLogged = false;

                    for (int i = 0; i < teamParticipant.size(); i++) {
                        MiembroParticipante miembroParticipante = teamParticipant.get(i);
                        if (item.getKey().equalsIgnoreCase(miembroParticipante.getEmail().replace('.', ','))) {
                            name = miembroParticipante.getUsuarioNombre();
                            participantAdapter.getItem(i).setDate(DateTime.forInstant(millis, TimeZone.getDefault()));
                            break;
                        }
                    }

                    if (item.getKey().equalsIgnoreCase(emailLogged.replace('.', ','))) {
                        markerLogged = true;
                    }

                    Marker marker = mMap.addMarker(new MarkerOptions().position(latLng).title(name));

                    if (markerLogged && firstLoad) {
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 12));
                    }

                    markers.add(marker);
                }

                participantAdapter.notifyDataSetChanged();

                if (firstLoad) {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                }

                firstLoad = false;
            }

            @Override
            public void onCancelled(DatabaseError error) {
                Log.d("ERRORARA", "Failed to read value.", error.toException());
            }
        });
    }

    private void showInternalError() {
        if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        Toast.makeText(ctx, R.string.internal_error, Toast.LENGTH_LONG).show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
}
