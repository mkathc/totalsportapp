package com.totalsportapp.ui.teammemberslocation;

import com.totalsportapp.data.api.model.EquipoMiembroAPI;

import java.util.Date;

import hirondelle.date4j.DateTime;

public class TeamMemberMapModel {

    private EquipoMiembroAPI equipoMiembroAPI;
    private DateTime date;

    public EquipoMiembroAPI getEquipoMiembroAPI() {
        return equipoMiembroAPI;
    }

    public void setEquipoMiembroAPI(EquipoMiembroAPI equipoMiembroAPI) {
        this.equipoMiembroAPI = equipoMiembroAPI;
    }

    public DateTime getDate() {
        return date;
    }

    public void setDate(DateTime date) {
        this.date = date;
    }
}
