package com.totalsportapp.ui.start;


import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.totalsportapp.R;

//import com.totalsportapp.util.FontHelper;

public class SliderFragment extends Fragment {

    public static final String PARAM_POS = "param_pos";
    private int param_pos = 0;
    private ImageView imgvBackground;
    private TextView txtvPrimerMensaje;
    private TextView txtvSegundoMensaje;
    private Context ctx;
    private View vwFirstIndicatorInactive;
    private View vwSecondIndicatorInactive;
    private View vwThirdIndicatorInactive;
    private View vwFourthIndicatorInactive;
    private View vwFirstIndicatorActive;
    private View vwSecondIndicatorActive;
    private View vwThirdIndicatorActive;
    private View vwFourthIndicatorActive;

    public SliderFragment() {
    }

    public static SliderFragment newInstance(int param_pos) {
        SliderFragment fragment = new SliderFragment();
        Bundle args = new Bundle();
        args.putInt(PARAM_POS, param_pos);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            param_pos = getArguments().getInt(PARAM_POS);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.fragment_slider, container, false);

        txtvPrimerMensaje = layout.findViewById(R.id.txtv_primer_mensaje);
        txtvSegundoMensaje = layout.findViewById(R.id.txtv_segundo_mensaje);
        imgvBackground = layout.findViewById(R.id.imgv_background);

        ctx = getContext();

        vwFirstIndicatorInactive = layout.findViewById(R.id.view_first_indicator_inactive);
        vwSecondIndicatorInactive = layout.findViewById(R.id.view_second_indicator_inactive);
        vwThirdIndicatorInactive = layout.findViewById(R.id.view_third_indicator_inactive);
        //vwFourthIndicatorInactive = layout.findViewById(R.id.view_fourth_indicator_inactive);
        vwFirstIndicatorActive = layout.findViewById(R.id.view_first_indicator_active);
        vwSecondIndicatorActive = layout.findViewById(R.id.view_second_indicator_active);
        vwThirdIndicatorActive = layout.findViewById(R.id.view_third_indicator_active);
        //vwFourthIndicatorActive = layout.findViewById(R.id.view_fourth_indicator_active);

        Drawable imageBG = null;
        String primer_mensaje = "";
        String segundo_mensaje = "";

        switch (param_pos){
            case 0:
                imageBG = getResources().getDrawable(R.drawable.bg_slider_0);
                primer_mensaje = "¿Quieres organizar tus\npichangas y retar rivales?";
                segundo_mensaje = "Únete a la primera comunidad\nde peloteros del Perú";
                vwFirstIndicatorInactive.setVisibility(View.GONE);
                vwFirstIndicatorActive.setVisibility(View.VISIBLE);
                break;
            case 1:
                imageBG = getResources().getDrawable(R.drawable.bg_slider_1);
                primer_mensaje = "¿Necesitas medir tu nivel de\npelotero con nuevos rivales?";
                segundo_mensaje = "Regístrate, califica tus\npartidos y jugadores";
                vwSecondIndicatorInactive.setVisibility(View.GONE);
                vwSecondIndicatorActive.setVisibility(View.VISIBLE);
                break;
            case 2:
                imageBG = getResources().getDrawable(R.drawable.bg_slider_2);
                primer_mensaje = "Pichanguea todas las semanas\ncon diferentes peloteros";
                segundo_mensaje = "Regístrate con tu equipo\ny reta a otros";
                vwThirdIndicatorInactive.setVisibility(View.GONE);
                vwThirdIndicatorActive.setVisibility(View.VISIBLE);
                break;
        }

        txtvPrimerMensaje.setText(primer_mensaje);
        txtvSegundoMensaje.setText(segundo_mensaje);
        imgvBackground.setImageDrawable(imageBG);

        return layout;
    }

}
