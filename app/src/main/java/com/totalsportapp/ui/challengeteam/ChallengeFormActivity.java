package com.totalsportapp.ui.challengeteam;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.totalsportapp.MainApplication;
import com.totalsportapp.R;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.UtilAPI;
import com.totalsportapp.data.api.model.EquipoSolicitudRetarRequest;
import com.totalsportapp.data.api.model.OperationResult;
import com.totalsportapp.data.db.model.DaoSession;
import com.totalsportapp.data.db.model.Equipo;
import com.totalsportapp.data.db.model.EquipoDao;
import com.totalsportapp.data.db.model.EquipoLugar;
import com.totalsportapp.data.db.model.EquipoLugarDao;
import com.totalsportapp.data.db.model.Ubigeo;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.util.DateUtils;
import com.totalsportapp.util.SimpleItem;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChallengeFormActivity extends BaseAppCompatActivity implements View.OnClickListener {

    //region controls
    private Toolbar toolbar;
    private TextView txtv_team_challenge;
    private Button btn_send_request_challenge;
    private EditText etxt_datetime_challenge;
    private EditText etxt_place;
    private Spinner sp_my_teams;
    private EditText etxt_local;
    private EditText etxt_commentaries;
    //endregion

    public static final String KEY_TEAM_ID = "KEY_TEAM_ID";
    public static final String KEY_TEAM_NAME = "KEY_TEAM_NAME ";
    public static final String KEY_PLACES_TEAM = "KEY_PLACES_TEAM";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_form);

        onCreateBase();

        findControls();

        configureControls();

        String teamName = getIntent().getExtras().getString(KEY_TEAM_NAME);
        txtv_team_challenge.setText("Retando al equipo:\n" + teamName);
    }

    private void findControls(){
        toolbar = findViewById(R.id.toolbar);
        txtv_team_challenge = findViewById(R.id.txtv_team_challenge);
        btn_send_request_challenge = findViewById(R.id.btn_send_request_challenge);
        etxt_datetime_challenge = findViewById(R.id.etxt_datetime_challenge);
        etxt_place = findViewById(R.id.etxt_place);
        sp_my_teams = findViewById(R.id.sp_my_teams);
        etxt_local = findViewById(R.id.etxt_local);
        etxt_commentaries = findViewById(R.id.etxt_commentaries);
    }

    private void configureControls(){
        setUpToolbar();

        btn_send_request_challenge.setOnClickListener(this);

       // loadSpinnerMyTeams();
        loadTeamsSync();

        DateUtils.setDatetimePickerDialog(ctx, etxt_datetime_challenge);
        etxt_place.setFocusableInTouchMode(false);
        etxt_place.setOnClickListener(this);
        etxt_place.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                return false;
            }
        });
    }

    private void loadTeamsSync(){

        AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<ArrayList<Equipo>> call = authServicesAPI.myCapitanTeams();
        call.enqueue(new Callback<ArrayList<Equipo>>() {
            @Override
            public void onResponse(Call<ArrayList<Equipo>> call, Response<ArrayList<Equipo>> response) {
                if(response.isSuccessful()){
                    DaoSession daoSession = getDaoSession();
                    EquipoDao equipoDao = daoSession.getEquipoDao();
                    EquipoLugarDao equipoLugarDao = daoSession.getEquipoLugarDao();
                    equipoLugarDao.deleteAll();
                    equipoDao.deleteAll();

                    ArrayList<Equipo> result = response.body();

                    for(Equipo item : result){
                        item.setId(null);
                        equipoDao.insert(item);
                        for(EquipoLugar itemEL : item.getLugares()){
                            itemEL.setId(null);
                            equipoLugarDao.insert(itemEL);
                        }
                    }
                }

                loadSpinnerMyTeams();
            }

            @Override
            public void onFailure(Call<ArrayList<Equipo>> call, Throwable t) {

            }
        });
    }

    private void loadSpinnerMyTeams(){
        List<Equipo> equipos = ((MainApplication) getApplication()).getDaoSession().getEquipoDao().loadAll();
        ArrayAdapter<SimpleItem> adapter = new ArrayAdapter<SimpleItem>(ctx, R.layout.simple_spinner_item);
        adapter.add(new SimpleItem("0", "Escoger Equipo"));
        for (Equipo equipo : equipos) {
            adapter.add(new SimpleItem(String.valueOf(equipo.getIdEquipo()), equipo.getNombre()));
        }
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp_my_teams.setAdapter(adapter);
    }

    private void btn_send_request_challenge_click(){
        final int idEquipo = getIntent().getExtras().getInt(KEY_TEAM_ID);

        final String comments = etxt_commentaries.getText().toString();
        final String local = etxt_local.getText().toString();
        String dateChallenge = etxt_datetime_challenge.getText().toString();
        final String place = (etxt_place.getTag() == null ? "" : etxt_place.getTag().toString());
        final int myTeam = Integer.parseInt(((SimpleItem)sp_my_teams.getSelectedItem()).getCode());

        if(dateChallenge.isEmpty()){
            simpleAlert("Seleccione fecha y hora para el reto");
            return;
        }else if(DateUtils.getDatetime(etxt_datetime_challenge) == null) {
            simpleAlert("Seleccione fecha y hora válida para el reto");
            return;
        }

        if(place.isEmpty()){
            simpleAlert("Seleccione un lugar para el reto");
            return;
        }

        if(comments.length() > 400){
            simpleAlert("Máximo 400 carácteres para sus comentarios");
            return;
        }

        if(local.length() > 255){
            simpleAlert("Máximo 255 carácteres para la sede");
            return;
        }

        if(myTeam == 0){
            simpleAlert("Escoja su equipo para el reto");
            return;
        }

        if(local.length() == 0){
            final AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
            builder.setTitle("Advertencia");
            builder.setMessage("¿Desea crear el reto sin sugerencia de sede?");
            builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    sendReto(idEquipo, myTeam, local, comments, place);
                    dialog.dismiss();
                }
            });
            builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                 }
            });
            builder.show();
        }else {
            sendReto(idEquipo, myTeam, local, comments, place);
        }

    }

    private void sendReto(int idEquipo, int myTeam, String local, String comments, String place){

        progressDialog.show();

            EquipoSolicitudRetarRequest equipoSolicitudRetarRequest = new EquipoSolicitudRetarRequest();
            equipoSolicitudRetarRequest.setIdEquipo(idEquipo);
            equipoSolicitudRetarRequest.setIdEquipoRetador(myTeam);
            equipoSolicitudRetarRequest.setFechaReto(DateUtils.getDatetime(etxt_datetime_challenge));
            equipoSolicitudRetarRequest.setNombreSede(local);
            equipoSolicitudRetarRequest.setComentarios(comments);
            equipoSolicitudRetarRequest.setUbigeo(place);

            AuthServicesAPI servicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
            Call<OperationResult> call = servicesAPI.challengeTeam(equipoSolicitudRetarRequest);
            call.enqueue(new Callback<OperationResult>() {
                @Override
                public void onResponse(Call<OperationResult> call, Response<OperationResult> response) {
                    if(response.isSuccessful()){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }

                        OperationResult operationResult = response.body();

                        if(operationResult.isStatus()){
                            Toast.makeText(ctx, "¡Solicitud Enviada!", Toast.LENGTH_LONG).show();
                            finish();
                        }else{
                            showResultRequestChallengeFail(operationResult.getErrors());
                        }
                    }else{
                        showInternalError();
                    }
                }

                @Override
                public void onFailure(Call<OperationResult> call, Throwable t) {
                    showInternalError();
                }
            });
    }

    private void setUpToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("Retar");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showInternalError() {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("ERROR", R.string.internal_error);
    }

    public void showResultRequestChallengeFail(List<String> errors) {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("Advertencia", errors.get(0));
    }

    private void etxt_place_click(){
        Gson gson = UtilAPI.getGsonSync();
        String jsonLugares = getIntent().getExtras().getString(KEY_PLACES_TEAM);
        Type typeLugares = new TypeToken<ArrayList<EquipoLugar>>(){}.getType();
        ArrayList<EquipoLugar> equipoLugares = gson.fromJson(jsonLugares, typeLugares);

        final ArrayAdapter<SimpleItem> adapter = new ArrayAdapter<SimpleItem>(ctx, R.layout.layout_item_ubigeo_select);
        for(EquipoLugar equipoLugar : equipoLugares){
            adapter.add(new SimpleItem(equipoLugar.getUbigeo(), equipoLugar.getNombreUbigeo()));
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle("Seleccione");
        builder.setAdapter(adapter, null);
        builder.setPositiveButton("OK",  null);
        builder.setNegativeButton("Cancelar", null);

        final AlertDialog dialog = builder.create();
        dialog.getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        dialog.show();

        if(etxt_place.getTag() != null) {
            String code = etxt_place.getTag().toString();

            for (int i = 0; i < adapter.getCount(); i++) {
                SimpleItem simpleItem = adapter.getItem(i);

                if (simpleItem.getCode().equalsIgnoreCase(code)) {
                    dialog.getListView().setItemChecked(i, true);
                    break;
                }
            }
        }

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int position = dialog.getListView().getCheckedItemPosition();

                if(position != AdapterView.INVALID_POSITION){
                    SimpleItem simpleItem = adapter.getItem(position);
                    etxt_place.setText(simpleItem.getDescription());
                    etxt_place.setTag(simpleItem.getCode());
                } else {
                    etxt_place.setText("");
                    etxt_place.setTag(null);
                }

                dialog.dismiss();
            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_send_request_challenge:
                btn_send_request_challenge_click();
                break;
            case R.id.etxt_place:
                etxt_place_click();
                break;
        }
    }
}
