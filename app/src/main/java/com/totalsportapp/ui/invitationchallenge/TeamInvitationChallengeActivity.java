package com.totalsportapp.ui.invitationchallenge;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.totalsportapp.R;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.UtilAPI;
import com.totalsportapp.data.api.model.InvitacionReto;
import com.totalsportapp.data.api.model.InviteMemberResponse;
import com.totalsportapp.data.api.model.OperationResult;
import com.totalsportapp.data.api.model.RequestSolicitudesModel;
import com.totalsportapp.data.api.model.SolicitudEquipoAPI;
import com.totalsportapp.data.db.model.NivelEquipo;
import com.totalsportapp.data.prefs.PreferencesHelper;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.ui.main.MainActivity;
import com.totalsportapp.ui.teamprofile.TeamProfileActivity;
import com.totalsportapp.ui.teamprofile.TeamProfileInvitationActivity;
import com.totalsportapp.ui.userprofile.UserProfileActivity;
import com.totalsportapp.util.GlideApp;
import com.totalsportapp.util.NetworkUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.totalsportapp.util.AppConstants.DATETIME_FORMAT_CLIENT;

public class TeamInvitationChallengeActivity extends BaseAppCompatActivity {

    //region controls
    private Toolbar toolbar;
    private LinearLayout ll_container_invitation_challenge;
    //end

    public static final int CODE_REQUEST_JOIN_TEAM = 4154;
    public static final int CODE_REQUEST_CHALLENGE_TEAM = 4156;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invitation_challenge);

        onCreateBase();

        findControls();

        configureControls();

        if(NetworkUtils.isNetworkConnected(ctx)){
            loadInvitationRequest();
        }else{
            showDisconnectedNetwork();
            onBackPressed();
        }
    }

    private void findControls(){
        toolbar = findViewById(R.id.toolbar);
        ll_container_invitation_challenge = findViewById(R.id.ll_container_invitation_challenge);
    }

    private void configureControls(){
        setUpToolbar();
    }

    private void loadInvitationRequest(){
        progressDialog.show();

        AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<ArrayList<InvitacionReto>> call = authServicesAPI.listChallengeRequest();
        call.enqueue(new Callback<ArrayList<InvitacionReto>>() {
            @Override
            public void onResponse(Call<ArrayList<InvitacionReto>> call, Response<ArrayList<InvitacionReto>> response) {
                if(response.isSuccessful()) {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    ll_container_invitation_challenge.removeAllViews();

                    ArrayList<InvitacionReto> requestInvitation = new ArrayList<>();
                    requestInvitation = response.body();

                    if(requestInvitation.size()!= 0){
                        setItemLayout(SolicitudEquipoAPI.TIPO_SOLICITUD_RETAR, requestInvitation);

                    }else{
                        setEmptyLayout(SolicitudEquipoAPI.TIPO_SOLICITUD_RETAR);
                    }


                } else {
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<InvitacionReto>> call, Throwable t) {
                showInternalError();
            }
        });
    }

    private void setUpToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("Retos");
    }

    private void setEmptyLayout(int typeList){
        View layout_empty = LayoutInflater.from(ctx).inflate(R.layout.layout_empty_request, null);
        TextView txtv_empty_request = layout_empty.findViewById(R.id.txtv_empty_request);
        txtv_empty_request.setText(R.string.empty_invitation_request);
        ll_container_invitation_challenge.addView(layout_empty);
    }

    private void setItemLayout(final int typeList, List<InvitacionReto> items){

        for(final InvitacionReto item : items){
            View layout = LayoutInflater.from(ctx).inflate(R.layout.layout_item_request, null);
            ImageView imgv_profile_photo = layout.findViewById(R.id.imgv_profile_photo);
            TextView txtv_title = layout.findViewById(R.id.txtv_title);
            TextView txtv_subtitle = layout.findViewById(R.id.txtv_subtitle);
            Button btn_accept_request = layout.findViewById(R.id.btn_accept_request);
            Button btn_deny_request = layout.findViewById(R.id.btn_deny_request);

            imgv_profile_photo.setClipToOutline(true);
            imgv_profile_photo.setVisibility(View.GONE);

            txtv_title.setText(item.getNombreEquipo());

            if(typeList == SolicitudEquipoAPI.TIPO_SOLICITUD_RETAR){

                Locale spanish = new Locale("es", "ES");
                String fechaHora = item.getFechaReto().format(DATETIME_FORMAT_CLIENT, spanish);
                txtv_subtitle.setText(fechaHora);
               // txtv_subtitle.setText(item.getFechaReto());
             /*   if(item.getNombreSede() != null && item.getNombreSede().isEmpty() == false){
                    txtv_subtitle.setText(txtv_subtitle.getText() + "\n" + "Sede: " + item.getNombreSede());
                }*/
            }

            ll_container_invitation_challenge.addView(layout);

            btn_accept_request.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    confirmAlert("¿Aceptar reto?", new ExecuteParam() {
                        @Override
                        public void onExecute() {
                            requestTeamResponse(item.getIdMiembroInvitacion(), 1);
                        }
                    });
                }
            });
            btn_deny_request.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    confirmAlert("¿Rechazar reto?", new ExecuteParam() {
                        @Override
                        public void onExecute() {
                            requestTeamResponse(item.getIdMiembroInvitacion(), 2 );
                        }
                    });
                }
            });

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(typeList == SolicitudEquipoAPI.TIPO_SOLICITUD_RETAR){
                        Gson gson = UtilAPI.getGsonSync();

                        Intent i = new Intent(ctx, TeamProfileInvitationActivity.class);
                        i.putExtra(TeamProfileInvitationActivity.KEY_TYPE_TEAM_PROFILE, TeamProfileActivity.TYPE_PROFILE_CHALLENGE_TEAM);
                        i.putExtra(TeamProfileInvitationActivity.KEY_TEAM_ID, item.getIdEquipoRetador());
                        i.putExtra(TeamProfileInvitationActivity.KEY_TEAM_NAME, item.getNombreEquipo());
                        i.putExtra(TeamProfileInvitationActivity.KEY_TEAM_RETO_NAME, item.getNombreEquipoRetador());
                        i.putExtra(TeamProfileInvitationActivity.KEY_REQUEST_DATA, gson.toJson(item));
                        startActivityForResult(i, CODE_REQUEST_CHALLENGE_TEAM);
                    }
                }
            });
        }
    }

    private void requestTeamResponse(int idMiembroInvitacion, final int responseInvitation){

        progressDialog.show();
        InviteMemberResponse inviteMemberResponse = new InviteMemberResponse();
        inviteMemberResponse.setIdMiembroInvitacion(idMiembroInvitacion);
        inviteMemberResponse.setResponse(responseInvitation);

        AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<OperationResult> call = authServicesAPI.challengeInviteResponse(inviteMemberResponse);
        call.enqueue(new Callback<OperationResult>() {
            @Override
            public void onResponse(Call<OperationResult> call, Response<OperationResult> response) {
                if (response.isSuccessful()){
                    OperationResult op = response.body();
                    if(op.isStatus()){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }

                        loadInvitationRequest();

                        if(responseInvitation == 1){

                            String channelId = getString(R.string.default_notification_channel_id);
                            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext(), channelId)
                                    .setSmallIcon(R.drawable.ic_notification)
                                    .setContentTitle("Total Sport")
                                    .setContentText("Te has unido a un nuevo reto")
                                    .setAutoCancel(true)
                                    .setSound(defaultSoundUri);

                            Intent i = new Intent(getApplicationContext(), MainActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.putExtra(MainActivity.KEY_TAB_LOAD, 1);
                            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0 /* Request code */, i, PendingIntent.FLAG_ONE_SHOT);
                            notificationBuilder.setContentIntent(pendingIntent);
                            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                NotificationChannel channel = new NotificationChannel(channelId, "Channel human readable title", NotificationManager.IMPORTANCE_DEFAULT);
                                notificationManager.createNotificationChannel(channel);
                            }

                            PreferencesHelper pref = PreferencesHelper.getInstance();
                            int notificacionId = pref.getNotificationId() + 1;
                            pref.setNotificationId(notificacionId);
                            notificationManager.notify(notificacionId, notificationBuilder.build());

                        }

                    }else{
                        showResultSaveResponseFail(op.getErrors());
                    }
                }else{
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<OperationResult> call, Throwable t) {
                showInternalError();
            }
        });
    }

    public void showResultSaveResponseFail(List<String> errors) {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert(errors.get(0));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showInternalError() {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("ERROR", R.string.internal_error);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && (requestCode == CODE_REQUEST_CHALLENGE_TEAM || requestCode == CODE_REQUEST_JOIN_TEAM)){
            loadInvitationRequest();
        }
    }
}
