package com.totalsportapp.ui.teamcalendar;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.totalsportapp.R;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.model.CalendarioEquipoAPI;
import com.totalsportapp.data.api.model.RequestCalendarModel;
import com.totalsportapp.data.prefs.PreferencesHelper;
import com.totalsportapp.extra.AppService;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.ui.BaseFragment;
import com.totalsportapp.ui.team.TeamsTabFragment;
import com.totalsportapp.ui.teammemberslocation.TeamMembersMapsActivity;
import com.totalsportapp.ui.teamrequests.RequestInterface;
import com.totalsportapp.ui.teamrequests.TeamRequestsActivity;
import com.totalsportapp.util.NetworkUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeamCalendarActivityPrueba extends BaseFragment {

    //region toolbar
    private ImageView imgv_icon_joined_calendar;
    private ImageView imgv_icon_joined_time;
    private ImageView imgv_icon_joined_place;

    private ImageView imgv_icon_challenged_calendar;
    private ImageView imgv_icon_challenged_time;
    private ImageView imgv_icon_challenged_place;

    private LinearLayout ll_container_joined_team;
    private LinearLayout ll_container_challenged_myteams;
    //endreigon

    public static TeamCalendarActivityPrueba newInstance() {
        return new TeamCalendarActivityPrueba();
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View layout = inflater.inflate(R.layout.activity_team_calendar, container, false);

        onCreateBase();

        findControls(layout);

        configureControls();

        if(NetworkUtils.isNetworkConnected(ctx)){
            loadCalendar();
        }else{
            showDisconnectedNetwork();
        }

        return layout;
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void findControls(View layout){
        imgv_icon_joined_calendar = layout.findViewById(R.id.imgv_icon_joined_calendar);
        imgv_icon_joined_time = layout.findViewById(R.id.imgv_icon_joined_time);
        imgv_icon_joined_place = layout.findViewById(R.id.imgv_icon_joined_place);

        imgv_icon_challenged_calendar = layout.findViewById(R.id.imgv_icon_challenged_calendar);
        imgv_icon_challenged_time = layout.findViewById(R.id.imgv_icon_challenged_time);
        imgv_icon_challenged_place = layout.findViewById(R.id.imgv_icon_challenged_place);

        ll_container_joined_team = layout.findViewById(R.id.ll_container_joined_team);
        ll_container_challenged_myteams = layout.findViewById(R.id.ll_container_challenged_myteams);
    }

    private void configureControls(){

        imgv_icon_joined_calendar.setVisibility(View.GONE);
        imgv_icon_joined_time.setVisibility(View.GONE);
        imgv_icon_joined_place.setVisibility(View.GONE);

        imgv_icon_challenged_calendar.setVisibility(View.GONE);
        imgv_icon_challenged_time.setVisibility(View.GONE);
        imgv_icon_challenged_place.setVisibility(View.GONE);
    }

    private void loadCalendar(){
       AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<RequestCalendarModel> call = authServicesAPI.calendar(1,1    );
        call.enqueue(new Callback<RequestCalendarModel>() {
            @Override
            public void onResponse(Call<RequestCalendarModel> call, Response<RequestCalendarModel> response) {
                if(response.isSuccessful()){

                    //ArrayList<CalendarioEquipoAPI> data = response.body();

                    ArrayList<CalendarioEquipoAPI> joined = new ArrayList<>();
                    ArrayList<CalendarioEquipoAPI> challenged = new ArrayList<>();

                    for (int i = 0; i <response.body().getCalendarioEquipoCapitan().getListCalendarioCapitan().size() ; i++) {
                        challenged.add(response.body().getCalendarioEquipoCapitan().getListCalendarioCapitan().get(i));
                    }

                    for (int i = 0; i <response.body().getCalendarioEquipoOtros().getListCalendarioEquipoOtros().size() ; i++) {
                        joined.add(response.body().getCalendarioEquipoOtros().getListCalendarioEquipoOtros().get(i));
                    }
                    //data.add(calendarioEquipoAPI);
                    //data.add(newcalendarioEquipoAPI);

                  /*  for(CalendarioEquipoAPI item : data){
                        if(item.getTipoListado() == CalendarioEquipoAPI.CALENDARIO_TIPO_UNIDO){
                            joined.add(item);
                        } else if(item.getTipoListado() == CalendarioEquipoAPI.CALENDARIO_TIPO_RETO){
                            challenged.add(item);
                        }
                    }
*/
                    if(joined.size() == 0){
                        setEmptyLayout(CalendarioEquipoAPI.CALENDARIO_TIPO_UNIDO);
                    }else{
                        imgv_icon_joined_calendar.setVisibility(View.VISIBLE);
                        imgv_icon_joined_time.setVisibility(View.VISIBLE);
                        imgv_icon_joined_place.setVisibility(View.VISIBLE);

                        setLayoutItem(joined);
                    }

                    if(challenged.size() == 0){
                        setEmptyLayout(CalendarioEquipoAPI.CALENDARIO_TIPO_RETO);
                    }else{
                        imgv_icon_challenged_calendar.setVisibility(View.VISIBLE);
                        imgv_icon_challenged_time.setVisibility(View.VISIBLE);
                        imgv_icon_challenged_place.setVisibility(View.VISIBLE);

                        setLayoutItem(challenged);
                    }
                }else{
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<RequestCalendarModel> call, Throwable t) {
                showInternalError();
            }
        });
    }

    private void setEmptyLayout(int typeList){
        View layout_empty = LayoutInflater.from(ctx).inflate(R.layout.layout_empty_calendar, null);
        //TextView txtv_empty_request = layout_empty.findViewById(R.id.txtv_empty_request);
        if(typeList == CalendarioEquipoAPI.CALENDARIO_TIPO_UNIDO){
            //txtv_empty_request.setText(R.string.empty_challenge_request);
            ll_container_joined_team.addView(layout_empty);
        }else if(typeList == CalendarioEquipoAPI.CALENDARIO_TIPO_RETO){
            //txtv_empty_request.setText(R.string.empty_join_request);
            ll_container_challenged_myteams.addView(layout_empty);
        }
    }

    private void setLayoutItem(ArrayList<CalendarioEquipoAPI> data){
        for(int i = 0; i < data.size(); i++){

            final CalendarioEquipoAPI item = data.get(i);

            View layout = LayoutInflater.from(ctx).inflate(R.layout.layout_calendar_item, null);
            LinearLayout ll_container_item = layout.findViewById(R.id.ll_container_item);
            TextView txtv_date = layout.findViewById(R.id.txtv_date);
            TextView txtv_hour = layout.findViewById(R.id.txtv_hour);
            TextView txtv_location = layout.findViewById(R.id.txtv_location);
            TextView txtv_teams = layout.findViewById(R.id.tvTeamName);
            View vw_separator_a = layout.findViewById(R.id.vw_separator_a);
            View vw_separator_b = layout.findViewById(R.id.vw_separator_b);
            ImageView imgv_icon_arrow = layout.findViewById(R.id.imgv_icon_arrow);

           /* ll_container_item.setBackground(ContextCompat.getDrawable(ctx, R.drawable.bg_calendar_item_odd));
            int colorWhite = ContextCompat.getColor(ctx, R.color.white);
            txtv_date.setTextColor(colorWhite);
            txtv_hour.setTextColor(colorWhite);
            txtv_location.setTextColor(colorWhite);
            vw_separator_a.setBackgroundColor(colorWhite);
            vw_separator_b.setBackgroundColor(colorWhite);
            imgv_icon_arrow.setImageResource(R.drawable.calendar_arrow_item_white);*/

            if(i % 2 == 0){
                ll_container_item.setBackground(ContextCompat.getDrawable(ctx, R.drawable.bg_calendar_item_odd));
                int colorWhite = ContextCompat.getColor(ctx, R.color.white);
                txtv_date.setTextColor(colorWhite);
                txtv_hour.setTextColor(colorWhite);
                txtv_location.setTextColor(colorWhite);
                vw_separator_a.setBackgroundColor(colorWhite);
                vw_separator_b.setBackgroundColor(colorWhite);
                imgv_icon_arrow.setImageResource(R.drawable.calendar_arrow_item_white);
            } else{
                ll_container_item.setBackground(ContextCompat.getDrawable(ctx, R.drawable.bg_calendar_item_even));
                int colorWhite = ContextCompat.getColor(ctx, R.color.white);
                txtv_date.setTextColor(colorWhite);
                txtv_hour.setTextColor(colorWhite);
                txtv_location.setTextColor(colorWhite);
                vw_separator_a.setBackgroundColor(colorWhite);
                vw_separator_b.setBackgroundColor(colorWhite);
                imgv_icon_arrow.setImageResource(R.drawable.calendar_arrow_item_white);
               /* int colorBlack = ContextCompat.getColor(ctx, R.color.black);
                txtv_date.setTextColor(colorBlack);
                txtv_hour.setTextColor(colorBlack);
                txtv_location.setTextColor(colorBlack);
                vw_separator_a.setBackgroundColor(colorBlack);
                vw_separator_b.setBackgroundColor(colorBlack);
                imgv_icon_arrow.setImageResource(R.drawable.calendar_arrow_item_black);*/
            }

            //layout.setTag(item.getiIdEquipoSolicitudRetar());
            Locale spanish = new Locale("es", "ES");
            txtv_date.setText(item.getFechaReto().format("WWW. DD MMM.", spanish));
            txtv_hour.setText(item.getFechaReto().format("hh12:mm a", spanish));
            txtv_location.setText(item.getUbigeo());
            txtv_teams.setText(item.getNombreEquipo() + " vs " + item.getNombreRetador());

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //Toast.makeText(ctx, "" + item.getIdEquipoSolicitudRetar(), Toast.LENGTH_SHORT).show();

                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm");

                    Date date1 = null;
                    Date date2 = null;
                    try {
                        date1 = format.parse(item.getFechaReto().toString());
                        date2 = Calendar.getInstance().getTime();

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    Log.e("Servicio date 1", date1.toString());
                    Log.e("Celular date 2", date2.toString());


                    if (date1.compareTo(date2) > 0) {

                        long difference = date2.getTime() - date1.getTime();
                        Log.e("differente", String.valueOf(difference));
                        Intent i = new Intent(ctx, TeamMembersMapsActivity.class);
                        i.putExtra(TeamMembersMapsActivity.KEY_TEAM_ID, item.getIdEquipo());
                        i.putExtra(TeamMembersMapsActivity.KEY_CHALLENGE_ID, item.getIdEquipoSolicitudRetar());
                        i.putExtra(TeamMembersMapsActivity.TIPE_LIST, item.getTipoListado());
                        startActivity(i);

                        if(difference <= 10800000){
                            Log.e("differente", "Menor a 3 horas");
                            PreferencesHelper.getInstance().setUbicationKey(true);
                            Intent iServices = new Intent(getContext(), AppService.class);
                            iServices.putExtra("idReto", item.getIdEquipoSolicitudRetar());
                            iServices.putExtra("idEquipo", item.getIdEquipo());
                            ContextCompat.startForegroundService(getContext(), iServices);
                            // Intent
                        }
                    } else {
                        Toast.makeText(ctx, "Este partido ya ha culminado", Toast.LENGTH_SHORT).show();
                    }

                }
            });


            if(item.getTipoListado() == CalendarioEquipoAPI.CALENDARIO_TIPO_UNIDO){
                ll_container_joined_team.addView(layout);
            } else if(item.getTipoListado() == CalendarioEquipoAPI.CALENDARIO_TIPO_RETO){
                ll_container_challenged_myteams.addView(layout);
            }
        }
    }

    public void showInternalError() {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("ERROR", R.string.internal_error);
    }
    protected void showDisconnectedNetwork(){
        Toast.makeText(ctx, R.string.disconnected_network, Toast.LENGTH_LONG).show();
    }


    @Override
    public void onStop() {
        super.onStop();

    }

    public void updateList() {
        loadCalendar();
    }
}
