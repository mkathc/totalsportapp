package com.totalsportapp.ui;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.totalsportapp.R;
import com.totalsportapp.util.FileAppUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static android.app.Activity.RESULT_OK;
import static com.totalsportapp.util.PhotoUtils.getOriginal;

public class BaseFragment  extends Fragment {

    protected Context ctx;

    protected static final int CAMERA_PERMISSION = 3;
    protected static final int GALLERY_PERMISION = 4;

    protected final static int REQUEST_TAKE_PHOTO = 1050;
    protected final static int PICK_PHOTO_CODE = 1051;

    protected String mCurrentPhotoPath;

    protected ProgressDialog progressDialog;

    public void onCreateBase() {
        ctx = getContext();

        progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("Espere por favor...");
        progressDialog.setCancelable(false);
    }

    protected void simpleAlert(String title, int resourceString) {
        String messague = getResources().getString(resourceString);
        this.simpleAlert(title, messague, null);
    }

    protected void simpleAlert(String messague) {
        this.simpleAlert(null, messague, null);
    }

    protected void simpleAlert(String title, String messague) {
        this.simpleAlert(title, messague, null);
    }

    protected void simpleAlert(String title, String messague, final BaseAppCompatActivity.ExecuteParam onOkAlert) {
        AlertDialog alertDialog = new AlertDialog.Builder(ctx).create();

        if (title != null) {
            alertDialog.setTitle(title);
        }

        alertDialog.setMessage(messague);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        if(onOkAlert != null){
                            onOkAlert.onExecute();
                        }
                    }
                });
        alertDialog.show();
    }

    protected void dispatchCamera() {
        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            //todo revisar si funciona el requestPermissions para versiones anteriores a android v5
            requestPermissions(new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION);
        } else {
            launchCamera();
        }
    }

    protected void launchCamera(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = FileAppUtils.createImageFile(ctx);
                mCurrentPhotoPath = photoFile.getAbsolutePath();
            } catch (IOException ex) {
                simpleAlert("Error al acceder al SD Card");
            }

            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(ctx,"com.totalsportapp.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                }
                else {
                    List<ResolveInfo> resInfoList = getActivity().getPackageManager().queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);

                    for (ResolveInfo resolveInfo : resInfoList) {
                        String packageName = resolveInfo.activityInfo.packageName;
                        getActivity().grantUriPermission(packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }
                }

                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    protected void dispatchGallery(){
        if (ContextCompat.checkSelfPermission(ctx, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            //todo revisar si funciona el requestPermissions para versiones anteriores a android v5
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, GALLERY_PERMISION);
        }else{
            launchGallery();
        }
    }

    protected void launchGallery(){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
            startActivityForResult(intent, PICK_PHOTO_CODE);
        }
    }

    protected void launchPhotoPicker(){
        final CharSequence[] items = {"Cámara", "Galería"};
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx)
                .setTitle("Seleccione")
                .setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int position) {
                        switch (position){
                            case 0:
                                dispatchCamera();
                                break;
                            case 1:
                                dispatchGallery();
                                break;
                        }
                    }
                });

        builder.create().show();
    }

    protected void showDialogConfirmPhoto(final ExecuteParam executeParam){
        final AppCompatDialog dialogPhotoSelected = new AppCompatDialog(ctx, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialogPhotoSelected.setContentView(R.layout.layout_photo_selected);
        Button btnOkPhotoSelected = dialogPhotoSelected.findViewById(R.id.btn_ok_photo_selected);
        Button btnCancelPhotoSelected = dialogPhotoSelected.findViewById(R.id.btn_cancel_photo_selected);
        ImageView imgvPhotoSelected = dialogPhotoSelected.findViewById(R.id.imgv_photo_selected);
        btnCancelPhotoSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogPhotoSelected.dismiss();
            }
        });
        btnOkPhotoSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogPhotoSelected.dismiss();
                if(executeParam != null){
                    executeParam.onExecute();
                }
            }
        });
        Bitmap bpPhoto = getOriginal(ctx, mCurrentPhotoPath);
        imgvPhotoSelected.setImageBitmap(bpPhoto);
        dialogPhotoSelected.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            showDialogPhotoSelected();

        } else if(requestCode == PICK_PHOTO_CODE && resultCode == RESULT_OK){
            Uri photoUri = data.getData();
            mCurrentPhotoPath = FileAppUtils.getRealPathFromURI(photoUri, ctx);

            showDialogPhotoSelected();
        }
    }

    protected void showDialogPhotoSelected(){
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    launchCamera();
                } else {
                    simpleAlert("Por favor brinda permisos para usar la cámara");
                }
                break;
            case GALLERY_PERMISION:
                if(grantResults.length > 0 && grantResults[0]  == PackageManager.PERMISSION_GRANTED){
                    launchGallery();
                }else{
                    simpleAlert( "Por favor brinda permisos para acceder a la galería");
                }
                break;
        }
    }

    public interface ExecuteParam {
        void onExecute();
    }
}
