package com.totalsportapp.ui.team;

import android.app.Activity;
import android.app.Dialog;
import android.app.ExpandableListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Spinner;
import android.widget.Toast;

import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.totalsportapp.MainApplication;
import com.totalsportapp.R;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.db.model.EquipoLugar;
import com.totalsportapp.data.db.model.Ubigeo;
import com.totalsportapp.data.prefs.PreferencesHelper;
import com.totalsportapp.ui.jointeam.JoinTeamActivity;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomDialogUbigeo extends Dialog implements View.OnClickListener {

    private Activity activity;

    private boolean isFirstDepartamento = true;
    private boolean isFirstProvincia = true;

    private Spinner spinner_departamento;
    private Spinner spinner_provincia;
    private ExpandableLinearLayout ell_locations;
    private Button btn_aceptar;
    private Button check_all_location;
    private Button clean_all_location;
    ArrayList<Ubigeo> departamentos;
    ArrayList<Ubigeo> provincias;
    ArrayList<Ubigeo> distritos;
    CreateTeamInterface createTeamInterface;
    List<EquipoLugar> listLugares;

    public CustomDialogUbigeo(Activity activity, CreateTeamInterface teamInterface, List<EquipoLugar> list) {
        super(activity);
        this.activity = activity;
        this.createTeamInterface = teamInterface;
        this.listLugares = list;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.custom_dialog_ubigeo);

        departamentos = new ArrayList<Ubigeo>();
        provincias = new ArrayList<Ubigeo>();
        distritos = new ArrayList<Ubigeo>();


        spinner_departamento = findViewById(R.id.spinner_departamento);
        spinner_provincia = findViewById(R.id.spinner_provincia);
        ell_locations = findViewById(R.id.ell_locations);
        btn_aceptar = findViewById(R.id.btn_aceptar);
        check_all_location = findViewById(R.id.check_all_locations);
        clean_all_location = findViewById(R.id.clean_all_locations);
        check_all_location.setOnClickListener(this);
        clean_all_location.setOnClickListener(this);
        btn_aceptar.setOnClickListener(this);


        setDepartamentoSpinner();
        setProvinciaSpinner();
        setUpLocationFilter();

        if (listLugares != null) {
            for (int i = 0; i < listLugares.size(); i++) {
                for (int j = 0; j < ell_locations.getChildCount(); j++) {
                    CheckBox chk = (CheckBox) ell_locations.getChildAt(j);
                    if (chk.getTag().toString().equals(listLugares.get(i).getUbigeo())) {
                        chk.setChecked(true);
                    }
                }
            }
        }
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btn_aceptar:

                ArrayList<Ubigeo> list = getAllLocations();
                createTeamInterface.onCLickLocation(list);
                dismiss();
                break;
            case R.id.clean_all_locations:

                cleanCheckboxLocation();
                break;
            case R.id.check_all_locations:

                checkedAllCheckboxLocation();
                break;
            default:
                Toast.makeText(activity, "Sin ubigeo", Toast.LENGTH_SHORT).show();

                break;
        }

    }


    private void cleanCheckboxLocation() {

        for (int i = 0; i < ell_locations.getChildCount(); i++) {
            CheckBox chk = (CheckBox) ell_locations.getChildAt(i);
            chk.setChecked(false);
        }
    }


    private void checkedAllCheckboxLocation() {

        for (int i = 0; i < ell_locations.getChildCount(); i++) {
            CheckBox chk = (CheckBox) ell_locations.getChildAt(i);
            chk.setChecked(true);
        }
    }


    private ArrayList<Ubigeo> getAllLocations() {
        ArrayList<Ubigeo> ubigeoSelected = new ArrayList<>();

        for (int i = 0; i < ell_locations.getChildCount(); i++) {
            CheckBox chk = (CheckBox) ell_locations.getChildAt(i);
            if (chk.isChecked()) {
                for (int j = 0; j < distritos.size(); j++) {
                    if (chk.getTag().toString().equals(distritos.get(j).getCodigo())) {
                        ubigeoSelected.add(distritos.get(j));
                    }
                }
            }
        }
        return ubigeoSelected;
    }


    private void setDepartamentoSpinner() {

        final List<String> list = new ArrayList<String>();

        departamentos = PreferencesHelper.getInstance().getArrayListDepartamentos(PreferencesHelper.ARRAY_DEPARTAMENTOS);

        for (int i = 0; i < departamentos.size(); i++) {
            list.add(departamentos.get(i).getNombre());
        }


        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_departamento.setAdapter(dataAdapter);

        spinner_departamento.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (isFirstDepartamento) {
                    isFirstDepartamento = false;
                } else {
                    getProvinciaById(spinner_departamento.getItemAtPosition(i).toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_departamento.setSelection(14);


    }

    public void getProvinciaById(String departamento) {

        String id = "150000";

        for (int i = 0; i < departamentos.size(); i++) {
            if (departamentos.get(i).getNombre().equals(departamento)) {
                id = departamentos.get(i).getCodigo();
            }
        }

        AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<ArrayList<Ubigeo>> call = authServicesAPI.getProvincias(id);
        call.enqueue(new Callback<ArrayList<Ubigeo>>() {
            @Override
            public void onResponse(Call<ArrayList<Ubigeo>> call, Response<ArrayList<Ubigeo>> response) {
                if (response.isSuccessful()) {
                    provincias = response.body();
                    setNewProvincias(response.body());
                } else {
                    Toast.makeText(activity, "No se encontraron provincias, intente nuevamente", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Ubigeo>> call, Throwable t) {
                Toast.makeText(activity, "Ha ocurrido un error al obtener provincias", Toast.LENGTH_SHORT).show();
            }
        });
    }


    private void setProvinciaSpinner() {

        final List<String> list = new ArrayList<String>();

        provincias = PreferencesHelper.getInstance().getArrayListProvincias(PreferencesHelper.ARRAY_PROVINCIAS);

        for (int i = 0; i < provincias.size(); i++) {
            list.add(provincias.get(i).getNombre());
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_provincia.setAdapter(dataAdapter);

        spinner_provincia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (isFirstProvincia) {
                    isFirstProvincia = false;
                } else {
                    getDistritoById(spinner_provincia.getItemAtPosition(i).toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        spinner_provincia.setSelection(7);


    }

    private void setNewProvincias(ArrayList<Ubigeo> listaProvincias) {
        final List<String> list = new ArrayList<String>();

        for (int i = 0; i < listaProvincias.size(); i++) {
            list.add(listaProvincias.get(i).getNombre());
        }

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(activity,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_provincia.setAdapter(dataAdapter);

        spinner_provincia.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (isFirstProvincia) {
                    isFirstProvincia = false;
                } else {
                    getDistritoById(spinner_provincia.getItemAtPosition(i).toString());
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }


    public void getDistritoById(String provincia) {

        String id = "150100";

        for (int i = 0; i < provincias.size(); i++) {
            if (provincias.get(i).getNombre().equals(provincia)) {
                id = provincias.get(i).getCodigo();
            }
        }

        AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<ArrayList<Ubigeo>> call = authServicesAPI.getDistritos(id);
        call.enqueue(new Callback<ArrayList<Ubigeo>>() {
            @Override
            public void onResponse(Call<ArrayList<Ubigeo>> call, Response<ArrayList<Ubigeo>> response) {
                if (response.isSuccessful()) {
                    distritos = response.body();
                    ell_locations.removeAllViews();

                    for (Ubigeo ubigeo : response.body()) {
                        CheckBox checkBox = (CheckBox) getLayoutInflater().inflate(R.layout.layout_item_filter, null);
                        checkBox.setText(ubigeo.getNombre());
                        checkBox.setTag(ubigeo.getCodigo());
                        ell_locations.addView(checkBox);
                    }
                } else {
                    Toast.makeText(activity, "No se encontraron distritos, intente nuevamente", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Ubigeo>> call, Throwable t) {
                Toast.makeText(activity, "Ha ocurrido un error al obtener los distritos", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setUpLocationFilter() {

        //List<Equipo> equipos = getDaoSession().getEquipoDao().queryBuilder().where(EquipoDao.Properties.EsLider.eq(true)).list();

        List<Ubigeo> ubigeos = ((MainApplication) activity.getApplication()).getDaoSession().getUbigeoDao().loadAll();
        distritos = (ArrayList<Ubigeo>) ((MainApplication) activity.getApplication()).getDaoSession().getUbigeoDao().loadAll();

        for (Ubigeo ubigeo : ubigeos) {
            CheckBox checkBox = (CheckBox) getLayoutInflater().inflate(R.layout.layout_item_filter, null);
            checkBox.setText(ubigeo.getNombre());
            checkBox.setTag(ubigeo.getCodigo());

            /*outerloop:
            for(Equipo equipo : equipos){
                for(EquipoLugar equipoLugar : equipo.getLugares()){
                    if(equipoLugar.getUbigeo().equalsIgnoreCase(ubigeo.getCodigo())){
                        checkBox.setChecked(true);
                        break outerloop;
                    }
                }
            }*/

            ell_locations.addView(checkBox);
        }
    }


}
