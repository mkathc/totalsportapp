package com.totalsportapp.ui.team;

import com.totalsportapp.data.db.model.Ubigeo;

import java.util.ArrayList;

public interface CreateTeamInterface {

    void onCLickLocation(ArrayList<Ubigeo> list);
}
