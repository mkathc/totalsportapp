package com.totalsportapp.ui.team;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckedTextView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.Toast;

import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.totalsportapp.MainApplication;
import com.totalsportapp.R;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.UtilAPI;
import com.totalsportapp.data.api.model.OperationResult;
import com.totalsportapp.data.api.model.ScheduleAPI;
import com.totalsportapp.data.db.model.Equipo;
import com.totalsportapp.data.db.model.EquipoLugar;
import com.totalsportapp.data.db.model.EquipoMiembro;
import com.totalsportapp.data.db.model.EquipoPreferenciaHoraria;
import com.totalsportapp.data.db.model.NivelEquipo;
import com.totalsportapp.data.db.model.Ubigeo;
import com.totalsportapp.data.db.model.Usuario;
import com.totalsportapp.data.prefs.PreferencesHelper;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.ui.BaseFragment;
import com.totalsportapp.ui.schedule.ScheduleActivity;
import com.totalsportapp.ui.teammembers.ChooseTeamMembersActivity;
import com.totalsportapp.util.FileAppUtils;
import com.totalsportapp.util.GlideApp;
import com.totalsportapp.util.NetworkUtils;
import com.totalsportapp.util.SimpleItem;

import java.io.File;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.totalsportapp.util.AppConstants.BASE_URL_PHOTO_TEAM;
import static com.totalsportapp.util.PhotoUtils.getOriginal;

public class CreateTeamActivity extends BaseAppCompatActivity implements View.OnClickListener , CreateTeamInterface {

    public final static int CODE_TEAM_MEMBERS = 2000;
    public final static int CODE_SCHEDULE_TEAM = 2001;

    public final static String KEY_TEAM_DATA = "KEY_TEAM_ID";

    //region controls
    private EditText etxtPlaces;
    private EditText etxtTeamName;

    private Toolbar toolbar;

    private Spinner spLevel;

    private boolean isFirstDepartamento = true;
    private boolean isFirstProvincia = true;

    private Spinner spinner_departamento;
    private Spinner spinner_provincia;
    private ExpandableLinearLayout ell_locations;

    ArrayList<Ubigeo> departamentos;
    ArrayList<Ubigeo> provincias;
    private ImageView imgv_profile_team_photo;

    private Button btnGoMembers;
    private Button btnSaveTeam;
    private Button btnGoSchedule;
    private Equipo equipo;
    //endregion

    private List<Integer> idMembers;
    private ArrayList<ScheduleAPI> scheduleAPI;

    private int idEquipo = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team);

        onCreateBase();

        idMembers = new ArrayList<>();
        scheduleAPI = new ArrayList<>();

        findControls();

        configureControls();

        if(NetworkUtils.isNetworkConnected(ctx)){
            loadTeam();
        }else{
            showDisconnectedNetwork();
            onBackPressed();
        }
    }

    private void loadTeam(){
        if(idEquipo != 0) {
            progressDialog.show();
            AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
            Call<Equipo> call = authServicesAPI.infoTeam(idEquipo);
            call.enqueue(new Callback<Equipo>() {
                @Override
                public void onResponse(Call<Equipo> call, Response<Equipo> response) {
                    if(response.isSuccessful()){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }

                        equipo = response.body();

                        etxtTeamName.setText(equipo.getNombre());
                        SpinnerAdapter spinnerAdapter = spLevel.getAdapter();

                        for(int i = 0; i < spinnerAdapter.getCount(); i++){
                            if(((SimpleItem)spinnerAdapter.getItem(i)).getCode().equalsIgnoreCase(String.valueOf(equipo.getNivel()))){
                                spLevel.setSelection(i);
                                break;
                            }
                        }

                        for(EquipoPreferenciaHoraria item : equipo.getPreferenciaHorarias()){
                            scheduleAPI.add(new ScheduleAPI(item.getDia(), item.getHora()));
                        }

                        for(EquipoMiembro item : equipo.getMiembros()){
                            idMembers.add(item.getIdUsuario());
                        }

                        List<String> ubigeos = new ArrayList<>();
                        for(EquipoLugar item : equipo.getLugares()){
                            ubigeos.add(item.getUbigeo());
                        }

                        etxtPlaces.setTag(ubigeos);
                        etxtPlaces.setText(equipo.getLugaresText());

                        if(equipo.getFoto() != null){
                            imgv_profile_team_photo.setPadding(0, 0 , 0, 0);
                            String photoUrl =  equipo.getFoto();
                            GlideApp.with(ctx)
                                    .load(photoUrl)
                                    .dontAnimate()
                                    .centerCrop()
                                    .into(imgv_profile_team_photo);
                        }
                    }else{
                        showInternalError();
                    }
                }

                @Override
                public void onFailure(Call<Equipo> call, Throwable t) {
                    showInternalError();
                }
            });
        }
    }

    private void findControls(){
        btnGoMembers = findViewById(R.id.btn_go_members);
        etxtPlaces = findViewById(R.id.etxt_places);
        toolbar = findViewById(R.id.toolbar);
        spLevel = findViewById(R.id.sp_level);
        btnSaveTeam = findViewById(R.id.btn_save_team);
        btnGoSchedule = findViewById(R.id.btn_go_schedule);
        imgv_profile_team_photo = findViewById(R.id.imgv_profile_team_photo);
        etxtTeamName = findViewById(R.id.etxtTeamName);
    }

    private void configureControls() {

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            idEquipo = extras.getInt(KEY_TEAM_DATA, 0);
        }

        setUpToolbar();

        btnGoSchedule.setOnClickListener(this);
        btnSaveTeam.setOnClickListener(this);
        btnGoMembers.setOnClickListener(this);
        imgv_profile_team_photo.setOnClickListener(this);
        imgv_profile_team_photo.setClipToOutline(true);
        etxtPlaces.setOnClickListener(this);
        etxtPlaces.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                return false;
            }
        });

        setUpSpinner();
    }

    private void setUpSpinner(){
        ArrayAdapter<SimpleItem> adapter = new ArrayAdapter<>(ctx, R.layout.simple_spinner_item);
        List<NivelEquipo> nivelesEquipo = NivelEquipo.getNivelesEquipo();

        for(NivelEquipo item : nivelesEquipo){
            adapter.add(new SimpleItem(String.valueOf(item.getId()), item.getDescripcion()));
        }

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spLevel.setAdapter(adapter);
    }

    private void setUpToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        if(idEquipo > 0){
            setTitle("Actualizar Equipo");
        } else {
            setTitle("Crear Equipo");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void imgvProfileTeamPhotoClick(){
        launchPhotoPicker();
    }

    @Override
    protected void showDialogPhotoSelected() {
        showDialogConfirmPhoto(new BaseFragment.ExecuteParam() {
            @Override
            public void onExecute() {
                Bitmap bpPhoto = getOriginal(ctx, mCurrentPhotoPath);
                imgv_profile_team_photo.setPadding(0, 0 , 0, 0);
                imgv_profile_team_photo.setScaleType(ImageView.ScaleType.CENTER_CROP);
                imgv_profile_team_photo.setImageBitmap(bpPhoto);
            }
        });
    }

    private void btnGoMembersClick(){
        Gson gson = new Gson();
        Intent i = new Intent(ctx, ChooseTeamMembersActivity.class);
        i.putExtra(ChooseTeamMembersActivity.KEY_TEAM_MEMBERS_SELECTED, gson.toJson(idMembers));
        startActivityForResult(i, CODE_TEAM_MEMBERS);
    }

    private boolean validateFormEquipo(Equipo equipo){
        if(equipo.getNombre() == null || equipo.getNombre().isEmpty()){
            showFormValidateError("Nombre del Equipo es requerido");
            return false;
        }
        if(equipo.getNivel() == 0){
            showFormValidateError("Nivel del Equipo es requerido");
            return false;
        }
        if(equipo.getLugares() == null || equipo.getLugares().size() == 0){
            showFormValidateError("Agregar un lugar de juego");
            return false;
        }

        if(equipo.getLugares() == null || equipo.getLugares().size() == 0){
            showFormValidateError("Agregar un lugar de juego");
            return false;
        }

        if(equipo.getPreferenciaHorarias() == null || equipo.getPreferenciaHorarias().size() == 0){
            showFormValidateError("Seleccione su preferencia horaria");
            return false;
        }

        return true;
    }

    public void showFormValidateError(String messague) {
        simpleAlert(messague);
    }

    private void btnSaveTeamClick(){
        String nivelSelected = ((SimpleItem)spLevel.getSelectedItem()).getCode();

        Equipo equipo = new Equipo();
        equipo.setNombre(etxtTeamName.getText().toString());
        equipo.setNivel(Integer.parseInt(nivelSelected));

        if(etxtPlaces.getTag() != null) {
            List<String> ubigeosSelected = (List<String>) etxtPlaces.getTag();

            List<EquipoLugar> lugares = new ArrayList<>();

            for(String item : ubigeosSelected){
                lugares.add(new EquipoLugar(0, item));
            }

            equipo.setLugares(lugares);
        }

        if(idMembers.size() > 0){
            List<EquipoMiembro> miembros = new ArrayList<>();
            for(Integer item : idMembers){
                miembros.add(new EquipoMiembro(0, item, 0));
            }
            equipo.setMiembros(miembros);
        }else{
            equipo.setMiembros(new ArrayList<EquipoMiembro>());
        }

        if(scheduleAPI.size() > 0){
            List<EquipoPreferenciaHoraria> preferenciaHorarias = new ArrayList<>();
            for(ScheduleAPI item : scheduleAPI) {
                preferenciaHorarias.add(new EquipoPreferenciaHoraria(0, item.getDia(), item.getHora()));
            }
            equipo.setPreferenciaHorarias(preferenciaHorarias);
        }

        if(validateFormEquipo(equipo) == false){
            return;
        }

        MultipartBody.Part filePart;

        if(mCurrentPhotoPath != null){
            File file = FileAppUtils.prepareFilePhotoToServer(ctx, mCurrentPhotoPath, "photo_team_temp_app", "photoTeam.jpg");
            filePart= MultipartBody.Part.createFormData("filePhoto", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));
        }else{
            RequestBody attachmentEmpty = RequestBody.create(MediaType.parse("text/plain"), "");
            filePart = MultipartBody.Part.createFormData("attachment", "", attachmentEmpty);
        }

        progressDialog.show();
        AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<OperationResult> call;

        if(idEquipo == 0){
            String teamJSON = UtilAPI.getGsonSync().toJson(equipo);
            call = authServicesAPI.createTeam(filePart, UtilAPI.createPartFromString(teamJSON));
        }else{
            equipo.setIdEquipo(idEquipo);
            String teamJSON = UtilAPI.getGsonSync().toJson(equipo);
            call = authServicesAPI.updateTeam(filePart, UtilAPI.createPartFromString(teamJSON));
        }

        call.enqueue(new Callback<OperationResult>() {
            @Override
            public void onResponse(Call<OperationResult> call, Response<OperationResult> response) {
                if(progressDialog.isShowing()){
                    progressDialog.dismiss();
                }

                if(response.isSuccessful()){
                    OperationResult op = response.body();
                    if(op.isStatus()){
                        if(idEquipo == 0){
                            Toast.makeText(ctx, "¡Equipo Creado!", Toast.LENGTH_LONG).show();
                            syncUserApp();
                        }else{
                            Toast.makeText(ctx, "¡Equipo Actualizado!", Toast.LENGTH_LONG).show();
                        }

                        setResult(RESULT_OK);
                        finish();
                    } else {
                       simpleAlert("Advertencia", op.errors.get(0));
                    }
                }else{
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<OperationResult> call, Throwable t) {
                showInternalError();
            }
        });
    }

    public void syncUserApp() {
            AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
            Call<Usuario> call = authServicesAPI.profile();
            call.enqueue(new Callback<Usuario>(){
                @Override
                public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                    if (response.isSuccessful()) {
                        Usuario usuario = response.body();
                        PreferencesHelper.getInstance().setUser(usuario);
                    }
                }

                @Override
                public void onFailure(Call<Usuario> call, Throwable t) {
                }
            });

    }

    public void showInternalError() {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("ERROR", R.string.internal_error);
    }

    private void btnGoScheduleClick(){
        Gson gson = new Gson();
        Intent i = new Intent(ctx, ScheduleActivity.class);
        i.putExtra(ScheduleActivity.KEY_SCHEDULE_TEAM_DATA, gson.toJson(scheduleAPI));
        i.putExtra(ScheduleActivity.KEY_SCHEDULE_TYPE, ScheduleActivity.SCHEDULE_TYPE_TEAM);
        startActivityForResult(i, CODE_SCHEDULE_TEAM);
    }

    private void etxtPlacesClick(){
        List<Ubigeo> ubigeos = ((MainApplication)getApplication()).getDaoSession().getUbigeoDao().loadAll();
        final boolean[] ubigeoPositionSelected =  new boolean[ubigeos.size()];

        final ArrayAdapter<Ubigeo> ubigeoAdapter = new ArrayAdapter<Ubigeo>(ctx, R.layout.layout_item_ubigeo_select);
        ubigeoAdapter.addAll(ubigeos);

        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle("Seleccione");
        builder.setAdapter(ubigeoAdapter, null);
        builder.setPositiveButton("OK",  null);
        builder.setNegativeButton("Cancelar", null);

        final AlertDialog dialog = builder.create();
        dialog.getListView().setChoiceMode(ListView.CHOICE_MODE_MULTIPLE);
        dialog.getListView().setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                CheckedTextView textView = (CheckedTextView) view;
                ubigeoPositionSelected[position] = textView.isChecked();
            }
        });
        dialog.show();

        if(etxtPlaces.getTag() != null) {
            List<String> ubigeosSelected = (List<String>) etxtPlaces.getTag();

            for (String itemSelected : ubigeosSelected) {
                for (int i = 0; i < ubigeoAdapter.getCount(); i++) {
                    Ubigeo ubigeo = ubigeoAdapter.getItem(i);

                    if (ubigeo.getCodigo().equalsIgnoreCase(itemSelected)) {
                        ubigeoPositionSelected[i] = true;
                        dialog.getListView().setItemChecked(i, true);
                        break;
                    }
                }
            }
        }

        dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final List<String> ubigeosSelected = new ArrayList<>();
                String placesText = "";

                for (int i = 0; i < ubigeoPositionSelected.length; i++) {
                    if(ubigeoPositionSelected[i]){
                        Ubigeo ubigeo = ubigeoAdapter.getItem(i);
                        ubigeosSelected.add(ubigeo.getCodigo());
                        placesText = placesText + ubigeo.getNombre() + ", ";
                    }
                }

                if(ubigeosSelected.isEmpty() == false){
                    placesText = placesText.substring(0, placesText.length() - 2);
                }

                etxtPlaces.setText(placesText);
                etxtPlaces.setTag(ubigeosSelected);
                dialog.dismiss();
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.imgv_profile_team_photo:
                imgvProfileTeamPhotoClick();
                break;
            case R.id.etxt_places:
              //  etxtPlacesClick();
                CustomDialogUbigeo customDialogUbigeo;
                if(equipo != null){
                    if(equipo.getLugares() == null){
                        customDialogUbigeo   = new CustomDialogUbigeo(CreateTeamActivity.this, this, null);

                    }else{
                        customDialogUbigeo  = new CustomDialogUbigeo(CreateTeamActivity.this, this, equipo.getLugares());
                    }
                }else{
                    customDialogUbigeo   = new CustomDialogUbigeo(CreateTeamActivity.this, this, null);
                }

                customDialogUbigeo.show();
                customDialogUbigeo.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);


                break;
            case R.id.btn_go_members:
                btnGoMembersClick();
                break;
            case R.id.btn_save_team:
                btnSaveTeamClick();
                break;
            case R.id.btn_go_schedule:
                btnGoScheduleClick();
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Gson gson = new Gson();

        if(requestCode == CODE_TEAM_MEMBERS && resultCode == RESULT_OK){

            String dataJSON = data.getStringExtra(ChooseTeamMembersActivity.KEY_TEAM_MEMBERS_SELECTED);

            Type listType = new TypeToken<List<Integer>>(){}.getType();
            idMembers = gson.fromJson(dataJSON, listType);

        } else if(requestCode == CODE_SCHEDULE_TEAM && resultCode == RESULT_OK){

            String dataJSON = data.getStringExtra(ScheduleActivity.KEY_SCHEDULE_TEAM_DATA);

            Type listType = new TypeToken<List<ScheduleAPI>>(){}.getType();
            scheduleAPI = gson.fromJson(dataJSON, listType);
        }
    }

    @Override
    public void onCLickLocation(ArrayList<Ubigeo> list) {

        String placesText = "";

        final List<String> ubigeosSelected = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
                Ubigeo ubigeo = list.get(i);
                ubigeosSelected.add(ubigeo.getCodigo());
                placesText = placesText + ubigeo.getNombre() + ", ";

        }

        if(list.isEmpty()){
            placesText = placesText.substring(0, placesText.length() - 2);
        }

        etxtPlaces.setText(placesText);
        etxtPlaces.setTag(ubigeosSelected);
    }
}
