package com.totalsportapp.ui.register;

import android.content.Intent;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.totalsportapp.R;
import com.totalsportapp.data.db.model.Gender;
import com.totalsportapp.data.api.model.RegisterRequest;
import com.totalsportapp.data.db.model.LoggedMode;
import com.totalsportapp.data.prefs.PreferencesHelper;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.ui.login.LoginActivity;
import com.totalsportapp.ui.main.MainActivity;
import com.totalsportapp.ui.start.StartActivity;
import com.totalsportapp.util.GoogleSignInUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.List;

import static com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes.SIGN_IN_CANCELLED;
import static com.google.android.gms.auth.api.signin.GoogleSignInStatusCodes.SIGN_IN_FAILED;

public class RegisterActivity extends BaseAppCompatActivity implements View.OnClickListener, RegisterView {

    private final static int RC_SIGN_IN_GOOGLE = 100;
    private IRegisterPresenter presenter;

    //region declare controls
    private Button btnRegisterFacebook;
    private Button btnRegisterGoogle;
    private Button btnRegister;
    private TextView txtvHasAccount;
    private CheckBox chkPrivacyPolicy;

    private EditText etxtFirstname;
    private EditText etxtLastname;
    private EditText etxtEmail;
    private EditText etxtPasword;
    private RadioButton rbMan;
    private RadioButton rbWoman;
    //endregion

    private GoogleSignInClient mGoogleSignInClient;

    private CallbackManager callbackManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        onCreateBase();

        findControls();

        configureControls();

        initializeGoogle();
        initializeFacebook();

        presenter = new RegisterPresenter(this);

        //setFakeDataTest();
    }

    private void setFakeDataTest(){
        etxtEmail.setText("bryan.chauca.6@hotmail.com");
        etxtFirstname.setText("Bryan Fernando");
        etxtLastname.setText("Chauca Hinostroza");
        etxtPasword.setText("123456");
        rbMan.setChecked(true);
        chkPrivacyPolicy.setChecked(true);
    }

    private void initializeGoogle(){
        mGoogleSignInClient = GoogleSignInUtil.getGoogleSignInClient(ctx);
    }

    private void initializeFacebook(){
        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        callbackFacebookSuccess(loginResult);
                    }

                    @Override
                    public void onCancel() {
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        if(exception != null){
                            if(exception.getMessage() != null){
                                simpleAlert( "Error Facebook Ex.", exception.getMessage());
                            }
                        }else{
                            simpleAlert("Error de repuesta de Facebook");
                        }
                    }
                });
    }

    private void findControls(){
        btnRegisterFacebook = findViewById(R.id.btn_register_fb);
        btnRegisterGoogle = findViewById(R.id.btn_register_google);
        btnRegister = findViewById(R.id.btn_create_account);

        chkPrivacyPolicy = findViewById(R.id.chk_privacy_policy);
        txtvHasAccount = findViewById(R.id.txtv_has_account);

        etxtFirstname = findViewById(R.id.etxt_firstname);
        etxtLastname = findViewById(R.id.etxt_lastname);
        etxtEmail = findViewById(R.id.etxt_email);
        etxtPasword = findViewById(R.id.etxt_password);
        rbMan = findViewById(R.id.rb_man);
        rbWoman = findViewById(R.id.rb_woman);
    }

    private void configureControls(){
        chkPrivacyPolicy.setMovementMethod(LinkMovementMethod.getInstance());

        txtvHasAccount.setOnClickListener(this);
        btnRegister.setOnClickListener(this);

        btnRegisterGoogle.setOnClickListener(this);
        btnRegisterFacebook.setOnClickListener(this);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        /*Intent i = new Intent(ctx, StartActivity.class);
        startActivity(i);*/
    }

    private boolean onActivityResultGoogleSignIn(int requestCode, Intent data){
        if(requestCode == RC_SIGN_IN_GOOGLE){
            Task<GoogleSignInAccount> completedTask = GoogleSignIn.getSignedInAccountFromIntent(data);

            try {
                GoogleSignInAccount account = completedTask.getResult(ApiException.class);
                RegisterRequest registerRequest = new RegisterRequest();
                registerRequest.setEmail(account.getEmail());
                registerRequest.setPassword(null);
                registerRequest.setGender(null);
                registerRequest.setFirstname(account.getGivenName());
                registerRequest.setLastname(account.getFamilyName());
                registerRequest.setGoogleId(account.getId());

                presenter.Register(registerRequest);

            } catch (ApiException e) {
                if(e.getStatusCode() == SIGN_IN_CANCELLED){
                }else if(e.getStatusCode() == SIGN_IN_FAILED){
                    simpleAlert("Error Google", GoogleSignInStatusCodes.getStatusCodeString(e.getStatusCode()));
                } else{
                    simpleAlert("Error Google", GoogleSignInStatusCodes.getStatusCodeString(e.getStatusCode()));
                }
            }

            return true;
        }
        return  false;
    }

    private void callbackFacebookSuccess(LoginResult loginResult){
        AccessToken accessToken = loginResult.getAccessToken();

        GraphRequest request = GraphRequest.newMeRequest(
                accessToken,
                new GraphRequest.GraphJSONObjectCallback() {
                        @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {

                        if(response.getError() == null){
                            RegisterRequest registerRequest = new RegisterRequest();
                            try {
                                registerRequest.setFacebookId(object.getString("id"));
                                registerRequest.setEmail(object.getString("email"));
                                registerRequest.setPassword(null);
                                registerRequest.setGender(null);
                                registerRequest.setFirstname(object.getString("first_name"));
                                registerRequest.setLastname(object.getString("last_name"));
                                presenter.Register(registerRequest);
                            } catch (JSONException e) {
                                simpleAlert("Error al obtener los datos de su perfil en facebook");
                            }
                        }else{
                            simpleAlert("Error al consultar los datos de su perfil en facebook");
                        }
                    }
                });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name,email,last_name,middle_name,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(callbackManager.onActivityResult(requestCode, resultCode, data)) {
            return;
        }

        if (onActivityResultGoogleSignIn(requestCode, data)) {
            return;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.txtv_has_account:
                hasAccountClick();
                break;
            case R.id.btn_register_google:
                registerGoogleClick();
                break;
            case R.id.btn_register_fb:
                registerFacebookClick();
                break;
            case R.id.btn_create_account:
                registerClick();
                break;
            default:
        }
    }

    private void hasAccountClick(){
        Intent i = new Intent(ctx, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);
    }

    private void registerGoogleClick(){
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN_GOOGLE);
    }

    private void registerFacebookClick(){
        LoginManager.getInstance().logInWithReadPermissions(RegisterActivity.this, Arrays.asList("email", "public_profile", "user_friends"));
    }

    private void registerClick(){
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setFirstname(etxtFirstname.getText().toString());
        registerRequest.setLastname(etxtLastname.getText().toString());
        registerRequest.setEmail(etxtEmail.getText().toString());
        registerRequest.setPassword(etxtPasword.getText().toString());

        if(rbMan.isChecked()){
            registerRequest.setGender(Gender.MAN);
        }else if(rbWoman.isChecked()){
            registerRequest.setGender(Gender.WOMAN);
        }
        registerRequest.setAcceptPrivacyPolicy(chkPrivacyPolicy.isChecked());

        presenter.Register(registerRequest);
    }

    @Override
    public void onResultRegisterSuccess(boolean isLogged) {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        if(isLogged == false){
            Toast.makeText(ctx, "¡Cuenta Creada!", Toast.LENGTH_LONG).show();
            Intent i = new Intent(ctx, LoginActivity.class);
            startActivity(i);
        } else {
            PreferencesHelper.getInstance().setCurrentUserLoggedInMode(LoggedMode.LOGGED_IN);
            Toast.makeText(ctx, "¡Bienvenido!", Toast.LENGTH_LONG).show();

            Intent i = new Intent(ctx, MainActivity.class);
            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(i);

            overridePendingTransition(R.anim.anim_enter, R.anim.anim_leave);
        }
    }

    @Override
    public void showResultRegisterFail(List<String> errors) {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert(errors.get(0));
    }

    @Override
    public void showInternalError() {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("ERROR", R.string.internal_error);
    }

    @Override
    public void showFormValidateError(String messague) {
        simpleAlert(messague);
    }

    @Override
    public void showProcessingRegister() {
        progressDialog.show();
    }
}
