package com.totalsportapp.ui.register;

import android.util.Log;

import com.totalsportapp.data.api.ServicesAPI;
import com.totalsportapp.data.api.UtilAPI;
import com.totalsportapp.data.api.model.OperationResult;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.model.RegisterRequest;
import com.totalsportapp.data.api.model.TokenAPI;
import com.totalsportapp.data.db.model.Gender;
import com.totalsportapp.util.TokenUtils;
import com.totalsportapp.util.ValidationUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterPresenter implements IRegisterPresenter {

    private RegisterView view;

    public RegisterPresenter(RegisterView registerView){
        view = registerView;
    }

    private boolean validateRegister(RegisterRequest registerRequest){
        if(registerRequest.getFirstname().isEmpty()){
            view.showFormValidateError("Nombres son requeridos");
            return false;
        }

        if(registerRequest.getLastname().isEmpty()){
            view.showFormValidateError("Apellidos son requeridos");
            return false;
        }

        if(ValidationUtils.isEmailValid(registerRequest.getEmail()) == false){
            view.showFormValidateError("Email no válido");
            return false;
        }

        if(registerRequest.getEmail().isEmpty()){
            view.showFormValidateError("El email es requerido");
            return false;
        }

        if(registerRequest.getFacebookId() == null
            && registerRequest.getGoogleId() == null
            && registerRequest.getPassword().isEmpty()){
            view.showFormValidateError("La contraseña es requerida");
            return false;
        }

        if(registerRequest.getFacebookId() == null
                && registerRequest.getGoogleId() == null
                && registerRequest.getPassword().length() > 25){
            view.showFormValidateError("Contraseña extensa, máximo 25 dígitos");
            return false;
        }

        if(registerRequest.getFirstname().length() > 70){
            view.showFormValidateError("Nombres muy extenso, máximo 70 dígitos");
            return false;
        }

        if(registerRequest.getLastname().length() > 70){
            view.showFormValidateError("Apellidos muy extenso, máximo 70 dígitos");
            return false;
        }

        if(registerRequest.getFacebookId() == null
                && registerRequest.getGoogleId() == null
                && registerRequest.getGender() == null){
            view.showFormValidateError("Seleccione su sexo");
            return false;
        }

        if(registerRequest.getFacebookId() == null
                && registerRequest.getGoogleId() == null
                && registerRequest.isAcceptPrivacyPolicy() == false){
            view.showFormValidateError("Debe aceptar las políticas de privacidad");
            return false;
        }

        return true;
    }

    @Override
    public void Register(RegisterRequest registerRequest) {
        if(validateRegister(registerRequest) == false){
            return;
        }

        view.showProcessingRegister();

        ServicesAPI servicesAPI = RetrofitClient.getClient().create(ServicesAPI.class);
        Call<OperationResult> call = servicesAPI.signIn(registerRequest);
        call.enqueue(new Callback<OperationResult>() {
            @Override
            public void onResponse(Call<OperationResult> call, Response<OperationResult> response) {
                if (response.isSuccessful()) {
                    OperationResult opResult = response.body();
                    if(opResult.isStatus()){
                        boolean isLogged = false;
                        if(opResult.getData() != null){
                            TokenAPI tokenAPI = UtilAPI.getGsonSync().fromJson(opResult.getData(), TokenAPI.class);
                            TokenUtils.saveToken(tokenAPI);
                            isLogged = true;
                        }
                        view.onResultRegisterSuccess(isLogged);
                    }else{
                        view.showResultRegisterFail(opResult.getErrors());
                    }
                }else{
                    view.showInternalError();
                }
            }

            @Override
            public void onFailure(Call<OperationResult> call, Throwable t) {
                view.showInternalError();
            }
        });
    }
}

