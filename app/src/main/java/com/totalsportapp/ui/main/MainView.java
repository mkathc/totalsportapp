package com.totalsportapp.ui.main;

import com.totalsportapp.data.db.model.UbigeoDao;
import com.totalsportapp.data.db.model.Usuario;

public interface MainView {

    boolean getPrefSyncUser();
    void setPrefSyncUser(boolean syncUser);
    void setPrefUser(Usuario usuario);

    void onErrorSyncProfile();

    void onErrorSyncUbigeo();
}
