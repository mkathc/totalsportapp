package com.totalsportapp.ui.main.tabs;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.totalsportapp.R;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.ServicesAPI;
import com.totalsportapp.data.api.model.LogoutModel;
import com.totalsportapp.data.api.model.OperationResult;
import com.totalsportapp.data.db.model.LoggedMode;
import com.totalsportapp.data.db.model.Usuario;
import com.totalsportapp.data.prefs.PreferencesHelper;
import com.totalsportapp.ui.BaseFragment;
import com.totalsportapp.ui.invitationchallenge.TeamInvitationChallengeActivity;
import com.totalsportapp.ui.myfriends.MyFriendsActivity;
import com.totalsportapp.ui.login.LoginActivity;
import com.totalsportapp.ui.profile.ProfileActivity;
import com.totalsportapp.ui.schedule.ScheduleActivity;
import com.totalsportapp.ui.team.TeamActivity;
import com.totalsportapp.ui.teamrequests.TeamRequestsActivity;
import com.totalsportapp.util.FileAppUtils;
import com.totalsportapp.util.GoogleSignInUtil;

import java.io.File;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

public class ProfileTabFragment extends BaseFragment implements View.OnClickListener {

    //region controls
    private RelativeLayout llContainerEditProfile;
    private RelativeLayout rlMenuInviteFriend;
    private RelativeLayout rlMenuSchedule;
    private RelativeLayout rlMenuFriends;
    private RelativeLayout rlMenuLogout;
    private RelativeLayout rl_menu_request_team;
    private RelativeLayout rl_menu_my_teams;
    private RelativeLayout rl_menu_invitation_challenge;
    private ImageView imgvProfilePhoto;
    private TextView txtvNickname;
    private TextView txtvFullname;
    private TextView txtv_position;
    private TextView txtvJugados;
    private TextView txtvAusencias;
    private TextView txtvOrganizados;
    private RatingBar ratingBar;
    private View line;
    //endregion

    public final int CODE_PROFILE_UPDATE = 1023;

    public ProfileTabFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        onCreateBase();

        View layout = inflater.inflate(R.layout.fragment_perfil_tab_two, container, false);

        findControls(layout);

        configureControls();

        readUsuario();

        return layout;
    }

    private void findControls(View layout){
        llContainerEditProfile = layout.findViewById(R.id.ll_container_edit_profile);
        rlMenuInviteFriend = layout.findViewById(R.id.rl_menu_invite_friend);
        rlMenuSchedule = layout.findViewById(R.id.rl_menu_schedule);
        rlMenuFriends = layout.findViewById(R.id.rl_menu_friends);
        rlMenuLogout = layout.findViewById(R.id.rl_menu_logout);
        imgvProfilePhoto = layout.findViewById(R.id.imgv_profile_photo);
        txtvFullname = layout.findViewById(R.id.txtv_fullname);
        txtvNickname = layout.findViewById(R.id.txtv_nickname);
        txtv_position = layout.findViewById(R.id.txtv_position);
        txtvAusencias = layout.findViewById(R.id.txtv_ausencias);
        txtvJugados = layout.findViewById(R.id.txtv_jugados);
        txtvOrganizados = layout.findViewById(R.id.txtv_organizados);
        ratingBar = layout.findViewById(R.id.rating_user);
        rl_menu_request_team = layout.findViewById(R.id.rl_menu_request_team);
        rl_menu_invitation_challenge = layout.findViewById(R.id.rl_menu_invitation_challenge);
        rl_menu_my_teams = layout.findViewById(R.id.rl_menu_my_teams);
        line = layout.findViewById(R.id.line_request);

    }

    private void configureControls(){
        llContainerEditProfile.setOnClickListener(this);
        rlMenuInviteFriend.setOnClickListener(this);
        rlMenuSchedule.setOnClickListener(this);
        rlMenuFriends.setOnClickListener(this);
        rlMenuLogout.setOnClickListener(this);
        rl_menu_invitation_challenge.setOnClickListener(this);
        rl_menu_my_teams.setOnClickListener(this);
        rl_menu_request_team.setOnClickListener(this);
        imgvProfilePhoto.setOnClickListener(this);
        imgvProfilePhoto.setClipToOutline(true);
    }

    private void readUsuario(){
        //todo falta leer info partidos y confiabilidad
        Usuario usuario = PreferencesHelper.getInstance().getUser();

        txtvFullname.setText(usuario.getNombres() + " " + usuario.getApellidos());

        if(usuario.getAlias() != null ){
            if(usuario.getAlias().equals("")){
                txtvNickname.setText("Sin Alias");
            }else{
                txtvNickname.setText(usuario.getAlias());
            }
        }

        if(usuario.getPosicion() != null){
            if(usuario.getPosicion().equals("")){
                txtv_position.setText("Sin Posición");
            }else{
                txtv_position.setText(usuario.getPosicion());
            }
        }

        if(usuario.getJugados() != 0){
            txtvJugados.setText(String.valueOf(usuario.getJugados()));
        }else{
            txtvJugados.setText("00");
        }

        if(usuario.getAusencias() != 0){
            txtvAusencias.setText(String.valueOf(usuario.getAusencias()));
        }else{
            txtvAusencias.setText("00");
        }

        if(usuario.getOrganizados() != 0){
            txtvOrganizados.setText(String.valueOf(usuario.getOrganizados()));
        }else{
            txtvOrganizados.setText("00");
        }

        if(!usuario.getCalificacion().equals("")){
            ratingBar.setRating(Float.valueOf(usuario.getCalificacion()));
        }

        loadPhotoProfile();
        if(!usuario.isEsCapitan()){
            rl_menu_request_team.setVisibility(View.GONE);
            line.setVisibility(View.GONE);
           // Toast.makeText(ctx, "Es capitan", Toast.LENGTH_SHORT).show();
        }else{
            rl_menu_request_team.setVisibility(View.VISIBLE);
            line.setVisibility(View.VISIBLE);
           // Toast.makeText(ctx, "No es capitan", Toast.LENGTH_SHORT).show();
        }

      /*  if(usuario.getEsCapitan() == 0){
            rl_menu_request_team.setVisibility(View.GONE);
            line.setVisibility(View.GONE);
        }else{
            rl_menu_request_team.setVisibility(View.VISIBLE);
            line.setVisibility(View.VISIBLE);
        }*/
    }

    private void llContainerEditarPerfilClick(){
        Intent i = new Intent(ctx, ProfileActivity.class);
        startActivityForResult(i, CODE_PROFILE_UPDATE);
    }

    private void rlMenuInvitateFriendClick(){
        final String appPackageName = ctx.getPackageName();
        Intent i = new Intent(android.content.Intent.ACTION_SEND);
        i.setType("text/plain");
        i.putExtra(android.content.Intent.EXTRA_SUBJECT, getResources().getString(R.string.title_share_app));
        i.putExtra(android.content.Intent.EXTRA_TEXT, getResources().getString(R.string.share_app) + "\n" + getResources()
                .getString(R.string.app_google_play_store_link) + appPackageName);
        startActivity(Intent.createChooser(i,getResources().getString(R.string.title_share_app)));
    }

    private void rlMenuScheduleClick(){
        Intent i = new Intent(ctx, ScheduleActivity.class);
        ctx.startActivity(i);
    }

    private void rlMenuFriendsClick(){
        Intent i = new Intent(ctx, MyFriendsActivity.class);
        ctx.startActivity(i);
    }

    private void rl_menu_request_team_click(){
        Intent i = new Intent(ctx, TeamRequestsActivity.class);
        ctx.startActivity(i);
    }

    private void rlMenuLogoutClick(){
        AlertDialog dialogLogout = new AlertDialog.Builder(ctx).create();
        dialogLogout.setMessage("¿Cerrar Sesión?");
        dialogLogout.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(ctx);

                        AccessToken accessToken = AccessToken.getCurrentAccessToken();
                        boolean isLoggedFbIn = accessToken != null && !accessToken.isExpired();

                        if(account != null){
                            GoogleSignInUtil.getGoogleSignInClient(ctx).signOut()
                                    .addOnCompleteListener(getActivity(), new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            internalLogout();
                                        }
                                    });
                        } else if(isLoggedFbIn){
                            LoginManager.getInstance().logOut();
                            internalLogout();
                        } else{
                            internalLogout();
                        }
                    }
                });
        dialogLogout.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                });
        dialogLogout.show();
    }

    private void internalLogout(){
        final PreferencesHelper pref = PreferencesHelper.getInstance();
        String currentToken = pref.getTokenFCM();
        String refreshToken = pref.getRefreshToken();

        LogoutModel logoutModel = new LogoutModel();
        logoutModel.setRefreshToken(refreshToken);
        logoutModel.setFCMToken(currentToken);

        pref.setSyncUser(false);
        pref.setCurrentUserLoggedInMode(LoggedMode.LOGGED_OUT);
        pref.setAccessToken(null);
        pref.setRefreshToken(null);
        pref.setExpireDateTime(null);
        pref.setTokenFCM(null);

        ServicesAPI servicesAPI = RetrofitClient.getClient().create(ServicesAPI.class);
        Call<ResponseBody> call = servicesAPI.logout(logoutModel);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
            }
        });

        RetrofitClient.clearAuthClient();

        Intent i = new Intent(ctx, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(i);

        getActivity().overridePendingTransition(R.anim.anim_enter, R.anim.anim_leave);
    }

    private void imgvProfilePhotoClick(){
        launchPhotoPicker();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.ll_container_edit_profile:
                llContainerEditarPerfilClick();
                break;
            case R.id.imgv_profile_photo:
                imgvProfilePhotoClick();
                break;
            case R.id.rl_menu_invite_friend:
                rlMenuInvitateFriendClick();
                break;
            case R.id.rl_menu_schedule:
                rlMenuScheduleClick();
                break;
            case R.id.rl_menu_friends:
                rlMenuFriendsClick();
                break;
            case R.id.rl_menu_logout:
                rlMenuLogoutClick();
                break;
            case R.id.rl_menu_request_team:
                rl_menu_request_team_click();
                break;
            case R.id.rl_menu_my_teams:
                rl_menu_my_teams_click();
                break;
            case R.id.rl_menu_invitation_challenge:
                rl_menu_invitation_challenge_click();
                break;
        }
    }


    private void rl_menu_my_teams_click(){
        Intent i = new Intent(ctx, TeamActivity.class);
        ctx.startActivity(i);
    }


    private void rl_menu_invitation_challenge_click(){
        Intent i = new Intent(ctx, TeamInvitationChallengeActivity.class);
        ctx.startActivity(i);
        //Toast.makeText(ctx, "Implementar invitación a retos", Toast.LENGTH_SHORT).show();
    }


    @Override
    protected void showDialogPhotoSelected(){
        showDialogConfirmPhoto(new ExecuteParam() {
            @Override
            public void onExecute() {
                sendServerPhoto();
            }
        });
    }

    private void sendServerPhoto(){
        progressDialog.show();

        File file = FileAppUtils.prepareFilePhotoToServer(ctx, mCurrentPhotoPath, "foto_temp_app", "photoProfile.jpg");

        MultipartBody.Part filePart = MultipartBody.Part.createFormData("filePhoto", file.getName(), RequestBody.create(MediaType.parse("image/*"), file));

        AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<OperationResult> call = authServicesAPI.savePhotoProfile(filePart);
        call.enqueue(new Callback<OperationResult>(){
            @Override
            public void onResponse(Call<OperationResult> call, Response<OperationResult> response) {
                if (response.isSuccessful()) {
                    progressDialog.dismiss();
                    OperationResult opResult = response.body();
                    if(opResult.isStatus()){
                        onSavePhotoSuccess(opResult.getData());
                    }else{
                        showResultSavePhotoFail(opResult.errors);
                    }
                }else{
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<OperationResult> call, Throwable t) {
                showInternalError();
            }
        });
    }

    public void onSavePhotoSuccess(String filenamePhoto){
        PreferencesHelper pref = PreferencesHelper.getInstance();
        Usuario usuario = pref.getUser();
        usuario.setFoto(filenamePhoto);
        pref.setUser(usuario);
        loadPhotoProfile();
        Toast.makeText(ctx, "¡Foto Actualizada!", Toast.LENGTH_LONG).show();
    }

    public void loadPhotoProfile(){
        Usuario usuario = PreferencesHelper.getInstance().getUser();
        if(usuario.getFoto() != null){
            imgvProfilePhoto.setPadding(0,0,0,0);
            String photoUrl = usuario.getFoto();
            //have to build project if GlideApp is not found
            Glide.with(this)
                    .load(photoUrl)
                    .dontAnimate()
                    .centerCrop()
                    .into(imgvProfilePhoto);
        }
    }

    public void showInternalError() {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("ERROR", R.string.internal_error);
    }

    public void showResultSavePhotoFail(List<String> errors) {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert(errors.get(0));
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == CODE_PROFILE_UPDATE && resultCode == RESULT_OK){
            readUsuario();
            return;
        }
    }
}
