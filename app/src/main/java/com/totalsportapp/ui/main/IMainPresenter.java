package com.totalsportapp.ui.main;

public interface IMainPresenter {
    void syncUserApp();
    void syncUbigeo();
    void syncDepartamento();
    void syncProvincia();
}
