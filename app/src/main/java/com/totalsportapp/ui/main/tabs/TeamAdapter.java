package com.totalsportapp.ui.main.tabs;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.totalsportapp.R;
import com.totalsportapp.data.api.UtilAPI;
import com.totalsportapp.data.db.model.Equipo;
import com.totalsportapp.data.db.model.NivelEquipo;
import com.totalsportapp.ui.challengeteam.ChallengeFormActivity;
import com.totalsportapp.ui.jointeam.JoinTeamFormActivity;
import com.totalsportapp.ui.teamprofile.TeamProfileActivity;

import java.util.ArrayList;

import okhttp3.internal.Util;

import static com.totalsportapp.util.AppConstants.BASE_URL_PHOTO_TEAM;

public class TeamAdapter extends ArrayAdapter<Equipo> {

    private int listType = LIST_TYPE_MY_TEAM;

    public final static int LIST_TYPE_MY_TEAM = 0;
    public final static int LIST_TYPE_CHALLENGE_TEAM = 1;
    public final static int LIST_TYPE_JOIN_TEAM = 2;

    public TeamAdapter(Context context, ArrayList<Equipo> items) {
        super(context, 0, items);
    }

    public void setListType(int listType) {
        this.listType = listType;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_item_team, parent, false);
        }

        final Equipo equipo = getItem(position);

        convertView.setTag(equipo.getIdEquipo());

        ImageView imgv_photo_team = convertView.findViewById(R.id.imgv_photo_team);
        TextView txtv_letter_team = convertView.findViewById(R.id.txtv_letter_team);
        TextView txtv_team_name = convertView.findViewById(R.id.txtv_team_name);
        TextView txtv_team_locations = convertView.findViewById(R.id.txtv_team_locations);
        TextView txtv_team_level = convertView.findViewById(R.id.txtv_team_level);
        TextView txtv_my_team_icon = convertView.findViewById(R.id.txtv_my_team_icon);
        Button btn_challenge = convertView.findViewById(R.id.btn_challenge);
        Button btn_join_team = convertView.findViewById(R.id.btn_join_team);

        imgv_photo_team.setClipToOutline(true);

        if(equipo.getFoto() != null){
            imgv_photo_team.setVisibility(View.VISIBLE);
            txtv_letter_team.setVisibility(View.GONE);
            String photoUrl =  equipo.getFoto();
            //have to build project if GlideApp is not found
            Glide.with(getContext())
                    .load(photoUrl)
                    .dontAnimate()
                    .centerCrop()
                    .into(imgv_photo_team);
        }else{
            imgv_photo_team.setVisibility(View.GONE);
            txtv_letter_team.setVisibility(View.VISIBLE);
            txtv_letter_team.setText(equipo.getNombre().substring(0, 1));
        }

        txtv_team_name.setText(equipo.getNombre());
        txtv_team_level.setText(NivelEquipo.getDescripcion(equipo.getNivel()));
        txtv_team_locations.setText(equipo.getLugaresText());

        if(listType == LIST_TYPE_MY_TEAM){
            if(equipo.getEsLider()){
                txtv_my_team_icon.setVisibility(View.VISIBLE);
            }else{
                txtv_my_team_icon.setVisibility(View.GONE);
            }
        } else if(listType == LIST_TYPE_CHALLENGE_TEAM){
            btn_challenge.setVisibility(View.VISIBLE);
            btn_challenge.setTag(equipo.getIdEquipo());
            btn_challenge.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Gson gson = UtilAPI.getGsonSync();

                    int idEquipo = (int) view.getTag();
                    Intent i = new Intent(getContext(), ChallengeFormActivity.class);
                    i.putExtra(ChallengeFormActivity.KEY_TEAM_ID, idEquipo);
                    i.putExtra(ChallengeFormActivity.KEY_TEAM_NAME, equipo.getNombre());
                    i.putExtra(ChallengeFormActivity.KEY_PLACES_TEAM, gson.toJson(equipo.getLugares()));
                    getContext().startActivity(i);
                }
            });
        } else if(listType == LIST_TYPE_JOIN_TEAM){
            btn_join_team.setVisibility(View.VISIBLE);
            btn_join_team.setTag(equipo.getIdEquipo());
            btn_join_team.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int idEquipo = (int) view.getTag();
                    Intent i = new Intent(getContext(), JoinTeamFormActivity.class);
                    i.putExtra(JoinTeamFormActivity.KEY_TEAM_ID, idEquipo);
                    i.putExtra(JoinTeamFormActivity.KEY_TEAM_NAME, equipo.getNombre());
                    getContext().startActivity(i);
                }
            });
        }

        return convertView;
    }
}
