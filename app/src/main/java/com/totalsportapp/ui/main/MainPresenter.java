package com.totalsportapp.ui.main;

import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;
import com.totalsportapp.MainApplication;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.UtilAPI;
import com.totalsportapp.data.db.model.DaoSession;
import com.totalsportapp.data.db.model.Ubigeo;
import com.totalsportapp.data.db.model.UbigeoDao;
import com.totalsportapp.data.db.model.Usuario;
import com.totalsportapp.data.prefs.PreferencesHelper;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainPresenter implements IMainPresenter {

    private MainView view;
    private DaoSession daoSession;

    public MainPresenter(MainView mainView, DaoSession daoSession){
        view = mainView;
        this.daoSession = daoSession;
    }

    @Override
    public void syncUserApp() {
        if(view.getPrefSyncUser() == false){
            AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
            Call<Usuario> call = authServicesAPI.profile();
            call.enqueue(new Callback<Usuario>(){
                @Override
                public void onResponse(Call<Usuario> call, Response<Usuario> response) {
                    if (response.isSuccessful()) {
                        Usuario usuario = response.body();
                        view.setPrefUser(usuario);
                        view.setPrefSyncUser(true);
                    }else{
                        view.onErrorSyncProfile();
                    }
                }

                @Override
                public void onFailure(Call<Usuario> call, Throwable t) {
                    view.onErrorSyncProfile();
                }
            });
        }
    }

    @Override
    public void syncDepartamento() {
        AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<ArrayList<Ubigeo>> call = authServicesAPI.getDepartamentos();
        call.enqueue(new Callback<ArrayList<Ubigeo>>() {
            @Override
            public void onResponse(Call<ArrayList<Ubigeo>> call, Response<ArrayList<Ubigeo>> response) {
                if (response.isSuccessful()) {
                    PreferencesHelper.getInstance().saveArrayListDepartamentos(response.body());

                }else{
                    view.onErrorSyncUbigeo();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Ubigeo>> call, Throwable t) {
                view.onErrorSyncUbigeo();
            }
        });
    }

    @Override
    public void syncProvincia() {
        AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<ArrayList<Ubigeo>> call = authServicesAPI.getProvincias("150000");
        call.enqueue(new Callback<ArrayList<Ubigeo>>() {
            @Override
            public void onResponse(Call<ArrayList<Ubigeo>> call, Response<ArrayList<Ubigeo>> response) {
                if (response.isSuccessful()) {
                    PreferencesHelper.getInstance().saveArrayListProvincias(response.body());

                }else{
                    view.onErrorSyncUbigeo();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Ubigeo>> call, Throwable t) {
                view.onErrorSyncUbigeo();
            }
        });
    }

    @Override
    public void syncUbigeo() {
        AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<ArrayList<Ubigeo>> call = authServicesAPI.getDistritos("150100");
        call.enqueue(new Callback<ArrayList<Ubigeo>>() {
            @Override
            public void onResponse(Call<ArrayList<Ubigeo>> call, Response<ArrayList<Ubigeo>> response) {
                if (response.isSuccessful()) {
                    ArrayList<Ubigeo> ubigeos = response.body();
                    daoSession.getUbigeoDao().deleteAll();
                    for (Ubigeo item : ubigeos) {
                        daoSession.getUbigeoDao().insert(item);
                    }
                }else{
                    view.onErrorSyncUbigeo();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Ubigeo>> call, Throwable t) {
                view.onErrorSyncUbigeo();
            }
        });
    }
}
