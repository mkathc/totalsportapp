package com.totalsportapp.ui.jointeam;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.totalsportapp.R;
import com.totalsportapp.data.api.UtilAPI;
import com.totalsportapp.data.db.model.Equipo;
import com.totalsportapp.data.db.model.NivelEquipo;
import com.totalsportapp.ui.challengeteam.ChallengeFormActivity;
import com.totalsportapp.util.LoaderAdapter;
import com.totalsportapp.util.OnClickListListener;

import java.util.ArrayList;

/**
 * Created by miguel on 15/03/17.
 */

public class JoinTeamAdapter extends LoaderAdapter<Equipo> implements OnClickListListener {

    private int listType = LIST_TYPE_MY_TEAM;

    public final static int LIST_TYPE_MY_TEAM = 0;
    public final static int LIST_TYPE_CHALLENGE_TEAM = 1;
    public final static int LIST_TYPE_JOIN_TEAM = 2;
    private ArrayList<Equipo> mPostEntities;
    private Context mContext;
    private JoinTeamInterface joinTeamInterface;
    private Activity mActivity;
    //, CommunicatorPostWithPresenter communicatorPostWithPresenter
    public JoinTeamAdapter(Context context, ArrayList<Equipo> postEntities, JoinTeamInterface joinTeamInterface, Activity activity) {

        super(context);
        this.mContext = context;
        this.joinTeamInterface = joinTeamInterface;
        this.mActivity = activity;
        setItems(postEntities);
    }

    public void setListType(int listType) {
        this.listType = listType;
    }

    public ArrayList<Equipo> getItems() {
        return (ArrayList<Equipo>) getmItems();
    }

    public void showLoadingIndicatorAdapter(){
        showLoader = false;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View root = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item_team, parent, false);
        return new ViewHolder(root, this);
    }

    @Override
    public int getItemCount() {
        return getmItems().size();
    }


    @Override
    public long getYourItemId(int position) {
        return 0;
    }

    @Override
    public RecyclerView.ViewHolder getYourItemViewHolder(ViewGroup parent) {
        return new ViewHolder(mInflater.inflate(R.layout.layout_item_team, parent, false), this);
    }

    @Override
    public void bindYourViewHolder(final RecyclerView.ViewHolder holder, final int position) {
        final Equipo equipo = getmItems().get(position);

        ((ViewHolder) holder).imgv_photo_team.setClipToOutline(true);

        if(equipo.getFoto() != null){
            ((ViewHolder) holder).imgv_photo_team.setVisibility(View.VISIBLE);
            ((ViewHolder) holder).txtv_letter_team.setVisibility(View.GONE);
            String photoUrl =  equipo.getFoto();
            //have to build project if GlideApp is not found
            Glide.with(mContext)
                    .load(photoUrl)
                    .dontAnimate()
                    .centerCrop()
                    .into( ((ViewHolder) holder).imgv_photo_team);
        }else{
            ((ViewHolder) holder).imgv_photo_team.setVisibility(View.GONE);
            ((ViewHolder) holder).txtv_letter_team.setVisibility(View.VISIBLE);
            ((ViewHolder) holder).txtv_letter_team.setText(equipo.getNombre().substring(0, 1));
        }

        ((ViewHolder) holder).txtv_team_name.setText(equipo.getNombre());
        ((ViewHolder) holder).txtv_team_level.setText(NivelEquipo.getDescripcion(equipo.getNivel()));
        ((ViewHolder) holder).txtv_team_locations.setText(equipo.getLugaresText());

        if(listType == LIST_TYPE_MY_TEAM){
            if(equipo.getEsLider()){
                ((ViewHolder) holder).txtv_my_team_icon.setVisibility(View.VISIBLE);
            }else{
                ((ViewHolder) holder).txtv_my_team_icon.setVisibility(View.GONE);
            }
        } else if(listType == LIST_TYPE_CHALLENGE_TEAM){
            ((ViewHolder) holder).btn_challenge.setVisibility(View.VISIBLE);
            ((ViewHolder) holder).btn_challenge.setTag(equipo.getIdEquipo());
            ((ViewHolder) holder).btn_challenge.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Gson gson = UtilAPI.getGsonSync();

                    int idEquipo = (int) view.getTag();
                    Intent i = new Intent(mActivity, ChallengeFormActivity.class);
                    i.putExtra(ChallengeFormActivity.KEY_TEAM_ID, idEquipo);
                    i.putExtra(ChallengeFormActivity.KEY_TEAM_NAME, equipo.getNombre());
                    i.putExtra(ChallengeFormActivity.KEY_PLACES_TEAM, gson.toJson(equipo.getLugares()));
                    mActivity.startActivity(i);
                }
            });
        } else if(listType == LIST_TYPE_JOIN_TEAM){
            ((ViewHolder) holder).btn_join_team.setVisibility(View.VISIBLE);
            ((ViewHolder) holder).btn_join_team.setTag(equipo.getIdEquipo());
            ((ViewHolder) holder).btn_join_team.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int idEquipo = (int) view.getTag();
                    Intent i = new Intent(mActivity, JoinTeamFormActivity.class);
                    i.putExtra(JoinTeamFormActivity.KEY_TEAM_ID, idEquipo);
                    i.putExtra(JoinTeamFormActivity.KEY_TEAM_NAME, equipo.getNombre());
                    mActivity.startActivity(i);
                }
            });
        }

    }


    @Override
    public void onClick(int position) {
        Equipo equipo = getItems().get(position);
        joinTeamInterface.onClick(equipo);
    }

  /*  public void updateitem(QuestionResponse questionResponse) {


        if (getmItems() != null) {
            for (int i = 0; i < getmItems().size(); i++) {
                if (questionResponse.getId().equals(getmItems().get(i).getId())) {

                    getmItems().get(i).setAnswers(questionResponse.getAnswers());
                    notifyDataSetChanged();
                    return;
                }
            }
        }

    }
*/
    static class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

      ImageView imgv_photo_team;
      TextView txtv_letter_team;
      TextView txtv_team_name;
      TextView txtv_team_locations;
      TextView txtv_team_level;
      TextView txtv_my_team_icon;
      Button btn_challenge;
      Button btn_join_team;

      private OnClickListListener onClickListListener;

        public ViewHolder(View itemView, OnClickListListener onClickListListener) {
            super(itemView);
            this.onClickListListener = onClickListListener;
            imgv_photo_team = itemView.findViewById(R.id.imgv_photo_team);
            txtv_letter_team = itemView.findViewById(R.id.txtv_letter_team);
            txtv_team_name = itemView.findViewById(R.id.txtv_team_name);
            txtv_team_locations = itemView.findViewById(R.id.txtv_team_locations);
            txtv_team_level = itemView.findViewById(R.id.txtv_team_level);
            txtv_my_team_icon = itemView.findViewById(R.id.txtv_my_team_icon);
            btn_challenge = itemView.findViewById(R.id.btn_challenge);
            btn_join_team = itemView.findViewById(R.id.btn_join_team);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            onClickListListener.onClick(getAdapterPosition());
        }
    }
}
