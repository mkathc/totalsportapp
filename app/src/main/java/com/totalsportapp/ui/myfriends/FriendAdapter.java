package com.totalsportapp.ui.myfriends;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.totalsportapp.R;
import com.totalsportapp.data.api.model.AmigoAPI;
import com.totalsportapp.data.db.model.EstadoAmigo;
import com.totalsportapp.util.GlideApp;

import java.util.ArrayList;
import java.util.List;

import static com.totalsportapp.util.AppConstants.BASE_URL_PHOTO;

public class FriendAdapter extends ArrayAdapter<AmigoAPI> {
    SearchFriendInterface friendInterface;
    public FriendAdapter(Context context, ArrayList<AmigoAPI> items,  SearchFriendInterface friendInterface) {
        super(context, 0, items);
        this.friendInterface = friendInterface;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_item_friend, parent, false);
        }

        final AmigoAPI amigoAPI = getItem(position);
        ImageView imgv_friend_photo = convertView.findViewById(R.id.imgv_friend_photo);
        TextView txtv_friend_name = convertView.findViewById(R.id.txtv_friend_name);
        TextView txtv_friend_nickname = convertView.findViewById(R.id.txtv_friend_nickname);
        TextView txtv_pend_friend = convertView.findViewById(R.id.txtv_pend_friend);
        TextView txtv_deny_friend = convertView.findViewById(R.id.txtv_deny_friend);
        RelativeLayout container_request = convertView.findViewById(R.id.rl_container_request);
        Button btn_accept_request = convertView.findViewById(R.id.btn_accept_request);
        Button btn_deny_request = convertView.findViewById(R.id.btn_deny_request);
        Button btn_delete_friend = convertView.findViewById(R.id.btn_delete_friend);


        if(amigoAPI.getFoto() != null){
            imgv_friend_photo.setPadding(0,0,0,0);
            String photoUrl =  amigoAPI.getFoto();
            //have to build project if GlideApp is not found
            GlideApp.with(getContext())
                    .load(photoUrl)
                    .dontAnimate()
                    .centerCrop()
                    .into(imgv_friend_photo);
        }else{
            GlideApp.with(getContext())
                    .load(getContext().getResources().getDrawable(R.drawable.logo_total_sport_png_512_x_512))
                    .into(imgv_friend_photo);
        }

        txtv_friend_name.setText(amigoAPI.getNombreCompleto());

        if(amigoAPI.getAlias() != null){
            txtv_friend_nickname.setText(amigoAPI.getAlias());
        }else{
            txtv_friend_nickname.setText("Sin alias");
        }

        if(amigoAPI.isAgregadoPorMi()){
            btn_delete_friend.setVisibility(View.GONE);
            container_request.setVisibility(View.GONE);
            if(amigoAPI.getEstado() == EstadoAmigo.PENDIENTE ||
                    (amigoAPI.getEstado() == EstadoAmigo.DENEGADO && amigoAPI.isAgregadoPorMi() == true)){
                txtv_pend_friend.setVisibility(View.VISIBLE);
            } else if( amigoAPI.getEstado() == EstadoAmigo.DENEGADO && amigoAPI.isAgregadoPorMi() == false){
                txtv_deny_friend.setVisibility(View.VISIBLE);
            }else if(amigoAPI.getEstado() == EstadoAmigo.ACEPTADO){
                btn_delete_friend.setVisibility(View.VISIBLE);
                btn_delete_friend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        friendInterface.clickDelete(amigoAPI.getIdUsuarioAmigo());
                    }
                });
            }
        }else {
            if(amigoAPI.getEstado() == EstadoAmigo.ACEPTADO){
                container_request.setVisibility(View.GONE);
                txtv_pend_friend.setVisibility(View.GONE);
                txtv_deny_friend.setVisibility(View.GONE);
                btn_delete_friend.setVisibility(View.VISIBLE);
                btn_delete_friend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        friendInterface.clickDelete(amigoAPI.getIdUsuarioAmigo());
                    }
                });
            }else{
                container_request.setVisibility(View.VISIBLE);
                txtv_pend_friend.setVisibility(View.GONE);
                txtv_deny_friend.setVisibility(View.GONE);
                btn_delete_friend.setVisibility(View.GONE);
                btn_accept_request.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        friendInterface.clickAccept(amigoAPI.getIdSolicitud());
                    }
                });

                btn_deny_request.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        friendInterface.clickDeny(amigoAPI.getIdSolicitud());
                    }
                });
            }
        }

        return convertView;
    }
}
