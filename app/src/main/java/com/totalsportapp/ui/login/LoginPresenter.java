package com.totalsportapp.ui.login;

import com.totalsportapp.data.api.ServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.UtilAPI;
import com.totalsportapp.data.api.model.LoginRequest;
import com.totalsportapp.data.api.model.OperationResult;
import com.totalsportapp.data.api.model.RegisterRequest;
import com.totalsportapp.data.api.model.TokenAPI;
import com.totalsportapp.data.db.model.LoggedMode;
import com.totalsportapp.util.TokenUtils;
import com.totalsportapp.util.ValidationUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginPresenter implements ILoginPresenter {

    private LoginView view;

    public LoginPresenter(LoginView loginView){
        view = loginView;
    }

    private boolean validateLoginRequest(LoginRequest loginRequest){
        if(ValidationUtils.isEmailValid(loginRequest.getUsername()) == false){
            view.showLoginFormValidateError("Email no válido");
            return false;
        }

        if(loginRequest.getUsername().isEmpty()){
            view.showLoginFormValidateError("El email es requerido");
            return false;
        }

        if(loginRequest.getPassword().isEmpty()){
            view.showLoginFormValidateError("La contraseña es requerida");
            return false;
        }

        if(loginRequest.getPassword().length() > 25){
            view.showLoginFormValidateError("Contraseña extensa, máximo 25 dígitos");
            return false;
        }

        return true;
    }

    @Override
    public void LogIn(LoginRequest loginRequest) {
        if(validateLoginRequest(loginRequest) == false){
            return;
        }

        view.showProcessingLogin();

        ServicesAPI services = RetrofitClient.getClient().create(ServicesAPI.class);
        Call<TokenAPI> call = services.logIn(loginRequest);
        call.enqueue(new Callback<TokenAPI>() {
            @Override
            public void onResponse(Call<TokenAPI> call, Response<TokenAPI> response) {
                if (response.isSuccessful()) {
                    TokenAPI tokenAPI = response.body();

                    if(tokenAPI.getAccessToken() == null &&
                            tokenAPI.getRefreshToken() == null){
                        view.showResultLoginFail("Usuario/contraseña incorrectos");
                    } else if(tokenAPI.getAccessToken() != null &&
                            tokenAPI.getRefreshToken() == null){
                        view.showResultLoginFail("Ha ocurrido un problema interno");
                    } else {
                        view.setLoggedMode(LoggedMode.LOGGED_IN);
                        TokenUtils.saveToken(tokenAPI);
                        view.onResultLoginSuccess();
                    }
                }else{
                    view.showInternalError();
                }
            }

            @Override
            public void onFailure(Call<TokenAPI> call, Throwable t) {
                view.showInternalError();
            }
        });
    }

    @Override
    public void Register(RegisterRequest registerRequest) {
        view.showProcessingLogin();

        ServicesAPI servicesAPI = RetrofitClient.getClient().create(ServicesAPI.class);
        Call<OperationResult> call = servicesAPI.signIn(registerRequest);
        call.enqueue(new Callback<OperationResult>() {
            @Override
            public void onResponse(Call<OperationResult> call, Response<OperationResult> response) {
                if (response.isSuccessful()) {
                    OperationResult opResult = response.body();
                    if(opResult.isStatus()){
                        if(opResult.getData() != null){
                            view.setLoggedMode(LoggedMode.LOGGED_IN);
                            TokenAPI tokenAPI = UtilAPI.getGsonSync().fromJson(opResult.getData(), TokenAPI.class);
                            TokenUtils.saveToken(tokenAPI);
                        }
                        view.onResultLoginSuccess();
                    }else{
                        view.showResultLoginFail(opResult.getErrors().get(0));
                    }
                }else{
                    view.showInternalError();
                }
            }

            @Override
            public void onFailure(Call<OperationResult> call, Throwable t) {
                view.showInternalError();
            }
        });
    }
}
