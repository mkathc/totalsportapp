package com.totalsportapp.ui.teamrequests;

import android.app.Activity;
import android.app.FragmentTransaction;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.totalsportapp.R;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.UtilAPI;
import com.totalsportapp.data.api.model.OperationResult;
import com.totalsportapp.data.api.model.RequestSolicitudesModel;
import com.totalsportapp.data.api.model.SolicitudEquipoAPI;
import com.totalsportapp.data.db.model.NivelEquipo;
import com.totalsportapp.data.prefs.PreferencesHelper;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.ui.main.MainActivity;
import com.totalsportapp.ui.main.tabs.CalendarTabFragment;
import com.totalsportapp.ui.teamcalendar.TeamCalendarActivityPrueba;
import com.totalsportapp.ui.teamprofile.TeamProfileActivity;
import com.totalsportapp.ui.userprofile.UserProfileActivity;
import com.totalsportapp.util.GlideApp;
import com.totalsportapp.util.NetworkUtils;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.totalsportapp.util.AppConstants.BASE_URL_PHOTO;
import static com.totalsportapp.util.AppConstants.BASE_URL_PHOTO_TEAM;
import static com.totalsportapp.util.AppConstants.DATETIME_FORMAT_CLIENT;

public class TeamRequestsActivity extends BaseAppCompatActivity {

    //region controls
    private Toolbar toolbar;
    private LinearLayout ll_container_join_request;
    private LinearLayout ll_container_challenge_request;
    //end

    public static final int CODE_REQUEST_JOIN_TEAM = 4154;
    public static final int CODE_REQUEST_CHALLENGE_TEAM = 4156;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_team_requests);

        onCreateBase();

        findControls();

        configureControls();

        if(NetworkUtils.isNetworkConnected(ctx)){
            loadTeamRequests();
        }else{
            showDisconnectedNetwork();
            onBackPressed();
        }

    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    private void findControls(){
        toolbar = findViewById(R.id.toolbar);
        ll_container_join_request = findViewById(R.id.ll_container_join_request);
        ll_container_challenge_request = findViewById(R.id.ll_container_challenge_request);
    }

    private void configureControls(){
        setUpToolbar();
    }

    private void loadTeamRequests(){
        progressDialog.show();

        AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<RequestSolicitudesModel> call = authServicesAPI.teamRequests(1,1);
        call.enqueue(new Callback<RequestSolicitudesModel>() {
            @Override
            public void onResponse(Call<RequestSolicitudesModel> call, Response<RequestSolicitudesModel> response) {
                if(response.isSuccessful()) {
                    if (progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }

                    ll_container_challenge_request.removeAllViews();
                    ll_container_join_request.removeAllViews();

                    List<SolicitudEquipoAPI> requestJoinTeam = new ArrayList<>();
                    List<SolicitudEquipoAPI> requestChallengeTeam = new ArrayList<>();

                    for (int i = 0; i <response.body().getListaUnirData().getListUnir().size() ; i++) {
                        requestJoinTeam.add(response.body().getListaUnirData().getListUnir().get(i));
                    }

                    for (int i = 0; i <response.body().getListaRetosData().getListRetos().size() ; i++) {
                        requestChallengeTeam.add(response.body().getListaRetosData().getListRetos().get(i));
                    }

                  /*  for(SolicitudEquipoAPI item : data){
                        if(item.getTipoSolicitud() == SolicitudEquipoAPI.TIPO_SOLICITUD_UNIR){
                            requestJoinTeam.add(item);
                        }
                    }

                    for(SolicitudEquipoAPI item : data){
                        if(item.getTipoSolicitud() == SolicitudEquipoAPI.TIPO_SOLICITUD_RETAR){
                            requestChallengeTeam.add(item);
                        }
                    }*/

                    if(requestJoinTeam.size() == 0){
                        setEmptyLayout(SolicitudEquipoAPI.TIPO_SOLICITUD_UNIR);
                    } else {
                        setItemLayout(SolicitudEquipoAPI.TIPO_SOLICITUD_UNIR, requestJoinTeam);
                    }

                    if(requestChallengeTeam.size() == 0){
                        setEmptyLayout(SolicitudEquipoAPI.TIPO_SOLICITUD_RETAR);
                    } else {
                        setItemLayout(SolicitudEquipoAPI.TIPO_SOLICITUD_RETAR, requestChallengeTeam);
                    }
                } else {
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<RequestSolicitudesModel> call, Throwable t) {
                showInternalError();
            }
        });
    }

    private void setUpToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("Solicitudes de Equipo");
    }

    private void setEmptyLayout(int typeList){
        View layout_empty = LayoutInflater.from(ctx).inflate(R.layout.layout_empty_request, null);
        TextView txtv_empty_request = layout_empty.findViewById(R.id.txtv_empty_request);
        if(typeList == SolicitudEquipoAPI.TIPO_SOLICITUD_RETAR){
            txtv_empty_request.setText(R.string.empty_challenge_request);
            ll_container_challenge_request.addView(layout_empty);
        }else if(typeList == SolicitudEquipoAPI.TIPO_SOLICITUD_UNIR){
            txtv_empty_request.setText(R.string.empty_join_request);
            ll_container_join_request.addView(layout_empty);
        }
    }

    private void setItemLayout(final int typeList, List<SolicitudEquipoAPI> items){

        for(final SolicitudEquipoAPI item : items){
            View layout = LayoutInflater.from(ctx).inflate(R.layout.layout_item_request, null);
            ImageView imgv_profile_photo = layout.findViewById(R.id.imgv_profile_photo);
            TextView txtv_title = layout.findViewById(R.id.txtv_title);
            TextView txtv_subtitle = layout.findViewById(R.id.txtv_subtitle);
            Button btn_accept_request = layout.findViewById(R.id.btn_accept_request);
            Button btn_deny_request = layout.findViewById(R.id.btn_deny_request);

            imgv_profile_photo.setClipToOutline(true);

            if(item.getFoto() != null){
                imgv_profile_photo.setPadding(0, 0 , 0, 0);

                String photoUrl = "";
                if(typeList == SolicitudEquipoAPI.TIPO_SOLICITUD_RETAR){
                    photoUrl =  item.getFoto();
                } else if(typeList == SolicitudEquipoAPI.TIPO_SOLICITUD_UNIR){
                    photoUrl =  item.getFoto();
                }

                GlideApp.with(ctx)
                        .load(photoUrl)
                        .dontAnimate()
                        .centerCrop()
                        .into(imgv_profile_photo);
            }

            txtv_title.setText(item.getTitulo());

            if(typeList == SolicitudEquipoAPI.TIPO_SOLICITUD_RETAR){
                txtv_subtitle.setLines(4);
                Locale spanish = new Locale("es", "ES");
                String equipoText = "vs.";
                String fechaHora = item.getFechaReto().format(DATETIME_FORMAT_CLIENT, spanish);
                txtv_subtitle.setText(equipoText + "\n" + item.getEquipo() +"\n" + fechaHora);
                if(item.getNombreSede() != null && item.getNombreSede().isEmpty() == false){
                    txtv_subtitle.setText(txtv_subtitle.getText() + "\n" + "Sede: " + item.getNombreSede());
                }
            } else if(typeList == SolicitudEquipoAPI.TIPO_SOLICITUD_UNIR){
                txtv_subtitle.setLines(2);
                String equipoText = "quiere integrarse a";
                txtv_subtitle.setText(equipoText + "\n" + item.getEquipo());
            }

            if(typeList == SolicitudEquipoAPI.TIPO_SOLICITUD_RETAR){
                ll_container_challenge_request.addView(layout);

            }else if(typeList == SolicitudEquipoAPI.TIPO_SOLICITUD_UNIR){
                ll_container_join_request.addView(layout);
            }

            btn_accept_request.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    confirmAlert("¿Aceptar?", new ExecuteParam() {
                        @Override
                        public void onExecute() {
                            requestTeamResponse(item.getIdSolicitud(), typeList, 1);
                        }
                    });
                }
            });
            btn_deny_request.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    confirmAlert("¿Rechazar?", new ExecuteParam() {
                        @Override
                        public void onExecute() {
                            requestTeamResponse(item.getIdSolicitud(), typeList, 2);
                        }
                    });
                }
            });

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(typeList == SolicitudEquipoAPI.TIPO_SOLICITUD_RETAR){
                        Gson gson = UtilAPI.getGsonSync();

                        Intent i = new Intent(ctx, TeamProfileActivity.class);
                        i.putExtra(TeamProfileActivity.KEY_TYPE_TEAM_PROFILE, TeamProfileActivity.TYPE_PROFILE_CHALLENGE_TEAM);
                        i.putExtra(TeamProfileActivity.KEY_TEAM_ID, item.getIdRelacional());
                        i.putExtra(TeamProfileActivity.KEY_TEAM_NAME, item.getEquipo());
                        i.putExtra(TeamProfileActivity.KEY_REQUEST_DATA, gson.toJson(item));
                        startActivityForResult(i, CODE_REQUEST_CHALLENGE_TEAM);
                    }else if(typeList == SolicitudEquipoAPI.TIPO_SOLICITUD_UNIR){
                        Intent i = new Intent(ctx, UserProfileActivity.class);
                        i.putExtra(UserProfileActivity.KEY_TYPE_VIEW, UserProfileActivity.TYPE_PROFILE_JOIN_TEAM);
                        i.putExtra(UserProfileActivity.KEY_ID_USER, item.getIdRelacional());
                        i.putExtra(UserProfileActivity.KEY_TEAM_NAME, item.getEquipo());
                        i.putExtra(UserProfileActivity.KEY_REQUEST_ID, item.getIdSolicitud());
                        i.putExtra(UserProfileActivity.KEY_REQUEST_COMMENTS, item.getComentarios());
                        startActivityForResult(i, CODE_REQUEST_JOIN_TEAM);
                    }
                }
            });
        }
    }

    private void requestTeamResponse(int idSolicitud, final int tipoSolicitud, int aprueba){

        progressDialog.show();

        AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<OperationResult> call = authServicesAPI.responseRequests(idSolicitud, tipoSolicitud, aprueba);
        call.enqueue(new Callback<OperationResult>() {
            @Override
            public void onResponse(Call<OperationResult> call, Response<OperationResult> response) {
                if (response.isSuccessful()){
                    OperationResult op = response.body();
                    if(op.isStatus()){
                        if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }
     
                        loadTeamRequests();

                        if(tipoSolicitud == SolicitudEquipoAPI.TIPO_SOLICITUD_RETAR){
                            String channelId = getString(R.string.default_notification_channel_id);
                            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(getApplicationContext(), channelId)
                                    .setSmallIcon(R.drawable.ic_notification)
                                    .setContentTitle("Total Sport")
                                    .setContentText("Usted tiene un nuevo reto")
                                    .setAutoCancel(true)
                                    .setSound(defaultSoundUri);

                            Intent i = new Intent(getApplicationContext(), MainActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            i.putExtra(MainActivity.KEY_TAB_LOAD, 1);
                            PendingIntent pendingIntent = PendingIntent.getActivity(getApplicationContext(), 0 /* Request code */, i, PendingIntent.FLAG_ONE_SHOT);
                            notificationBuilder.setContentIntent(pendingIntent);
                            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                                NotificationChannel channel = new NotificationChannel(channelId, "Channel human readable title", NotificationManager.IMPORTANCE_DEFAULT);
                                notificationManager.createNotificationChannel(channel);
                            }

                            PreferencesHelper pref = PreferencesHelper.getInstance();
                            int notificacionId = pref.getNotificationId() + 1;
                            pref.setNotificationId(notificacionId);
                            notificationManager.notify(notificacionId, notificationBuilder.build());
                        }

                    }else{
                        showResultSaveResponseFail(op.getErrors());
                    }
                }else{
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<OperationResult> call, Throwable t) {
                showInternalError();
            }
        });
    }

    public void showResultSaveResponseFail(List<String> errors) {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert(errors.get(0));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    public void showInternalError() {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("ERROR", R.string.internal_error);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode == RESULT_OK && (requestCode == CODE_REQUEST_CHALLENGE_TEAM || requestCode == CODE_REQUEST_JOIN_TEAM)){
            loadTeamRequests();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();

    }
}
