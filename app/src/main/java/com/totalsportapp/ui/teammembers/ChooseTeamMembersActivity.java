package com.totalsportapp.ui.teammembers;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.totalsportapp.R;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.model.AmigoAPI;
import com.totalsportapp.data.db.model.EstadoAmigo;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.ui.team.CreateTeamActivity;
import com.totalsportapp.util.NetworkUtils;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChooseTeamMembersActivity extends BaseAppCompatActivity {

    //region controls
    private Toolbar toolbar;
    private ListView lv_my_friends;
    private RelativeLayout rl_container_zero_friends;
    //endregion
    public static final String KEY_TEAM_MEMBERS_SELECTED = "key_team_members_selected";

    private ChooseTeamMembersAdapter chooseTeamMembersAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_members);

        onCreateBase();

        findControls();

        configureControls();

        if(NetworkUtils.isNetworkConnected(ctx)){
            displayFriends();
        }else{
            showDisconnectedNetwork();
            onBackPressed();
        }
    }

    private void findControls(){
        toolbar = findViewById(R.id.toolbar);
        lv_my_friends = findViewById(R.id.lv_my_friends);
        rl_container_zero_friends = findViewById(R.id.rl_container_zero_friends);
    }

    private void configureControls(){
        setUpToolbar();
    }

    private void setUpToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("Elegir Miembros");
    }

    private void displayFriends(){
        progressDialog.show();

        final String jsonUsuariosIDs = getIntent().getStringExtra(KEY_TEAM_MEMBERS_SELECTED);

        AuthServicesAPI services = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<ArrayList<AmigoAPI>> call = services.myFriends();
        call.enqueue(new Callback<ArrayList<AmigoAPI>>() {
            @Override
            public void onResponse(Call<ArrayList<AmigoAPI>> call, Response<ArrayList<AmigoAPI>> response) {
                if (response.isSuccessful()) {
                    if(progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }

                    Gson gson = new Gson();
                    Type listType = new TypeToken<List<Integer>>(){}.getType();
                    List<Integer> idMembers = gson.fromJson(jsonUsuariosIDs, listType);

                    ArrayList<AmigoAPI> amigosAPI = response.body();

                    for(int i = 0; i < amigosAPI.size(); i++){
                        if(amigosAPI.get(i).getEstado() == EstadoAmigo.PENDIENTE
                            ||amigosAPI.get(i).getEstado() == EstadoAmigo.DENEGADO){
                            amigosAPI.remove(i);
                            i--;
                        }
                    }

                    if(amigosAPI.size() == 0){
                        lv_my_friends.setVisibility(View.GONE);
                        rl_container_zero_friends.setVisibility(View.VISIBLE);
                        return;
                    }else{
                        lv_my_friends.setVisibility(View.VISIBLE);
                        rl_container_zero_friends.setVisibility(View.GONE);
                    }

                    ArrayList<ChooseTeamMemberItem> items = new ArrayList<>();
                    for(AmigoAPI item : amigosAPI){

                        boolean isSelected = false;

                        for(Integer idSelected : idMembers){
                            if(item.getIdUsuarioAmigo() == idSelected){
                                isSelected = true;
                                break;
                            }
                        }

                        items.add(new ChooseTeamMemberItem(item, isSelected));
                    }

                    chooseTeamMembersAdapter = new ChooseTeamMembersAdapter(ctx, items);
                    lv_my_friends.setAdapter(chooseTeamMembersAdapter);
                    lv_my_friends.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                            View vw_selecct = view.findViewById(R.id.vw_selecct);
                            if(view.getTag() == "0"){
                                vw_selecct.setBackground(ContextCompat.getDrawable(ctx, R.drawable.bg_choose_members_select));
                                view.setTag("1");
                                chooseTeamMembersAdapter.getItem(position).setSelected(true);
                            } else {
                                vw_selecct.setBackground(ContextCompat.getDrawable(ctx, R.drawable.bg_choose_members_unselect));
                                view.setTag("0");
                                chooseTeamMembersAdapter.getItem(position).setSelected(false);
                            }


                        }
                    });
                }else{
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<AmigoAPI>> call, Throwable t) {
                showInternalError();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_elegir_miembros, menu);

        return true;
    }

    private void saveActionClick(){
        List<Integer> ids = new ArrayList<>();

        for(int i = 0; i < chooseTeamMembersAdapter.getCount(); i++){

            ChooseTeamMemberItem item = chooseTeamMembersAdapter.getItem(i);

            if(item.isSelected()){
                ids.add(item.getAmigoAPI().getIdUsuarioAmigo());
            }
        }

        if(ids.size() == 0){
            simpleAlert(R.string.not_selected_members_for_team);
            return;
        }

        Toast.makeText(ctx, "¡Miembros seleccionados!", Toast.LENGTH_LONG).show();
        Gson gson = new Gson();
        Intent i = new Intent(ctx, CreateTeamActivity.class);
        i.putExtra(KEY_TEAM_MEMBERS_SELECTED, gson.toJson(ids));
        setResult(RESULT_OK, i);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.save_action:
                saveActionClick();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showInternalError(){
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("ERROR", R.string.internal_error);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        /*if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {

        }*/
    }
}
