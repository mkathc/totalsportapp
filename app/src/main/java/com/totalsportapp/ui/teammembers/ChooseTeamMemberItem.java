package com.totalsportapp.ui.teammembers;

import com.totalsportapp.data.api.model.AmigoAPI;

public class ChooseTeamMemberItem {

    private AmigoAPI amigoAPI;
    private boolean selected;

    public ChooseTeamMemberItem() {
    }

    public ChooseTeamMemberItem(AmigoAPI amigoAPI, boolean selected) {
        this.amigoAPI = amigoAPI;
        this.selected = selected;
    }

    public AmigoAPI getAmigoAPI() {
        return amigoAPI;
    }

    public void setAmigoAPI(AmigoAPI amigoAPI) {
        this.amigoAPI = amigoAPI;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
