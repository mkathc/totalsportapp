package com.totalsportapp.ui;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDialog;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.totalsportapp.MainApplication;
import com.totalsportapp.R;
import com.totalsportapp.data.db.model.DaoSession;
import com.totalsportapp.util.FileAppUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static com.totalsportapp.util.PhotoUtils.getOriginal;

public class BaseAppCompatActivity extends AppCompatActivity {

    protected Context ctx;
    protected ProgressDialog progressDialog;

    protected static final int CAMERA_PERMISSION = 1;
    protected static final int GALLERY_PERMISION = 2;
    protected static final int LOCATION_PERMISION = 3;

    protected final static int REQUEST_TAKE_PHOTO = 1045;
    protected final static int PICK_PHOTO_CODE = 1046;
    protected String mCurrentPhotoPath;

    protected void onCreateBase() {
        ctx = this;

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Espere por favor...");
        progressDialog.setCancelable(false);
    }

    protected void simpleAlert(String messague) {
        this.simpleAlert(null, messague, null);
    }

    protected void simpleAlert(int resourceString) {
        String messague = getResources().getString(resourceString);
        this.simpleAlert(null, messague, null);
    }

    protected void simpleAlert(String title, int resourceString) {
        String messague = getResources().getString(resourceString);
        this.simpleAlert(title, messague, null);
    }

    protected void simpleAlert(String title, String messague) {
        this.simpleAlert(title, messague, null);
    }

    protected void simpleAlert(String title, String messague, final ExecuteParam onOkAlert) {
        AlertDialog alertDialog = new AlertDialog.Builder(BaseAppCompatActivity.this).create();

        if (title != null) {
            alertDialog.setTitle(title);
        }

        alertDialog.setMessage(messague);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();

                        if(onOkAlert != null){
                            onOkAlert.onExecute();
                        }
                    }
                });
        alertDialog.show();
    }

    protected void confirmAlert(String messague, final ExecuteParam onOk){
        AlertDialog dialogLogout = new AlertDialog.Builder(ctx).create();
        dialogLogout.setMessage(messague);
        dialogLogout.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if(onOk != null){
                            onOk.onExecute();
                        }
                    }
                });
        dialogLogout.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                });
        dialogLogout.show();
    }

    protected DaoSession getDaoSession(){
        return ((MainApplication)getApplication()).getDaoSession();
    }

    protected void dispatchCamera() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, CAMERA_PERMISSION);
        } else {
            launchCamera();
        }
    }

    protected void requestLocationPermision() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.FOREGROUND_SERVICE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.FOREGROUND_SERVICE
            }, LOCATION_PERMISION);
        } else {
            //launchCamera();
        }
    }

    protected void launchCamera(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {

            File photoFile = null;
            try {
                photoFile = FileAppUtils.createImageFile(ctx);
                mCurrentPhotoPath = photoFile.getAbsolutePath();
            } catch (IOException ex) {
                simpleAlert("Error al acceder al SD Card");
            }

            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(ctx,"com.totalsportapp.fileprovider", photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
                }
                else {
                    List<ResolveInfo> resInfoList = getPackageManager().queryIntentActivities(takePictureIntent, PackageManager.MATCH_DEFAULT_ONLY);

                    for (ResolveInfo resolveInfo : resInfoList) {
                        String packageName = resolveInfo.activityInfo.packageName;
                        grantUriPermission(packageName, photoURI, Intent.FLAG_GRANT_WRITE_URI_PERMISSION | Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    }
                }

                startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO);
            }
        }
    }

    protected void dispatchGallery(){
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, GALLERY_PERMISION);
        }else{
            launchGallery();
        }
    }

    protected void launchGallery(){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(intent, PICK_PHOTO_CODE);
        }
    }

    protected void launchPhotoPicker(){
        final CharSequence[] items = {"Cámara", "Galería"};
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx)
                .setTitle("Seleccione")
                .setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int position) {
                        switch (position){
                            case 0:
                                dispatchCamera();
                                break;
                            case 1:
                                dispatchGallery();
                                break;
                        }
                    }
                });

        builder.create().show();
    }

    protected void showDialogConfirmPhoto(final BaseFragment.ExecuteParam executeParam){
        final AppCompatDialog dialogPhotoSelected = new AppCompatDialog(ctx, android.R.style.Theme_Black_NoTitleBar_Fullscreen);
        dialogPhotoSelected.setContentView(R.layout.layout_photo_selected);
        Button btnOkPhotoSelected = dialogPhotoSelected.findViewById(R.id.btn_ok_photo_selected);
        Button btnCancelPhotoSelected = dialogPhotoSelected.findViewById(R.id.btn_cancel_photo_selected);
        ImageView imgvPhotoSelected = dialogPhotoSelected.findViewById(R.id.imgv_photo_selected);
        btnCancelPhotoSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogPhotoSelected.dismiss();
            }
        });
        btnOkPhotoSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogPhotoSelected.dismiss();
                if(executeParam != null){
                    executeParam.onExecute();
                }
            }
        });

        Bitmap bpPhoto = getOriginal(ctx, mCurrentPhotoPath);
        imgvPhotoSelected.setImageBitmap(bpPhoto);
        dialogPhotoSelected.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_TAKE_PHOTO && resultCode == RESULT_OK) {
            showDialogPhotoSelected();

        } else if(requestCode == PICK_PHOTO_CODE && resultCode == RESULT_OK){
            Uri photoUri = data.getData();
            mCurrentPhotoPath = FileAppUtils.getRealPathFromURI(photoUri, ctx);

            showDialogPhotoSelected();
        }
    }

    protected void showDisconnectedNetwork(){
        Toast.makeText(ctx, R.string.disconnected_network, Toast.LENGTH_LONG).show();
    }

    protected void showDialogPhotoSelected(){
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,  String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        switch (requestCode) {
            case CAMERA_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    launchCamera();
                } else {
                    simpleAlert("Por favor brinda permisos para usar la cámara");
                }
                return;
            case GALLERY_PERMISION:
                if(grantResults.length > 0 && grantResults[0]  == PackageManager.PERMISSION_GRANTED){
                    launchGallery();
                }else{
                    simpleAlert( "Por favor brinda permisos para acceder a la galería");
                }
                return;
        }
    }

    public interface ExecuteParam {
        void onExecute();
    }
}
