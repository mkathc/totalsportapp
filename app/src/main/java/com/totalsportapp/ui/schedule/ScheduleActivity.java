package com.totalsportapp.ui.schedule;

import android.content.Intent;
import android.support.v4.content.res.ResourcesCompat;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.totalsportapp.R;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.model.OperationResult;
import com.totalsportapp.data.api.model.ScheduleAPI;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.ui.team.CreateTeamActivity;
import com.totalsportapp.util.NetworkUtils;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Equator;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ScheduleActivity extends BaseAppCompatActivity {

    //region controls
    private Toolbar toolbar;
    private LinearLayout ll_container_horario;
    //endregion

    public final static int SCHEDULE_TYPE_USER = 1;
    public final static int SCHEDULE_TYPE_TEAM = 2;
    public final static String KEY_SCHEDULE_TYPE = "KEY_SCHEDULE_TYPE";
    public final static String KEY_SCHEDULE_TEAM_DATA = "KEY_SCHEDULE_TEAM_DATA";
    public final static String KEY_SCHEDULE_CAN_SAVE = "KEY_SCHEDULE_CAN_SAVE";

    private int scheduleType = SCHEDULE_TYPE_USER;

    private ArrayList<ScheduleAPI> userScheduleAPI;

    private boolean askedWannaSave;

    private boolean canSave = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule);

        onCreateBase();

        askedWannaSave = false;

        findControls();

        configureControls();

        Bundle extras = getIntent().getExtras();
        if(extras != null){
            if(extras.containsKey(KEY_SCHEDULE_TYPE)){
                scheduleType = extras.getInt(KEY_SCHEDULE_TYPE);
            }

            if(extras.containsKey(KEY_SCHEDULE_CAN_SAVE)){
                canSave = extras.getBoolean(KEY_SCHEDULE_CAN_SAVE);
            }
        }

        if(scheduleType == SCHEDULE_TYPE_USER){
            if(NetworkUtils.isNetworkConnected(ctx)){
                displayScheduleUser();
            }else{
                showDisconnectedNetwork();
                onBackPressed();
            }
        } else if(scheduleType == SCHEDULE_TYPE_TEAM){
            displayScheduleTeam();
        }
    }

    private void findControls(){
        toolbar = findViewById(R.id.toolbar);
        ll_container_horario = findViewById(R.id.ll_container_horario);
    }

    private void configureControls(){
        setUpToolbar();
    }

    private void setUpToolbar(){
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        setTitle("Preferencias Horarias");
    }

    private TagScheduleItem getTagScheduleItem(ArrayList<ScheduleAPI> scheduleAPI, int day, int hour){
        boolean isSelected = false;

        for(ScheduleAPI item : scheduleAPI){
            if(day == item.getDia()
                    && hour == item.getHora()){
                isSelected = true;
                break;
            }
        }

        return new TagScheduleItem(isSelected, day, hour);
    }

    private void setUpSchedule(ArrayList<ScheduleAPI> scheduleAPI){
        userScheduleAPI = scheduleAPI;

        final RelativeLayout rl_container_dom = findViewById(R.id.rl_container_dom);
        final RelativeLayout rl_container_sab = findViewById(R.id.rl_container_sab);
        final RelativeLayout rl_container_vie = findViewById(R.id.rl_container_vie);
        final RelativeLayout rl_container_jue = findViewById(R.id.rl_container_jue);
        final RelativeLayout rl_container_mie = findViewById(R.id.rl_container_mie);
        final RelativeLayout rl_container_mar = findViewById(R.id.rl_container_mar);
        final RelativeLayout rl_container_lun = findViewById(R.id.rl_container_lun);
        final RelativeLayout rl_container_empty = findViewById(R.id.rl_container_empty);

        int hour = 12;
        int hourSystem = 0;
        boolean isPM = false;

        for(int i = 0; i < 24; i++){
            LinearLayout ll_container_row = (LinearLayout) getLayoutInflater().inflate(R.layout.layout_row_schedule, null);
            TextView txtv_hora = ll_container_row.findViewById(R.id.txtv_hora);

            txtv_hora.setText(String.format("%02d", hour) + (isPM ? " PM" : " AM"));
            hourSystem = hour + (isPM ? 12 : 0);

            if(hourSystem == 12){
                hourSystem = 0;
            } else if(hourSystem == 24){
                hourSystem = 12;
            }

            RelativeLayout rl_dom = ll_container_row.findViewById(R.id.rl_dom);
            RelativeLayout rl_lun = ll_container_row.findViewById(R.id.rl_lun);
            RelativeLayout rl_mar = ll_container_row.findViewById(R.id.rl_mar);
            RelativeLayout rl_mie = ll_container_row.findViewById(R.id.rl_mie);
            RelativeLayout rl_jue = ll_container_row.findViewById(R.id.rl_jue);
            RelativeLayout rl_vie = ll_container_row.findViewById(R.id.rl_vie);
            RelativeLayout rl_sab = ll_container_row.findViewById(R.id.rl_sab);

            setOnClickItemSchedule(rl_dom, getTagScheduleItem(scheduleAPI,7, hourSystem));
            setOnClickItemSchedule(rl_lun, getTagScheduleItem(scheduleAPI,1, hourSystem));
            setOnClickItemSchedule(rl_mar, getTagScheduleItem(scheduleAPI,2, hourSystem));
            setOnClickItemSchedule(rl_mie, getTagScheduleItem(scheduleAPI,3, hourSystem));
            setOnClickItemSchedule(rl_jue, getTagScheduleItem(scheduleAPI,4, hourSystem));
            setOnClickItemSchedule(rl_vie, getTagScheduleItem(scheduleAPI,5, hourSystem));
            setOnClickItemSchedule(rl_sab, getTagScheduleItem(scheduleAPI,6, hourSystem));

            if(hour == 12){
                hour = 0;
            }

            hour = hour + 1;

            if(hour == 12 && isPM == false){
                isPM = true;
            }

            ll_container_horario.addView(ll_container_row);
        }

        rl_container_empty.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        int w = rl_container_empty.getWidth();
                                        LinearLayout.LayoutParams defaultLLP = new LinearLayout.LayoutParams(0, w, 1);

                                        rl_container_dom.setLayoutParams(defaultLLP);
                                        rl_container_sab.setLayoutParams(defaultLLP);
                                        rl_container_vie.setLayoutParams(defaultLLP);
                                        rl_container_jue.setLayoutParams(defaultLLP);
                                        rl_container_mie.setLayoutParams(defaultLLP);
                                        rl_container_mar.setLayoutParams(defaultLLP);
                                        rl_container_lun.setLayoutParams(defaultLLP);
                                        rl_container_empty.setLayoutParams(defaultLLP);

                                        LinearLayout.LayoutParams defaultItemLLP = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, w, 0);

                                        for(int i = 0; i < ll_container_horario.getChildCount(); i++){
                                            View item_vw = ll_container_horario.getChildAt(i);
                                            item_vw.setLayoutParams(defaultItemLLP);
                                        }
                                    }
                                }
        );
    }

    private void displayScheduleUser(){
        progressDialog.show();

        userScheduleAPI = new ArrayList<>();

        AuthServicesAPI services = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<ArrayList<ScheduleAPI>> call = services.getMySchedule();
        call.enqueue(new Callback<ArrayList<ScheduleAPI>>() {
            @Override
            public void onResponse(Call<ArrayList<ScheduleAPI>> call, Response<ArrayList<ScheduleAPI>> response) {
                if (response.isSuccessful()) {
                    ArrayList<ScheduleAPI> scheduleAPI = response.body();
                    setUpSchedule(scheduleAPI);
                    if(progressDialog.isShowing()){
                        progressDialog.dismiss();
                    }
                }else{
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<ScheduleAPI>> call, Throwable t) {
                showInternalError();
            }
        });
    }

    private void displayScheduleTeam(){
        String jsonSchedule = getIntent().getExtras().getString(KEY_SCHEDULE_TEAM_DATA);
        Gson gson = new Gson();
        Type listType = new TypeToken<ArrayList<ScheduleAPI>>(){}.getType();
        ArrayList<ScheduleAPI> scheduleAPI = gson.fromJson(jsonSchedule,listType);
        setUpSchedule(scheduleAPI);
    }

    private void setOnClickItemSchedule(RelativeLayout rl, TagScheduleItem tag){
        if(tag.isSelected()){
            rl.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.sel_square_pref_hor_on, ctx.getTheme()));
        }
        rl.setTag(tag);

        if(canSave){
            rl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    TagScheduleItem tagOnClick = (TagScheduleItem) view.getTag();

                    if(tagOnClick.isSelected() == false){
                        tagOnClick.setSelected(true);
                        view.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.sel_square_pref_hor_on, ctx.getTheme()));
                    }else{
                        tagOnClick.setSelected(false);
                        view.setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.sel_square_pref_hor_off, ctx.getTheme()));

                    }
                    view.setTag(tagOnClick);
                }
            });
        }
    }

    private ArrayList<ScheduleAPI> getUserScheduleAPIForm(){
        ArrayList<ScheduleAPI> scheduleAPI = new ArrayList<>();

        for(int i = 0; i < ll_container_horario.getChildCount(); i++){
            LinearLayout itemRow = (LinearLayout) ll_container_horario.getChildAt(i);
            for(int x = 0; x < itemRow.getChildCount(); x++){
                View scheduleItem = itemRow.getChildAt(x);
                if(scheduleItem.getTag() != null){
                    TagScheduleItem tagScheduleItem = (TagScheduleItem) scheduleItem.getTag();
                    if(tagScheduleItem.isSelected()){
                        scheduleAPI.add(new ScheduleAPI(tagScheduleItem.getDay(), tagScheduleItem.getHour()));
                    }
                }
            }
        }

        return scheduleAPI;
    }

    private void saveSchedule(){

        final ArrayList<ScheduleAPI> scheduleAPI = getUserScheduleAPIForm();

        if(scheduleAPI.size() == 0){
            simpleAlert(R.string.not_selected_schedule);
            return;
        }

        if(scheduleType == SCHEDULE_TYPE_USER){
            saveScheduleUser(scheduleAPI);
        }else if(scheduleType == SCHEDULE_TYPE_TEAM){
            saveScheduleTeam(scheduleAPI);
        }
    }

    private void saveScheduleUser(final ArrayList<ScheduleAPI> scheduleAPI){
        confirmAlert("¿Guardar?", new ExecuteParam() {
            @Override
            public void onExecute() {
                AuthServicesAPI services = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
                Call<OperationResult> call = services.saveSchedule(scheduleAPI);
                call.enqueue(new Callback<OperationResult>() {
                    @Override
                    public void onResponse(Call<OperationResult> call, Response<OperationResult> response) {
                        if (response.isSuccessful()) {
                            OperationResult opResult = response.body();
                            if(opResult.isStatus()){
                                userScheduleAPI = scheduleAPI;
                                onResultSaveScheduleSuccess();
                            }else{
                                showResultSaveScheduleFail(opResult.getErrors());
                            }
                        }else{
                            showInternalError();
                        }
                    }

                    @Override
                    public void onFailure(Call<OperationResult> call, Throwable t) {
                        showInternalError();
                    }
                });

            }
        });
    }

    private void saveScheduleTeam(ArrayList<ScheduleAPI> scheduleAPI){
        Toast.makeText(ctx, "¡Preferencias Seleccionadas!", Toast.LENGTH_LONG).show();
        Gson gson = new Gson();
        Intent i = new Intent(ctx, CreateTeamActivity.class);
        i.putExtra(KEY_SCHEDULE_TEAM_DATA, gson.toJson(scheduleAPI));
        setResult(RESULT_OK, i);
        finish();
    }

    private void onResultSaveScheduleSuccess(){
        Toast.makeText(ctx, "¡Preferencias Guardadas!", Toast.LENGTH_LONG).show();
        onBackPressed();
    }

    private void showResultSaveScheduleFail(List<String> errors){
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert(errors.get(0));
    }

    private void showInternalError(){
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("ERROR", R.string.internal_error);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        if(canSave){
            getMenuInflater().inflate(R.menu.menu_simple_save, menu);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.save_action:
                saveSchedule();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {

        ArrayList<ScheduleAPI> newScheduleAPI = getUserScheduleAPIForm();

        boolean listAreEqual = CollectionUtils.isEqualCollection(userScheduleAPI, newScheduleAPI, new Equator<ScheduleAPI>() {
            @Override
            public boolean equate(ScheduleAPI o1, ScheduleAPI o2) {
                if(o1.getHora() == o2.getHora() && o1.getDia() == o2.getDia()){
                    return true;
                }
                return false;
            }

            @Override
            public int hash(ScheduleAPI o) {
                return 0;
            }
        });

        if(listAreEqual){
            super.onBackPressed();
        }else{
            if(askedWannaSave){
                super.onBackPressed();
            }else{
                confirmAlert("¿Salir sin guardar cambios?",
                        new ExecuteParam() {
                            @Override
                            public void onExecute() {
                                askedWannaSave = true;
                                onBackPressed();
                            }
                        });
            }
        }
    }
}
