package com.totalsportapp.ui.encuestas;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;


import com.google.gson.Gson;
import com.totalsportapp.R;
import com.totalsportapp.data.api.AuthServicesAPI;
import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.UtilAPI;
import com.totalsportapp.data.api.model.EquipoSolicitudRetarDataFCM;
import com.totalsportapp.data.api.model.MiembroParticipante;
import com.totalsportapp.data.api.model.OperationResult;
import com.totalsportapp.data.api.model.ParticipantModel;
import com.totalsportapp.data.api.model.QualifyChallengeModel;
import com.totalsportapp.data.api.model.QualifyMemberModel;
import com.totalsportapp.data.prefs.PreferencesHelper;
import com.totalsportapp.ui.BaseAppCompatActivity;
import com.totalsportapp.ui.BaseFragment;
import com.totalsportapp.ui.requestchallenge.MemberRequestAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * Created by junior on 27/08/16.
 */
public class EncuestaFragment extends BaseFragment {

    private static final String TAG = EncuestaFragment.class.getSimpleName();
    private static final int ASISTIO_EQUIPO = 1;
    private static final int NO_ASISTIO_EQUIPO = 0;

    private static final String GANADO = "G";
    private static final String PERDIDO = "P";
    private static final String EMPATADO = "E";

    RelativeLayout buttonSendEncuesta;

    Button btn_no;
    Button btn_yes;
    Button btn_win;
    Button btn_lose;
    Button btn_tie;
    RelativeLayout btn_send_encuesta;

    private EncuestaAdapter mAdapter;
    private LinearLayoutManager mlinearLayoutManager;
    private int idReto;
    private int idEquipo;
    private ArrayList<MiembroParticipante> equipos;
    private RecyclerView rvList;
    private String result = "";
    private int asistencia = -1;

    public EncuestaFragment() {
        // Requires empty public constructor
    }

    public static EncuestaFragment newInstance(Bundle bundle) {
        EncuestaFragment encuestaFragment = new EncuestaFragment();
        encuestaFragment.setArguments(bundle);
        return encuestaFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(getArguments().getString("data") != null ){
            String jsonData = getArguments().getString("data");
            Log.e("DATA ENCUESTA", jsonData);
            Gson gson = UtilAPI.getGsonSync();
            EquipoSolicitudRetarDataFCM equipoSolicitudRetarDataFCM = gson.fromJson(jsonData, EquipoSolicitudRetarDataFCM.class);
            idEquipo = equipoSolicitudRetarDataFCM.getMiEquipo();
            idReto = equipoSolicitudRetarDataFCM.getIdEquipoSolicitudRetar();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.encuesta_fragment, container, false);
        //mProgressDialogCustom = new ProgressDialogCustom(getContext(), "Cargando...");

        getTeamParticipants();

        getButtons(root);
        return root;
    }

    private void getButtons(View view) {
        btn_yes = view.findViewById(R.id.button_yes);
        btn_no = view.findViewById(R.id.button_no);
        btn_win = view.findViewById(R.id.button_win);
        btn_lose = view.findViewById(R.id.button_lose);
        btn_tie = view.findViewById(R.id.button_tie);
        rvList = view.findViewById(R.id.rv_list);
        btn_send_encuesta = view.findViewById(R.id.button_send_encuesta);

        btn_yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                asistencia = ASISTIO_EQUIPO;
                btn_yes.setBackground(getResources().getDrawable(R.drawable.button_selected));
                btn_no.setBackground(getResources().getDrawable(R.drawable.button_border));
                btn_yes.setTextColor(getContext().getResources().getColor(R.color.white));
                btn_no.setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
            }
        });

        btn_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                asistencia = NO_ASISTIO_EQUIPO;
                btn_no.setBackground(getResources().getDrawable(R.drawable.button_selected));
                btn_yes.setBackground(getResources().getDrawable(R.drawable.button_border));
                btn_no.setTextColor(getContext().getResources().getColor(R.color.white));
                btn_yes.setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
            }
        });

        btn_win.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result = GANADO;
                btn_win.setBackground(getResources().getDrawable(R.drawable.button_selected));
                btn_lose.setBackground(getResources().getDrawable(R.drawable.button_border));
                btn_tie.setBackground(getResources().getDrawable(R.drawable.button_border));

                btn_win.setTextColor(getContext().getResources().getColor(R.color.white));
                btn_lose.setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
                btn_tie.setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
            }
        });

        btn_lose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result = PERDIDO;
                btn_lose.setBackground(getResources().getDrawable(R.drawable.button_selected));
                btn_win.setBackground(getResources().getDrawable(R.drawable.button_border));
                btn_tie.setBackground(getResources().getDrawable(R.drawable.button_border));

                btn_win.setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
                btn_lose.setTextColor(getContext().getResources().getColor(R.color.white));
                btn_tie.setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
            }
        });

        btn_tie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                result = EMPATADO;
                btn_tie.setBackground(getResources().getDrawable(R.drawable.button_selected));
                btn_lose.setBackground(getResources().getDrawable(R.drawable.button_border));
                btn_win.setBackground(getResources().getDrawable(R.drawable.button_border));

                btn_win.setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
                btn_lose.setTextColor(getContext().getResources().getColor(R.color.colorPrimary));
                btn_tie.setTextColor(getContext().getResources().getColor(R.color.white));
            }
        });

        btn_send_encuesta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onViewClicked();
            }
        });
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mAdapter = new EncuestaAdapter(new ArrayList<MiembroParticipante>(), getContext());
        mlinearLayoutManager = new LinearLayoutManager(getContext());
        mlinearLayoutManager.setOrientation(LinearLayout.VERTICAL);
        rvList.setAdapter(mAdapter);
        rvList.setLayoutManager(mlinearLayoutManager);
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    public void getTeamParticipants() {
/*        progressDialog = new ProgressDialog(ctx);
        progressDialog.setMessage("Espere por favor...");
        progressDialog.setCancelable(false);
        progressDialog.show();
*/

        AuthServicesAPI services = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<ArrayList<MiembroParticipante>> call = services.teamMembersByReto(idEquipo, idReto);
        call.enqueue(new Callback<ArrayList<MiembroParticipante>>() {
            @Override
            public void onResponse(Call<ArrayList<MiembroParticipante>> call, Response<ArrayList<MiembroParticipante>> response) {
                if (response.isSuccessful()) {
                    equipos = response.body();
                    ArrayList<MiembroParticipante> newList = new ArrayList<>();

                    for (int i = 0; i < equipos.size(); i++) {
                        if (equipos.get(i).getEstado() == 1) {
                            newList.add(equipos.get(i));
                        }else{
                            if (equipos.get(i).getEsCapitan() == 1) {
                                newList.add(equipos.get(i));
                            }
                        }
                    }
                    getAdapter(newList);

                } else {
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<MiembroParticipante>> call, Throwable t) {
                showInternalError();
            }
        });
    }

    private void getAdapter(ArrayList<MiembroParticipante> equipos) {
        if (equipos.size() != 0) {
            mAdapter.setItems(equipos);
        }
       /* if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }*/
    }

    private void showInternalError() {
       /* if (progressDialog.isShowing()) {
            progressDialog.dismiss();
        }*/

        Toast.makeText(ctx, R.string.internal_error, Toast.LENGTH_LONG).show();
    }

    public void onViewClicked() {
        ArrayList<ParticipantModel> list = new ArrayList<>();

        if (PreferencesHelper.getInstance().getArrayListEncuesta(PreferencesHelper.ARRAY_ENCUESTA) == null) {

            confirmAlert("¿Seguro que desea enviar sin calificar a su equipo?", new ExecuteParam() {
                @Override
                public void onExecute() {
                    ArrayList<ParticipantModel> emptylist = new ArrayList<>();
                    QualifyMemberModel qualifyMemberModel = new QualifyMemberModel();
                    qualifyMemberModel.setIdEquipo(idEquipo);
                    qualifyMemberModel.setResultado(result);
                    qualifyMemberModel.setParticipants(emptylist);
                    sendQualifyMember(qualifyMemberModel);
                }
            });

        } else {
            list = PreferencesHelper.getInstance().getArrayListEncuesta(PreferencesHelper.ARRAY_ENCUESTA);

            QualifyMemberModel qualifyMemberModel = new QualifyMemberModel();
            qualifyMemberModel.setIdEquipo(idEquipo);
            qualifyMemberModel.setResultado(result);
            qualifyMemberModel.setParticipants(list);
            sendQualifyMember(qualifyMemberModel);
        }

        if(asistencia!= -1 || !result.equals("")){
            QualifyChallengeModel qualifyChallengeModel = new QualifyChallengeModel();
            qualifyChallengeModel.setIdEquipo(idEquipo);
            qualifyChallengeModel.setResultado(result);
            qualifyChallengeModel.setAsistencia(asistencia);
            sendQualifyTeam(qualifyChallengeModel);
        }else{
            confirmAlert("¿Seguro que desea salir sin calificar su juego?", new ExecuteParam() {
                @Override
                public void onExecute() {
                    QualifyChallengeModel qualifyChallengeModel = new QualifyChallengeModel();
                    qualifyChallengeModel.setIdEquipo(idEquipo);
                    qualifyChallengeModel.setResultado(result);
                    qualifyChallengeModel.setAsistencia(asistencia);
                    sendQualifyTeam(qualifyChallengeModel);
                }
            });
        }
    }

    private void sendQualifyMember(QualifyMemberModel qualifyMemberModel){
        AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<OperationResult> call = authServicesAPI.challengeQualifyMembers(qualifyMemberModel);
        call.enqueue(new Callback<OperationResult>() {
            @Override
            public void onResponse(Call<OperationResult> call, Response<OperationResult> response) {
                if (response.isSuccessful()){
                    OperationResult op = response.body();
                    if(op.isStatus()){
                        getActivity().finish();
                        Toast.makeText(getContext(), op.getData(), Toast.LENGTH_SHORT).show();
                    }else{
                        showResultAddFriendFail(op.getErrors());
                    }
                }else{
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<OperationResult> call, Throwable t) {
                showInternalError();
            }
        });
    }
    public void showResultAddFriendFail(List<String> errors) {
        if(progressDialog.isShowing()){
            progressDialog.dismiss();
        }

        simpleAlert("Error", errors.get(0));
    }



    private void sendQualifyTeam(QualifyChallengeModel qualifyMemberModel){
        AuthServicesAPI authServicesAPI = RetrofitClient.getClientAuth().create(AuthServicesAPI.class);
        Call<OperationResult> call = authServicesAPI.challengeQualifyTeam(qualifyMemberModel);
        call.enqueue(new Callback<OperationResult>() {
            @Override
            public void onResponse(Call<OperationResult> call, Response<OperationResult> response) {
                if (response.isSuccessful()){
                    OperationResult op = response.body();
                    if(op.isStatus()){
                        /*if(progressDialog.isShowing()){
                            progressDialog.dismiss();
                        }*/
                    }else{
                        showResultAddFriendFail(op.getErrors());
                    }
                }else{
                    showInternalError();
                }
            }

            @Override
            public void onFailure(Call<OperationResult> call, Throwable t) {
                showInternalError();
            }
        });
    }
    protected void confirmAlert(String messague, final ExecuteParam onOk){
        AlertDialog dialogLogout = new AlertDialog.Builder(getContext()).create();
        dialogLogout.setMessage(messague);
        dialogLogout.setButton(AlertDialog.BUTTON_POSITIVE, "Aceptar",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        if(onOk != null){
                            onOk.onExecute();
                        }
                    }
                });
        dialogLogout.setButton(AlertDialog.BUTTON_NEGATIVE, "Cancelar",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int i) {
                        dialog.dismiss();
                    }
                });
        dialogLogout.show();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        PreferencesHelper.getInstance().saveArrayListEncuesta(null);
    }
}
