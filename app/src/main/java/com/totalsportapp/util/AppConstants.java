package com.totalsportapp.util;

public class AppConstants {

    public static final String DATE_FORMAT_CLIENT = "DD/MM/YYYY";
    public static final String DATETIME_FORMAT_CLIENT = "DD/MM/YYYY hh12:mm a";

    public static final String DATETIME_FORMAT_API = "YYYY-MM-DD hh:mm:ss";
    public static final String DATE_FORMAT_API = "YYYY-MM-DD";

    public static final String BASE_URL = "http://tideco-001-site5.dtempurl.com/";
    //http://gersoncarrascol-001-site1.dtempurl.com/
    //public static final String BASE_URL = "http://tideco-001-site5.dtempurl.com/";
    public static final String BASE_URL_PHOTO = BASE_URL + "App_Foto/";
    public static final String BASE_URL_PHOTO_TEAM = BASE_URL + "App_FotoEquipo/";
    public static final String BASE_URL_API = BASE_URL + "api/";
    //public static final String DATE_FORMAT_TIME = "DD/MM/YYYY";
}
