package com.totalsportapp.util;

import android.content.Context;
import android.content.ContextWrapper;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FileAppUtils {

    public static String getRealPathFromURI(Uri uri, Context ctx) {
        if (uri == null) {
            return null;
        }
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = ctx.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor
                    .getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        }
        return uri.getPath();
    }

    public static File createImageFile(Context ctx) throws IOException {
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = ctx.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        return image;
    }

    public static File prepareFilePhotoToServer(Context ctx, String mCurrentPhotoPath, String directoryPhoto, String filename){
        Bitmap photoProfile = PhotoUtils.getOriginal(ctx, mCurrentPhotoPath);
        ContextWrapper cw = new ContextWrapper(ctx);
        File directory = cw.getDir(directoryPhoto, Context.MODE_PRIVATE);
        File file = new File(directory, filename);

        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);

            photoProfile.compress(Bitmap.CompressFormat.JPEG, 90, fos);

        } catch (Exception e) {
        } finally {
            try {
                fos.close();
            } catch (IOException e) {
            }
        }

        return file;
    }
}
