package com.totalsportapp.data.api.model;

import java.io.Serializable;

public class InviteMemberResponse implements Serializable {
    private int IdMiembroInvitacion;
    private int Response;

    public int getIdMiembroInvitacion() {
        return IdMiembroInvitacion;
    }

    public void setIdMiembroInvitacion(int idMiembroInvitacion) {
        IdMiembroInvitacion = idMiembroInvitacion;
    }

    public int getResponse() {
        return Response;
    }

    public void setResponse(int response) {
        Response = response;
    }
}
