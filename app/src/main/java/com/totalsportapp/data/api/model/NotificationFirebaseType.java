package com.totalsportapp.data.api.model;

public class NotificationFirebaseType {

    public final static int FRIEND_ADD = 1;
    public final static int JOIN_TEAM = 2;
    public final static int CHALLENGE_TEAM = 3;
    public final static int START_SERVICE_LOCATION = 4;
    public final static int CHALLENGE_INVITE_MEMBER = 5;
    public final static int CHALLENGE_INVITE_RESPONSE = 6;
    public final static int END_SERVICE_LOCATION = 7;
    public final static int CHALLENGE_RESPONSE = 8;
    public final static int INTEGRATION_RESPONSE = 9;
    public final static int CHALLENGE_QUALIFY = 10;

}
