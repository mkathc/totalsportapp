package com.totalsportapp.data.api;

import android.util.Log;

import com.totalsportapp.data.api.RetrofitClient;
import com.totalsportapp.data.api.ServicesAPI;
import com.totalsportapp.data.api.UtilAPI;
import com.totalsportapp.data.api.model.TokenAPI;
import com.totalsportapp.data.prefs.PreferencesHelper;
import com.totalsportapp.util.AppConstants;
import com.totalsportapp.util.TokenUtils;

import java.io.IOException;

import okhttp3.Authenticator;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.Route;

public class TokenAuthenticator implements Authenticator {

    @Override
    public Request authenticate(Route route, Response response) throws IOException {
        if (response.code() == 401) {

            OkHttpClient client = new OkHttpClient();
            String refreshToken = PreferencesHelper.getInstance().getRefreshToken();
            HttpUrl.Builder urlBuilder = HttpUrl.parse(AppConstants.BASE_URL_API + "account/refreshTokenSecurity").newBuilder();
            urlBuilder.addQueryParameter("refreshToken", refreshToken);
            String url = urlBuilder.build().toString();

            Request request = new Request.Builder()
                    .url(url)
                    .build();

            Response refreshResponse = client.newCall(request).execute();
            if (refreshResponse != null && refreshResponse.code() == 200) {

                RetrofitClient.clearAuthClient();
                String responseString = refreshResponse.body().string();

                TokenAPI tokenAPI = UtilAPI.getGsonSync().fromJson(responseString, TokenAPI.class);

                TokenUtils.saveToken(tokenAPI);

                return response.request().newBuilder()
                        .header("Authorization", "Bearer " + tokenAPI.getAccessToken())
                        .build();
            } else {
                return null;
            }
        } else {
            return null;
        }
    }
}
