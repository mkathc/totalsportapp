package com.totalsportapp.data.api.model;

import java.io.Serializable;

public class EquipoMiembroAPI implements Serializable {

    private int idEquipoMiembro;
    private boolean EsCapitan;
    private String nombres;
    private String apellidos;
    private String alias;
    private String foto;
    private String email;
    private String posicion;

    public boolean isEsCapitan() {
        return EsCapitan;
    }

    public void setEsCapitan(boolean esCapitan) {
        EsCapitan = esCapitan;
    }

    public int getIdEquipoMiembro() {
        return idEquipoMiembro;
    }

    public void setIdEquipoMiembro(int idEquipoMiembro) {
        this.idEquipoMiembro = idEquipoMiembro;
    }

    public String getPosicion() {
        return posicion;
    }

    public void setPosicion(String posicion) {
        this.posicion = posicion;
    }

    public String getNombreCompleto(){
        return this.nombres + " " + this.getApellidos();
    }

    public String getNombres() {
        return nombres;
    }

    public void setNombres(String nombres) {
        this.nombres = nombres;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
