package com.totalsportapp.data.api.model;

import hirondelle.date4j.DateTime;

public class CalendarioEquipoAPI {

    public static final int CALENDARIO_TIPO_UNIDO = 1;
    public static final int CALENDARIO_TIPO_RETO = 2;

    private int idEquipoSolicitudRetar;
    private String ubigeo;
    private DateTime fechaReto;
    private int tipoListado;
    private int idEquipo;
    private String nombreEquipo;
    private String nombreRetador;

    public String getNombreEquipo() {
        return nombreEquipo;
    }

    public void setNombreEquipo(String nombreEquipo) {
        this.nombreEquipo = nombreEquipo;
    }

    public String getNombreRetador() {
        return nombreRetador;
    }

    public void setNombreRetador(String nombreRetador) {
        this.nombreRetador = nombreRetador;
    }

    public int getIdEquipoSolicitudRetar() {
        return idEquipoSolicitudRetar;
    }

    public void setIdEquipoSolicitudRetar(int idEquipoSolicitudRetar) {
        this.idEquipoSolicitudRetar = idEquipoSolicitudRetar;
    }

    public String getUbigeo() {
        return ubigeo;
    }

    public void setUbigeo(String ubigeo) {
        this.ubigeo = ubigeo;
    }

    public DateTime getFechaReto() {
        return fechaReto;
    }

    public void setFechaReto(DateTime fechaReto) {
        this.fechaReto = fechaReto;
    }

    public int getTipoListado() {
        return tipoListado;
    }

    public void setTipoListado(int tipoListado) {
        this.tipoListado = tipoListado;
    }

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }
}
