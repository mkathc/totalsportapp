package com.totalsportapp.data.api.model;

import com.totalsportapp.data.db.model.Equipo;

import java.io.Serializable;
import java.util.ArrayList;

public class CalendarioEquipoCapitan implements Serializable {
    private int TotalPagina;
    private ArrayList<CalendarioEquipoAPI> ListCalendarioCapitan;

    public int getTotalPagina() {
        return TotalPagina;
    }

    public void setTotalPagina(int totalPagina) {
        TotalPagina = totalPagina;
    }

    public ArrayList<CalendarioEquipoAPI> getListCalendarioCapitan() {
        return ListCalendarioCapitan;
    }

    public void setListCalendarioCapitan(ArrayList<CalendarioEquipoAPI> listCalendarioCapitan) {
        ListCalendarioCapitan = listCalendarioCapitan;
    }
}
