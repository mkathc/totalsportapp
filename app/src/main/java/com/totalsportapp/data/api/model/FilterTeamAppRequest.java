package com.totalsportapp.data.api.model;

import java.util.ArrayList;
import java.util.List;

public class FilterTeamAppRequest {

    public final static int FILTER_TYPE_JOIN = 1;
    public final static int FILTER_TYPE_CHALLENGE = 2;

    private int pagina;
    private int tipoFiltro;
    private List<Integer> niveles;
    private List<String> ubigeos;

    public FilterTeamAppRequest() {
        niveles = new ArrayList<>();
        ubigeos = new ArrayList<>();
    }

    public int getPagina() {
        return pagina;
    }

    public void setPagina(int pagina) {
        this.pagina = pagina;
    }

    public List<Integer> getNiveles() {
        return niveles;
    }

    public void setNiveles(List<Integer> niveles) {
        this.niveles = niveles;
    }

    public List<String> getUbigeos() {
        return ubigeos;
    }

    public void setUbigeos(List<String> ubigeos) {
        this.ubigeos = ubigeos;
    }

    public int getTipoFiltro() {
        return tipoFiltro;
    }

    public void setTipoFiltro(int tipoFiltro) {
        this.tipoFiltro = tipoFiltro;
    }
}
