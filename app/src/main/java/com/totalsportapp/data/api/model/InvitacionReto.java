package com.totalsportapp.data.api.model;

import hirondelle.date4j.DateTime;

public class InvitacionReto {
    private int IdMiembroInvitacion;
    private int IdEquipoSolicitudRetar;
    private int IdEquipoRetador;
    private String NombreEquipoRetador;
    private int IdEquipo;
    private String NombreEquipo;
    private DateTime FechaReto;

    public String getNombreEquipoRetador() {
        return NombreEquipoRetador;
    }

    public void setNombreEquipoRetador(String nombreEquipoRetador) {
        NombreEquipoRetador = nombreEquipoRetador;
    }

    public int getIdMiembroInvitacion() {
        return IdMiembroInvitacion;
    }

    public void setIdMiembroInvitacion(int idMiembroInvitacion) {
        IdMiembroInvitacion = idMiembroInvitacion;
    }

    public int getIdEquipoSolicitudRetar() {
        return IdEquipoSolicitudRetar;
    }

    public void setIdEquipoSolicitudRetar(int idEquipoSolicitudRetar) {
        IdEquipoSolicitudRetar = idEquipoSolicitudRetar;
    }

    public int getIdEquipoRetador() {
        return IdEquipoRetador;
    }

    public void setIdEquipoRetador(int idEquipoRetador) {
        IdEquipoRetador = idEquipoRetador;
    }

    public int getIdEquipo() {
        return IdEquipo;
    }

    public void setIdEquipo(int idEquipoRetado) {
        IdEquipo = idEquipoRetado;
    }

    public String getNombreEquipo() {
        return NombreEquipo;
    }

    public void setNombreEquipo(String nombreEquipo) {
        NombreEquipo = nombreEquipo;
    }

    public DateTime getFechaReto() {
        return FechaReto;
    }

    public void setFechaReto(DateTime fechaReto) {
        FechaReto = fechaReto;
    }
}
