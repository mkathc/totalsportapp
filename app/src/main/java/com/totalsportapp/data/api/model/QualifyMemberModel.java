package com.totalsportapp.data.api.model;

import java.util.ArrayList;

public class QualifyMemberModel {
    private int idEquipo;
    private String resultado;
    private ArrayList<ParticipantModel> participants;

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public ArrayList<ParticipantModel> getParticipants() {
        return participants;
    }

    public void setParticipants(ArrayList<ParticipantModel> participants) {
        this.participants = participants;
    }
}
