package com.totalsportapp.data.api.model;

import hirondelle.date4j.DateTime;

public class SolicitudEquipoAPI {

    public static final int TIPO_SOLICITUD_RETAR = 2;
    public static final int TIPO_SOLICITUD_UNIR = 1;

    private int idSolicitud;
    private String titulo;
    private String subtitulo;
    private String foto;
    private int tipoSolicitud;
    private int idRelacional;
    private int idEquipo;
    private String equipo;

    private DateTime fechaReto;
    private String fechaRetoString;

    private String ubigeo;
    private String nombreSede;
    private int nivel;
    private String comentarios;

    public String getFechaRetoString() {
        return fechaRetoString;
    }

    public void setFechaRetoString(String fechaRetoString) {
        this.fechaRetoString = fechaRetoString;
    }

    public int getIdSolicitud() {
        return idSolicitud;
    }

    public void setIdSolicitud(int idSolicitud) {
        this.idSolicitud = idSolicitud;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getSubtitulo() {
        return subtitulo;
    }

    public void setSubtitulo(String subtitulo) {
        this.subtitulo = subtitulo;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public int getTipoSolicitud() {
        return tipoSolicitud;
    }

    public void setTipoSolicitud(int tipoSolicitud) {
        this.tipoSolicitud = tipoSolicitud;
    }

    public int getIdRelacional() {
        return idRelacional;
    }

    public void setIdRelacional(int idRelacional) {
        this.idRelacional = idRelacional;
    }

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }

    public String getEquipo() {
        return equipo;
    }

    public void setEquipo(String equipo) {
        this.equipo = equipo;
    }

    public DateTime getFechaReto() {
        return fechaReto;
    }

    public void setFechaReto(DateTime fechaReto) {
        this.fechaReto = fechaReto;
    }

    public String getUbigeo() {
        return ubigeo;
    }

    public void setUbigeo(String ubigeo) {
        this.ubigeo = ubigeo;
    }

    public String getNombreSede() {
        return nombreSede;
    }

    public void setNombreSede(String nombreSede) {
        this.nombreSede = nombreSede;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }
}
