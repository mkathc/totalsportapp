package com.totalsportapp.data.prefs;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.totalsportapp.MainApplication;
import com.totalsportapp.data.api.UtilAPI;
import com.totalsportapp.data.api.model.ParticipantModel;
import com.totalsportapp.data.db.model.LoggedMode;
import com.totalsportapp.data.db.model.Ubigeo;
import com.totalsportapp.data.db.model.Usuario;

import java.lang.reflect.Type;
import java.util.ArrayList;

import hirondelle.date4j.DateTime;

public class PreferencesHelper {

    private static final String PREF_KEY_USER_LOGGED_IN_MODE = "PREF_KEY_USER_LOGGED_IN_MODE";
    private static final String PREF_KEY_ACCESS_TOKEN = "PREF_KEY_ACCESS_TOKEN";
    private static final String PREF_KEY_REFRESH_TOKEN = "PREF_KEY_REFRESH_TOKEN";
    private static final String PREF_KEY_EXPIRE_TOKEN = "PREF_KEY_EXPIRE_TOKEN";
    private static final String PREF_KEY_SYNC_USER = "PREF_KEY_SYNC_USER";
    private static final String PREF_KEY_USER = "PREF_KEY_USER";
    private static final String PREF_KEY_TOKEN_FCM = "PREF_KEY_TOKEN_FCM";
    private static final String PREF_KEY_NOTIFICATION_ID = "PREF_KEY_NOTIFICATION_ID";
    private static final String PREF_KEY_EQUIPO = "PREF_KEY_EQUIPO";
    private static final String PREF_KEY_RETO = "PREF_KEY_RETO";
    private static final String PREF_KEY_SET_UBICATION = "PREF_KEY_SET_UBICATION";

    public static final String ARRAY_ENCUESTA = "array_encuesta";
    public static final String ARRAY_EMAILS = "array_emails";

    public static final String ARRAY_DEPARTAMENTOS = "array_departamentos";
    public static final String ARRAY_PROVINCIAS= "array_provincias";

    private final SharedPreferences mPrefs;

    public PreferencesHelper(Context context) {
        mPrefs = context.getSharedPreferences("prefs_app", Context.MODE_PRIVATE);
    }

    public int getCurrentUserLoggedInMode() {
        return mPrefs.getInt(PREF_KEY_USER_LOGGED_IN_MODE, LoggedMode.LOGGED_OUT);
    }

    public void setCurrentUserLoggedInMode(int mode) {
        mPrefs.edit().putInt(PREF_KEY_USER_LOGGED_IN_MODE, mode).apply();
    }

    public String getAccessToken() {
        return mPrefs.getString(PREF_KEY_ACCESS_TOKEN, null);
    }

    public void setAccessToken(String accessToken) {
        mPrefs.edit().putString(PREF_KEY_ACCESS_TOKEN, accessToken).apply();
    }

    public void setRefreshToken(String refreshToken) {
        mPrefs.edit().putString(PREF_KEY_REFRESH_TOKEN, refreshToken).apply();
    }

    public String getRefreshToken() {
        return mPrefs.getString(PREF_KEY_REFRESH_TOKEN, null);
    }

    public void setTokenFCM(String tokenFCM) {
        mPrefs.edit().putString(PREF_KEY_TOKEN_FCM, tokenFCM).apply();
    }

    public int getNotificationId() {
        return mPrefs.getInt(PREF_KEY_NOTIFICATION_ID, 0);
    }

    public void setNotificationId(int notificationId) {
        mPrefs.edit().putInt(PREF_KEY_NOTIFICATION_ID, notificationId).apply();
    }

    public String getTokenFCM() {
        return mPrefs.getString(PREF_KEY_TOKEN_FCM, null);
    }

    public void setExpireDateTime(DateTime dtExpire) {
        if(dtExpire != null){
            String dtExpireText = dtExpire.format("YYYY-MM-DD hh:mm:ss");
            mPrefs.edit().putString(PREF_KEY_EXPIRE_TOKEN, dtExpireText).apply();
        }else{
            mPrefs.edit().putString(PREF_KEY_EXPIRE_TOKEN, null).apply();
        }
    }

    public DateTime getExpireDateTime() {
        String dtExpireText = mPrefs.getString(PREF_KEY_EXPIRE_TOKEN, null);
        if(dtExpireText == null){
            return null;
        }else{
            return new DateTime(dtExpireText);
        }
    }

    public void setSyncUser(boolean syncUser) {
        mPrefs.edit().putBoolean(PREF_KEY_SYNC_USER, syncUser).apply();
    }

    public boolean getSyncUser() {
        return mPrefs.getBoolean(PREF_KEY_SYNC_USER, false);
    }

    public Usuario getUser() {
        String usuarioJSON = mPrefs.getString(PREF_KEY_USER, null);
        if(usuarioJSON == null){
            return null;
        }
        Gson gson = UtilAPI.getGsonSync();
        return gson.fromJson(usuarioJSON, Usuario.class);
    }

    public void setUser(Usuario usuario) {
        Gson gson = UtilAPI.getGsonSync();
        String usuarioJSON = gson.toJson(usuario);
        mPrefs.edit().putString(PREF_KEY_USER, usuarioJSON).apply();
    }

    private static PreferencesHelper preferencesHelper = null;

    public static PreferencesHelper getInstance(){
        if(preferencesHelper == null){
            preferencesHelper = new PreferencesHelper(MainApplication.getAppContext());
        }
        return preferencesHelper;
    }

    public void saveArrayListEncuesta(ArrayList<ParticipantModel> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        mPrefs.edit().putString(ARRAY_ENCUESTA, json).apply();   // This line is IMPORTANT !!!
    }

    public ArrayList<ParticipantModel> getArrayListEncuesta(String key) {
        Gson gson = new Gson();
        String json = mPrefs.getString(key, null);
        Type type = new TypeToken<ArrayList<ParticipantModel>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public void saveArrayListEmails(ArrayList<String> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        mPrefs.edit().putString(ARRAY_EMAILS, json).apply();   // This line is IMPORTANT !!!
    }

    public ArrayList<String> getArrayListEmails(String key) {
        Gson gson = new Gson();
        String json = mPrefs.getString(key, null);
        Type type = new TypeToken<ArrayList<String>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public void saveArrayListDepartamentos(ArrayList<Ubigeo> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        mPrefs.edit().putString(ARRAY_DEPARTAMENTOS, json).apply();   // This line is IMPORTANT !!!
    }

    public ArrayList<Ubigeo> getArrayListDepartamentos(String key) {
        Gson gson = new Gson();
        String json = mPrefs.getString(key, null);
        Type type = new TypeToken<ArrayList<Ubigeo>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public void saveArrayListProvincias(ArrayList<Ubigeo> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        mPrefs.edit().putString(ARRAY_PROVINCIAS, json).apply();   // This line is IMPORTANT !!!
    }

    public ArrayList<Ubigeo> getArrayListProvincias(String key) {
        Gson gson = new Gson();
        String json = mPrefs.getString(key, null);
        Type type = new TypeToken<ArrayList<Ubigeo>>() {
        }.getType();
        return gson.fromJson(json, type);
    }

    public int getEquipoNumber() {
        return mPrefs.getInt(PREF_KEY_EQUIPO, 0);
    }

    public void setEquipoNumber(int accessToken) {
        mPrefs.edit().putInt(PREF_KEY_EQUIPO, accessToken).apply();
    }

    public int getRetoNumer() {
        return mPrefs.getInt(PREF_KEY_RETO, 0);
    }

    public void setRetoNumber(int accessToken) {
        mPrefs.edit().putInt(PREF_KEY_RETO, accessToken).apply();
    }

    public boolean getUbicationKey() {
        return mPrefs.getBoolean(PREF_KEY_SET_UBICATION, false);
    }

    public void setUbicationKey(boolean accessToken) {
        mPrefs.edit().putBoolean(PREF_KEY_SET_UBICATION, accessToken).apply();
    }
}

