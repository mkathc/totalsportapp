package com.totalsportapp.data.db.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class EquipoLugar {

    @Id(autoincrement = true)
    private Long id;

    private int idEquipo;
    private String ubigeo;
    private String nombreUbigeo;

    public EquipoLugar() {
    }

    public EquipoLugar(int idEquipo, String ubigeo) {
        this.idEquipo = idEquipo;
        this.ubigeo = ubigeo;
    }

    @Generated(hash = 793937322)
    public EquipoLugar(Long id, int idEquipo, String ubigeo, String nombreUbigeo) {
        this.id = id;
        this.idEquipo = idEquipo;
        this.ubigeo = ubigeo;
        this.nombreUbigeo = nombreUbigeo;
    }

    public int getIdEquipo() {
        return idEquipo;
    }

    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }

    public String getUbigeo() {
        return ubigeo;
    }

    public void setUbigeo(String ubigeo) {
        this.ubigeo = ubigeo;
    }

    public String getNombreUbigeo() {
        return nombreUbigeo;
    }

    public void setNombreUbigeo(String nombreUbigeo) {
        this.nombreUbigeo = nombreUbigeo;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
