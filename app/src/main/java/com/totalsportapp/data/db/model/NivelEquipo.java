package com.totalsportapp.data.db.model;

import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class NivelEquipo {

    private int id;
    private String descripcion;

    public NivelEquipo() {
    }

    public NivelEquipo(int id, String descripcion) {
        this.id = id;
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public static List<NivelEquipo> getNivelesEquipo(){
        List<NivelEquipo> nivelesEquipo = new ArrayList<>();

        nivelesEquipo.add(new NivelEquipo(1, "Entusiastas"));
        nivelesEquipo.add(new NivelEquipo(2, "Amateur"));
        nivelesEquipo.add(new NivelEquipo(3, "Intermedio"));
        nivelesEquipo.add(new NivelEquipo(4, "Semi profesional"));
        nivelesEquipo.add(new NivelEquipo(5, "Profesional"));

        return nivelesEquipo;
    }

    public static String getDescripcion(int idNivel){
        List<NivelEquipo> nivelesEquipo = getNivelesEquipo();

        for(int i = 0; i < nivelesEquipo.size(); i++){
            if(nivelesEquipo.get(i).id == idNivel){
                return nivelesEquipo.get(i).getDescripcion();
            }
        }

        return null;
    }
}
