package com.totalsportapp.data.db.model;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.JoinProperty;
import org.greenrobot.greendao.annotation.ToMany;
import org.greenrobot.greendao.annotation.Transient;

import java.util.List;
import org.greenrobot.greendao.DaoException;

@Entity
public class Equipo {

    @Id(autoincrement = true)
    private Long id;

    private int idEquipo;
    private String nombre;
    private int nivel;
    private String foto;
    private int idUsuario;
    private boolean esLider;
    private int ganados;
    private int empatados;
    private int ausencias;
    private String calificacion;


    @Transient
    private String capitan;

    @Transient
    private List<EquipoPreferenciaHoraria> preferenciaHorarias;

    @ToMany(joinProperties = {
            @JoinProperty(name = "idEquipo", referencedName = "idEquipo")
    })
    private List<EquipoLugar> lugares;

    @Transient
    private List<EquipoMiembro> miembros;

    /** Used to resolve relations */
    @Generated(hash = 2040040024)
    private transient DaoSession daoSession;
    /** Used for active entity operations. */
    @Generated(hash = 1799817752)
    private transient EquipoDao myDao;

    @Generated(hash = 1991173996)
    public Equipo(Long id, int idEquipo, String nombre, int nivel, String foto, int idUsuario,
            boolean esLider, int ganados, int empatados, int ausencias, String calificacion) {
        this.id = id;
        this.idEquipo = idEquipo;
        this.nombre = nombre;
        this.nivel = nivel;
        this.foto = foto;
        this.idUsuario = idUsuario;
        this.esLider = esLider;
        this.ganados = ganados;
        this.empatados = empatados;
        this.ausencias = ausencias;
        this.calificacion = calificacion;
    }
    @Generated(hash = 1311658498)
    public Equipo() {
    }

    public int getIdEquipo() {
        return this.idEquipo;
    }
    public void setIdEquipo(int idEquipo) {
        this.idEquipo = idEquipo;
    }
    public String getNombre() {
        return this.nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public int getNivel() {
        return this.nivel;
    }
    public void setNivel(int nivel) {
        this.nivel = nivel;
    }
    public String getFoto() {
        return this.foto;
    }
    public void setFoto(String foto) {
        this.foto = foto;
    }
    public int getIdUsuario() {
        return this.idUsuario;
    }
    public void setIdUsuario(int idUsuario) {
        this.idUsuario = idUsuario;
    }
    public boolean getEsLider() {
        return this.esLider;
    }
    public void setEsLider(boolean esLider) {
        this.esLider = esLider;
    }
    public String getCapitan() { return capitan; }
    public void setCapitan(String capitan) { this.capitan = capitan;}

    public int getGanados() {
        return ganados;
    }

    public void setGanados(int ganados) {
        this.ganados = ganados;
    }

    public int getEmpatados() {
        return empatados;
    }

    public void setEmpatados(int empatados) {
        this.empatados = empatados;
    }

    public int getAusencias() {
        return ausencias;
    }

    public void setAusencias(int ausencias) {
        this.ausencias = ausencias;
    }

    public String getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(String calificacion) {
        this.calificacion = calificacion;
    }

    public String getLugaresText(){
        String lugares = "";
        for(EquipoLugar equipoLugar : this.getLugares()){
            lugares = lugares + equipoLugar.getNombreUbigeo() + ", ";
        }

        if(lugares.isEmpty() == false){
            lugares = lugares.substring(0, lugares.length() - 2);
        }

        return lugares;
    }

    public void setLugares(List<EquipoLugar> lugares) {
        this.lugares = lugares;
    }

    public List<EquipoPreferenciaHoraria> getPreferenciaHorarias() {
        return preferenciaHorarias;
    }

    public void setPreferenciaHorarias(List<EquipoPreferenciaHoraria> preferenciaHorarias) {
        this.preferenciaHorarias = preferenciaHorarias;
    }

    public List<EquipoMiembro> getMiembros() {
        return miembros;
    }

    public void setMiembros(List<EquipoMiembro> miembros) {
        this.miembros = miembros;
    }

    /**
     * To-many relationship, resolved on first access (and after reset).
     * Changes to to-many relations are not persisted, make changes to the target entity.
     */
    @Generated(hash = 1716744900)
    public List<EquipoLugar> getLugares() {
        if (lugares == null) {
            final DaoSession daoSession = this.daoSession;
            if (daoSession == null) {
                throw new DaoException("Entity is detached from DAO context");
            }
            EquipoLugarDao targetDao = daoSession.getEquipoLugarDao();
            List<EquipoLugar> lugaresNew = targetDao._queryEquipo_Lugares(idEquipo);
            synchronized (this) {
                if (lugares == null) {
                    lugares = lugaresNew;
                }
            }
        }
        return lugares;
    }
    /** Resets a to-many relationship, making the next get call to query for a fresh result. */
    @Generated(hash = 151675732)
    public synchronized void resetLugares() {
        lugares = null;
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#delete(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 128553479)
    public void delete() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.delete(this);
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#refresh(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 1942392019)
    public void refresh() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.refresh(this);
    }
    /**
     * Convenient call for {@link org.greenrobot.greendao.AbstractDao#update(Object)}.
     * Entity must attached to an entity context.
     */
    @Generated(hash = 713229351)
    public void update() {
        if (myDao == null) {
            throw new DaoException("Entity is detached from DAO context");
        }
        myDao.update(this);
    }
    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    /** called by internal mechanisms, do not call yourself. */
    @Generated(hash = 1926466649)
    public void __setDaoSession(DaoSession daoSession) {
        this.daoSession = daoSession;
        myDao = daoSession != null ? daoSession.getEquipoDao() : null;
    }
}
