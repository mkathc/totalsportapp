package com.totalsportapp.data.db.model;

public class LoggedMode {
    public static final int LOGGED_OUT = 0;
    public static final int LOGGED_IN = 1;
    //public static final int LOGGED_IN_FB = 2;
    //public static final int LOGGED_IN_GOOGLE = 2;
}
