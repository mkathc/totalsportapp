package com.totalsportapp.data.db.model;

import org.greenrobot.greendao.DaoException;
import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Generated;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.JoinProperty;
import org.greenrobot.greendao.annotation.ToMany;
import org.greenrobot.greendao.annotation.Transient;

import java.util.List;

public class EstadoInvitacion {
    public final static int NO_INVITADO = -1;
    public final static int INVITADO = 0;
    public final static int ACEPTADO = 1;
    public final static int DENEGADO = 2;
}
