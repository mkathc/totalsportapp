package com.totalsportapp;

import android.app.Application;
import android.content.Context;

import com.totalsportapp.data.db.DbOpenHelper;
import com.totalsportapp.data.db.model.DaoMaster;
import com.totalsportapp.data.db.model.DaoSession;

public class MainApplication extends Application {

    private static Context ctx;
    private DaoSession mDaoSession;

    @Override
    public void onCreate() {
        super.onCreate();

        ctx = this;

        mDaoSession = new DaoMaster(new DbOpenHelper(this, "totalsport_app.db").getWritableDb()).newSession();
    }

    public static Context getAppContext(){
        return ctx;
    }

    public DaoSession getDaoSession() {
        return mDaoSession;
    }
}
